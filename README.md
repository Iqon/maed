# README 
__MEADN__ - Mensch Ärger Dich Nicht build by __AIG__ _Artificial Intelligent Gameplay_.

## ABOUT
Custom made game, display with WS2801 LEDS driven by a STM32F4.

For the architectual and technical documenation please visit [MAEDN Documentation](https://maed-doc.aerobatic.io/)

## GAME
![picture](img/IMG_20170325_155740.jpg)
![picture](img/IMG_20170325_155827.jpg)
![picture](img/IMG_20170325_155919.jpg)
![picture](img/IMG_20170325_155931.jpg)