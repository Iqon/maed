/* Includes */
#include "stm32f4xx.h"
#include "core_cm4.h"
#include "stm32f4_discovery.h"

void Delay(__IO uint32_t time);
extern __IO uint32_t TimingDelay;

int main(void)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	STM_EVAL_LEDInit(LED6);
	STM_EVAL_LEDOn(LED6);

	SysTick_Config(SystemCoreClock/1000);

	while (1)
	{
		Delay(500);
		STM_EVAL_LEDToggle(LED6);
	}
}

void Delay(__IO uint32_t time)
{
  TimingDelay = time;
  while(TimingDelay !=0);
}

/*
 * Callback used by stm32f4_discovery_audio_codec.c.
 * Refer to stm32f4_discovery_audio_codec.h for more info.
 */
extern "C" void EVAL_AUDIO_TransferComplete_CallBack(uint32_t pBuffer, uint32_t Size)
{
  return;
}

/*
 * Callback used by stm324xg_eval_audio_codec.c.
 * Refer to stm324xg_eval_audio_codec.h for more info.
 */
extern "C" uint16_t EVAL_AUDIO_GetSampleCallBack(void)
{
  return -1;
}
