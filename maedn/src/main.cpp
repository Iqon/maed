/**
 * \file    main.cpp
 * \author	Aiko Isselhard
 * \brief	Main entry point. Initialises the hardware, starts the software stack and starts a new game.
 * */

void TimerTickCallback(void);

#include <inttypes.h>

#include <stm32f4xx.h>
#include <stm32f4_discovery.h>
#include <stm32f4xx_rng.h>

#include <io/input/InputMgr.h>

#include <io/output/ILEDDriver.h>
#include <io/output/WS2801LEDDriver.h>

#include <game/GameMgr.h>

#include <state/StateMgr.h>

#include <states/GameStartState.h>
#include <states/development/Developement.h>

#define REDRAW_TICKS (5)

void InitTimer( );
void GlobalInit( );
IState* GetStartState( );

extern void (*functionPtr)(void);

static volatile uint32_t Count = 0;

TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

/**
 * \brief Main entry point.
 */
int main(void) {
    functionPtr = TimerTickCallback;

    GlobalInit();
    InitTimer();

    IState *state = GetStartState();
    StateMgr::GetInstance()->SwitchToState(state);

    uint32_t counter = TIM_GetCounter(TIM2);
    for (;;) {
        if (Count < REDRAW_TICKS) continue;

        Count = 0x00;

        uint32_t new_val = TIM_GetCounter(TIM2);
        uint32_t diff = new_val > counter ? new_val - counter : 0;
        counter = new_val;

        WS2801LEDDriver::GetInstance()->Clear();
        GameMgr::GetInstance()->Draw();

        InputMgr::GetInstance()->Update(diff);
        StateMgr::GetInstance()->Update(diff);
        WS2801LEDDriver::GetInstance()->Update();
    }
}

IState* GetStartState( ) {
    InputMgr *input = InputMgr::GetInstance();
    input->Update(0);
    IState* result;

    if (input->GetPlayerButton(0, ButtonTypeChange) == ButtonStateDown) {
        result = new TestADCState();
    } else if (input->GetPlayerButton(0, ButtonTypeConfirm) == ButtonStateDown) {
        result = new AnimateDisplayTestState();
    } else if (input->GetPlayerButton(1, ButtonTypeChange) == ButtonStateDown) {
        result = new TestInputState(ButtonStateReleased);
    } else if (input->GetPlayerButton(1, ButtonTypeConfirm) == ButtonStateDown) {
        result = new TestInputState(ButtonStateDown);
    } else if (input->GetPlayerButton(2, ButtonTypeChange) == ButtonStateDown) {
        result = new TestGameState();
    } else if (input->GetPlayerButton(2, ButtonTypeConfirm) == ButtonStateDown) {
        result = new PulseCompleteDisplayState();
    } else {
        result = new GameStartState();
    }

    return result;
}

void TimerTickCallback(void) {
    Count++;
}

void GlobalInit( ) {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

    /* Initialize LEDs */
    STM_EVAL_LEDInit(LED6);

    /* Turn on LEDs */
    STM_EVAL_LEDOn(LED6);

    RNG_Cmd(ENABLE);
    SysTick_Config(SystemCoreClock / 1000);
}

void InitTimer( ) {
    /* TIM2 clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
    TIM_TimeBaseStructure.TIM_Prescaler = 40000;
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV4;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

    /* TIM2 enable counter */
    TIM_Cmd(TIM2, ENABLE);
}

/*
 * Callback used by stm32f4_discovery_audio_codec.c.
 * Refer to stm32f4_discovery_audio_codec.h for more info.
 */
extern "C" void EVAL_AUDIO_TransferComplete_CallBack(uint32_t pBuffer,
                                                     uint32_t Size) {
    return;
}

/*
 * Callback used by stm324xg_eval_audio_codec.c.
 * Refer to stm324xg_eval_audio_codec.h for more info.
 */
extern "C" uint16_t EVAL_AUDIO_GetSampleCallBack(void) {
    return -1;
}
