/**
 * \file	InputMgr.cpp
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <io/input/InputMgr.h>

InputMgr::InputMgr( ) {
    this->_brightness = 0x00;

    for (uint8_t i = 0x00; i < INPUT_MGR_NUM_BUTTONS; ++i) {
        this->_states[i] = ButtonStateUp;
        this->_deboundeDelay[i] = 0x00;
    }

    GPIOD->MODER &= ~GPIO_MODER_MODER11; // DevButtonChange
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR11;

    GPIOD->MODER &= ~GPIO_MODER_MODER13; // DevButtonConfirm
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR13;

    GPIOE->MODER &= ~GPIO_MODER_MODER13; // Player0Confirm
    GPIOE->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR13;

    GPIOE->MODER &= ~GPIO_MODER_MODER15; // Player0Change
    GPIOE->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15;

    GPIOB->MODER &= ~GPIO_MODER_MODER11; // Player1Change
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR11;

    GPIOB->MODER &= ~GPIO_MODER_MODER13; // Player1Confirm
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR13;

    GPIOD->MODER &= ~GPIO_MODER_MODER9; // Player2Confirm
    GPIOD->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9;

    GPIOB->MODER &= ~GPIO_MODER_MODER15; // Player2Change
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15;

    GPIOE->MODER &= ~GPIO_MODER_MODER9; // Player3Confirm
    GPIOE->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9;

    GPIOE->MODER &= ~GPIO_MODER_MODER11; // Player3Change
    GPIOE->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR11;

    this->InitADC();
}

void InputMgr::InitADC(void) {
    SystemInit();
    TM_GPIO_Init(GPIOA, GPIO_PIN_1, TM_GPIO_Mode_AN, TM_GPIO_OType_PP, TM_GPIO_PuPd_DOWN, TM_GPIO_Speed_Medium);
    ADC_InitTypeDef ADC_InitStruct;
    ADC_CommonInitTypeDef ADC_CommonInitStruct;

    /* Init ADC settings */
    ADC_InitStruct.ADC_ContinuousConvMode = DISABLE;
    ADC_InitStruct.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStruct.ADC_ExternalTrigConv = DISABLE;
    ADC_InitStruct.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
    ADC_InitStruct.ADC_NbrOfConversion = 1;
    ADC_InitStruct.ADC_ScanConvMode = DISABLE;

    /* Enable ADC clock */
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;

    /* Set resolution */
    ADC_InitStruct.ADC_Resolution = ADC_Resolution_12b;

    /* Set common ADC settings */
    ADC_CommonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    ADC_CommonInitStruct.ADC_Mode = ADC_Mode_Independent;
    ADC_CommonInitStruct.ADC_Prescaler = ADC_Prescaler_Div4;
    ADC_CommonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_8Cycles;
    ADC_CommonInit(&ADC_CommonInitStruct);

    /* Initialize ADC */
    ADC_Init(ADC1, &ADC_InitStruct);

    /* Enable ADC */
    ADC1->CR2 |= ADC_CR2_ADON;
}

ButtonState InputMgr::GetPlayerButton(uint8_t player,
                                      ButtonType btn) {
    switch (player) {
    case 0x00:
        return btn == ButtonTypeChange ? this->GetButtonState(Player0Change) : this->GetButtonState(Player0Confirm);
    case 0x01:
        return btn == ButtonTypeChange ? this->GetButtonState(Player1Change) : this->GetButtonState(Player1Confirm);
    case 0x02:
        return btn == ButtonTypeChange ? this->GetButtonState(Player2Change) : this->GetButtonState(Player2Confirm);
    case 0x03:
        return btn == ButtonTypeChange ? this->GetButtonState(Player3Change) : this->GetButtonState(Player3Confirm);
    default:
        return ButtonStateUp;
    }
}

ButtonState InputMgr::GetButtonState(Button btn) {
    uint8_t no = ((uint8_t) btn) % INPUT_MGR_NUM_BUTTONS;
    return this->_states[no];
}

void InputMgr::Update(uint32_t diff) {
    DecreaseDebounceDelay(diff);

    this->UpdateButtonState(DevButtonChange, GPIOD->IDR & GPIO_IDR_IDR_11);
    this->UpdateButtonState(DevButtonConfirm, GPIOD->IDR & GPIO_IDR_IDR_13);

    this->UpdateButtonState(Player0Confirm, GPIOE->IDR & GPIO_IDR_IDR_15);
    this->UpdateButtonState(Player0Change, GPIOE->IDR & GPIO_IDR_IDR_13);

    this->UpdateButtonState(Player1Change, GPIOB->IDR & GPIO_IDR_IDR_13);
    this->UpdateButtonState(Player1Confirm, GPIOB->IDR & GPIO_IDR_IDR_11);

    this->UpdateButtonState(Player2Confirm, GPIOB->IDR & GPIO_IDR_IDR_15);
    this->UpdateButtonState(Player2Change, GPIOD->IDR & GPIO_IDR_IDR_9);

    this->UpdateButtonState(Player3Confirm, GPIOE->IDR & GPIO_IDR_IDR_9);
    this->UpdateButtonState(Player3Change, GPIOE->IDR & GPIO_IDR_IDR_11);

    this->ReadADC();
}

void InputMgr::UpdateButtonState(Button button,
                                 bool pressed) {
    uint8_t no = ((uint8_t) button) % INPUT_MGR_NUM_BUTTONS;

    if (pressed) {
        this->ButtonDown(no);
    } else if (this->Debounced(no)) {
        this->ButtonUp(no);
    }
}

void InputMgr::DecreaseDebounceDelay(uint32_t diff) {
    for (uint8_t i = 0x00; i < INPUT_MGR_NUM_BUTTONS; ++i) {
        if (_deboundeDelay[i] < diff) {
            _deboundeDelay[i] = 0x00;
        } else {
            _deboundeDelay[i] -= diff;
        }
    }
}

void InputMgr::ReadADC(void) {
    ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_15Cycles);

    /* Start software conversion */
    ADC1->CR2 |= (uint32_t) ADC_CR2_SWSTART;

    /* Wait till done */
    while (!(ADC1->SR & ADC_SR_EOC))
        ;

    /* Return result */
    uint16_t val = ADC1->DR;
    this->_brightness = ((INPUT_MGR_ADC_RANGE - (float) val) / INPUT_MGR_ADC_RANGE) + INPUT_MGR_ADC_MIN;

    if (this->_brightness < INPUT_MGR_ADC_MIN) {
        this->_brightness = INPUT_MGR_ADC_MIN;
    }
    if (this->_brightness > 1.0f) {
        this->_brightness = 1.0f;
    }

    WS2801LEDDriver::GetInstance()->SetBrightness(this->_brightness);
}

void InputMgr::ButtonUp(uint8_t btnNo) {
    if (this->_states[btnNo] == ButtonStateDown) {
        this->_states[btnNo] = ButtonStateReleased;
    } else {
        this->_states[btnNo] = ButtonStateUp;
    }
}

void InputMgr::ButtonDown(uint8_t btnNo) {
    this->_states[btnNo] = ButtonStateDown;
    this->_deboundeDelay[btnNo] = INPUT_MGR_DEBOUNCE;
}

bool InputMgr::Debounced(uint8_t btnNo) {
    return this->_deboundeDelay[btnNo] == 0x00;
}

float InputMgr::GetBrightness( ) {
    return this->_brightness;
}
