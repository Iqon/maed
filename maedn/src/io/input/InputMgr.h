/**
 * \file	InputMgr.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Contains the interface for interacting with the physical buttons and switches.
 * 			Allows for easy player specific access to the buttons on the field.
 * 			Attention the RCC_AHB1PeriphClockCmd must be enabled for port b and d.
 *
 * 			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
 *			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
 * */

#ifndef INPUTMGR_H_
#define INPUTMGR_H_

#include <stm32f4xx.h>
#include <tm_stm32f4_gpio.h>
#include <global.h>

#include <io/input/Button.h>
#include <io/input/ButtonState.h>
#include <io/input/ButtonType.h>
#include <io/output/WS2801LEDDriver.h>

#define INPUT_MGR_DEBOUNCE (20 * MS_TICKS)

#define INPUT_MGR_ADC_RANGE	(4095.0f) // measured range is from 0 - 4095
#define INPUT_MGR_ADC_MIN	(0.05f)

/**
 * \brief	Contains the interface for interacting with the physical buttons and switches.
 * 			Allows for easy player specific access to the buttons on the field.
 * 			Attention the RCC_AHB1PeriphClockCmd must be enabled for port b and d.
 *
 * 			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
 *			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
 */
class InputMgr {
public:
    /**
     * \brief	Returns the singleton instance of the InputMgr.
     * \returns the singleton instance of the InputMgr.
     */
    static InputMgr *GetInstance( ) {
        static InputMgr *mgr = new InputMgr();
        return mgr;
    }

    /**
     * \brief	Will update all buttons state. Must be called repeatedly, because the button states are polled and not controlled by interrupts.
     * \param	diff	The time difference since the last call, needed to debounce the buttons.
     */
    void Update(uint32_t diff);

    /**
     * \brief	Returns the ButtonState for the given Button.
     * \returns the ButtonState
     */
    ButtonState GetButtonState(Button btn);

    /**
     * \brief	Returns the ButtonState for the given player and ButtonType.
     * \returns the ButtonState
     */
    ButtonState GetPlayerButton(uint8_t player,
                                ButtonType btn);

    /**
     * \brief	gets the brightness read from the ADC and converted into a float range 0f..1f.
     * \returns gets the brightness read from the ADC and converted into a float range 0f..1f.
     */
    float GetBrightness( );

private:
    ButtonState _states[INPUT_MGR_NUM_BUTTONS];
    uint32_t _deboundeDelay[INPUT_MGR_NUM_BUTTONS];
    float _brightness;

    InputMgr( );

    void UpdateButtonState(Button button,
                           bool pressed);
    void DecreaseDebounceDelay(uint32_t diff);

    void ButtonUp(uint8_t btnNo);
    void ButtonDown(uint8_t btnNo);
    bool Debounced(uint8_t btnNo);

    void InitADC(void);
    void ReadADC(void);
};

#endif /* INPUTMGR_H_ */
