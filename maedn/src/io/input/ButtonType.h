/**
 * \file	ButtonType.h
 * \date	Jun 15, 2016
 * \author 	Aiko Isselhard
 * \brief	Definitions for the button types (confirm or change).
 * */

#ifndef BUTTONTYPE_H_
#define BUTTONTYPE_H_

/**
 * Enumeration of the button types (either change or confirm).
 */
enum ButtonType {
    ButtonTypeChange, ButtonTypeConfirm
};

#endif /* BUTTONTYPE_H_ */
