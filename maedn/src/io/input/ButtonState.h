/**
 * \file	ButtonState.h
 * \date	Jun 15, 2016
 * \author 	Aiko Isselhard
 * \brief	Definitions for the different states a button can have.
 * */

#ifndef BUTTONSTATE_H_
#define BUTTONSTATE_H_

/**
 * enumeration of all possible button states.
 */
enum ButtonState {
    /**
     * Set if the button is currently not pressed (default).
     */
    ButtonStateUp = 0,

    /**
     * Sets if the button is currently pressed.
     */
    ButtonStateDown = 1,

    /**
     * Set it the button is currently released. Will only be set for one frame
     * before the button state will go to ButtonStateUp.
     */
    ButtonStateReleased = 2
};

#endif /* BUTTONSTATE_H_ */
