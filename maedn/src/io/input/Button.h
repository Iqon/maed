/**
 * \file	Button.h
 * \date	Jun 15, 2016
 * \author 	Aiko Isselhard
 * \brief	Definitions for the different buttons.
 * */

#ifndef BUTTON_H_
#define BUTTON_H_

#define INPUT_MGR_NUM_BUTTONS 10

/**
 * \brief	enumeration of all available buttons.
 */
enum Button {
    DevButtonChange = 0,
    DevButtonConfirm = 1,
    Player0Change = 2,
    Player0Confirm = 3,
    Player1Change = 4,
    Player1Confirm = 5,
    Player2Change = 6,
    Player2Confirm = 7,
    Player3Change = 8,
    Player3Confirm = 9
};

#endif /* BUTTON_H_ */
