/**
 * \file	DisplayDriver.h
 * \date	May 27, 2016
 * \author 	Aiko Isselhard
 * \brief	The display driver maps leds to the game environment and to the game's objects.
 * */

#ifndef DISPLAYDRIVER_H_
#define DISPLAYDRIVER_H_

#include <io/output/ILEDDriver.h>
#include <io/output/rgb.h>
#include <io/output/WS2801LEDDriver.h>

/**
 * \brief	Sets the number of leds for one round trip on the field.
 */
#define ROUND_LEN 40

/**
 * \brief	The display driver maps leds to the game environment and to the game's objects.
 * 			Contains only one clear function for the multipliyer bar. Generally it is a bad
 * 			idea to only clear parts of the display. Always clear the complete display and
 * 			redraw everything.
 *
 * 			Uses the ILEDDriver interface to map view objects to the LED indexes available
 * 			by the ILEDDriver.
 * */
class DisplayDriver {
public:
    virtual ~DisplayDriver( );

    /**
     * \brief	Sets dice to display the given number
     */
    void Dice(const uint8_t no,
              const rgb color);

    /**
     * \brief	Sets one pixel in the multiplyer bar.
     */
    void Multiplyer(const uint8_t player,
                    const uint8_t no,
                    const rgb color);

    /**
     * \brief	Clears the multiplyer bar.
     */
    void ClearMultiplyer(const uint8_t player);

    /**
     * \brief	Sets one pixel in the house of the given player at the given position
     */
    void House(const uint8_t player,
               const uint8_t no,
               const rgb color);

    /**
     * \brief	Sets one pixel in the goal of the given player at the given position
     */
    void Goal(const uint8_t player,
              const uint8_t no,
              const rgb color);

    /**
     * \brief	Sets one pixel on the field
     */
    void Field(const uint8_t pos,
               const rgb color);

    /**
     * \brief	Returns the singleton instance of the display driver
     *
     * \returns the singleton instance of DisplayDriver.
     */
    static DisplayDriver *GetInstance( ) {
        static DisplayDriver *driver = new DisplayDriver();
        return driver;
    }

private:
    DisplayDriver( );

    ILEDDriver *_driver;

    void Dice1(const rgb color);
    void Dice2(const rgb color);
    void Dice3(const rgb color);
    void Dice4(const rgb color);
    void Dice5(const rgb color);
    void Dice6(const rgb color);
    void Dice7(const rgb color);

    void GoalPlayer1(const uint8_t count,
                     const rgb color);
    void GoalPlayer2(const uint8_t count,
                     const rgb color);
    void GoalPlayer3(const uint8_t count,
                     const rgb color);
    void GoalPlayer4(const uint8_t count,
                     const rgb color);
};

#endif /* DISPLAYDRIVER_H_ */
