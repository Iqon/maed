/**
 * \file	WS2801LEDDriver.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <io/output/WS2801LEDDriver.h>

#define CROP_MIN(val, min)  ((val) < (min) ? (min) : (val))
#define CROP_MAX(val, max)  ((val) > (max) ? (max) : (val))
#define CROP(val)           (CROP_MAX((CROP_MIN(val, 0x00)), 0xFF))

//(CROP_MAX((CROP_MIN(val, 0x00)), 0xFF))

WS2801LEDDriver::WS2801LEDDriver( ) {
    this->_led_count = NUM_LEDS;

    // initialize the screen buffer.
    memset(this->_drawingBuffer, 0x00, sizeof(rgb) * NUM_LEDS);

    // initialize the spi bus.
    this->InitSPI();
    this->Update();

    this->_brightness = 1.0f;
}

WS2801LEDDriver::~WS2801LEDDriver( ) {
    this->_led_count = 0x00;
}

void WS2801LEDDriver::SetLED(uint16_t pos,
                             uint8_t r,
                             uint8_t g,
                             uint8_t b) {
    if (pos < this->_led_count) {
        this->_drawingBuffer[pos].r = r;
        this->_drawingBuffer[pos].g = g;
        this->_drawingBuffer[pos].b = b;
    }
}

void WS2801LEDDriver::SetBrightness(float brightness) {
    this->_brightness = brightness;
}

void WS2801LEDDriver::Update(void) {
    __disable_irq();

    for (uint16_t i = 0x00; i < this->_led_count; i++) {
        rgb pixel = this->_drawingBuffer[i];

        // The last 7 pixels have a different rgb sequence
        if (i < this->_led_count - 7) {
            uint8_t r = (uint8_t) CROP(pixel.r * this->_brightness);
            uint8_t b = (uint8_t) CROP(pixel.b * this->_brightness);
            uint8_t g = (uint8_t) CROP(pixel.g * this->_brightness);

            this->SPISend(r); // needs r g b
            this->SPISend(b);
            this->SPISend(g);

        } else {
            uint8_t r = (uint8_t) CROP(pixel.r * this->_brightness * DICE_BRIGHTNESS);
            uint8_t g = (uint8_t) CROP(pixel.g * this->_brightness * DICE_BRIGHTNESS);
            uint8_t b = (uint8_t) CROP(pixel.b * this->_brightness * DICE_BRIGHTNESS);

            this->SPISend(r); // needs r g b
            this->SPISend(g);
            this->SPISend(b);
        }
    }

    __enable_irq();
}

void WS2801LEDDriver::Clear(void) {
    memset(this->_drawingBuffer, 0x00, sizeof(rgb) * NUM_LEDS);
}

void WS2801LEDDriver::InitSPI(void) {
    GPIO_InitTypeDef GPIO_InitStruct;
    SPI_InitTypeDef SPI_InitStruct;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    /* configure pins used by SPI1
     * PA5 = SCK
     * PA6 = MISO
     * PA7 = MOSI
     */
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_6 | GPIO_Pin_5;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStruct);

    // connect SPI1 pins to SPI alternate function
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);

    // enable clock for used IO pins
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);

    /* Configure the chip select pin in this case we will use PE7 */
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOE, &GPIO_InitStruct);

    GPIOE->BSRRL |= GPIO_Pin_7; // set PE7 high

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

    /* configure SPI1 in Mode 0
     * CPOL = 0 --> clock is low when idle
     * CPHA = 0 --> data is sampled at the first edge
     */
    SPI_InitStruct.SPI_Direction = SPI_Direction_1Line_Tx; 				// set to full duplex mode, seperate MOSI and MISO lines
    SPI_InitStruct.SPI_Mode = SPI_Mode_Master;     						// transmit in master mode, NSS pin has to be always high
    SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b; 						// one packet of data is 8 bits wide
    SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;        						// clock is low when idle
    SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;      						// data sampled at first edge
    SPI_InitStruct.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set; 	// set the NSS management to internal and pull internal NSS high
    SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_32; 	// SPI frequency is APB2 frequency / 32
    SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;						// data is transmitted MSB first
    SPI_Init(SPI1, &SPI_InitStruct);

    SPI_Cmd(SPI1, ENABLE); // enable SPI1
}

void WS2801LEDDriver::SPISend(uint8_t data) {
    while ( SPI1->SR & SPI_I2S_FLAG_BSY)
        ; 		// wait until SPI is not busy anymore
    SPI1->DR = data; 							// write data to be transmitted to the SPI data register
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE))
        ; 	// wait until transmit complete
}
