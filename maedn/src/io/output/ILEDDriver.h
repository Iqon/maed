/**
 * \file	ILEDDriver.h
 * \date	May 27, 2016
 * \author 	Aiko Isselhard
 * \brief	Public interface for an LED Driver. Used to drive the led stripes.
 * */

#ifndef ILEDDRIVER_H_
#define ILEDDRIVER_H_

#include <global.h>
#include <io/output/rgb.h>

/**
 * @brief	Interface for LED Driver. Used so the hardware layer is exchangeable.
 * 			Allows for easy interaction with LEDs in any physical layout by
 * 			Directly accessing the leds by their index.
 */
class ILEDDriver {
public:
    ILEDDriver( );
    virtual ~ILEDDriver( );

    /**
     * \brief Sets the led at given position to the given color. Colors range from 0x00 to 0xFF
     *
     * \param pos 	Position of the led to light
     * \param r 	The red intensity [0x00-0xFF]
     * \param g 	The green intensity [0x00-0xFF]
     * \param b 	The blue intensity [0x00-0xFF]
     */
    virtual void SetLED(uint16_t pos,
                        uint8_t r,
                        uint8_t g,
                        uint8_t b) {
    }

    /**
     * \brief	Sets the led at given position to the given color. Colors range from 0x00 to 0xFF
     */
    virtual void SetLED(uint16_t pos,
                        rgb color) {
        this->SetLED(pos, color.r, color.g, color.b);
    }

    /**
     * \brief	Updates the led matrix. Will display the current buffer on the led matrix.
     */
    virtual void Update(void) {
    }

    /**
     * \brief	Clears the led buffer, which sets all leds to 0x000000
     */
    virtual void Clear(void) {
    }

    /**
     * \brief 	Sets the current matrixs brightness. Setting the brightness above 100% will oversteer the matrix.
     * \param brightness	Sets the brightness in percent 0-1
     */
    virtual void SetBrightness(float brightness) {
    }
};

#endif /* ILEDDRIVER_H_ */
