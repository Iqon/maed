/**
 * \file	DisplayDriver.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <io/output/DisplayDriver.h>

DisplayDriver::DisplayDriver( ) {
    this->_driver = WS2801LEDDriver::GetInstance();
}

DisplayDriver::~DisplayDriver( ) {

}

void DisplayDriver::Dice(const uint8_t no,
                         const rgb color) {
    switch (no) {
    case 1:
        this->Dice1(color);
    break;
    case 2:
        this->Dice2(color);
    break;
    case 3:
        this->Dice3(color);
    break;
    case 4:
        this->Dice4(color);
    break;
    case 5:
        this->Dice5(color);
    break;
    case 6:
        this->Dice6(color);
    break;
    case 7:
        this->Dice7(color);
    break;
    }
}

void DisplayDriver::Dice1(const rgb color) {
    this->_driver->SetLED(75, color);
}

void DisplayDriver::Dice2(const rgb color) {
    this->_driver->SetLED(72, color);
    this->_driver->SetLED(76, color);

}

void DisplayDriver::Dice3(const rgb color) {
    this->_driver->SetLED(72, color);
    this->_driver->SetLED(75, color);
    this->_driver->SetLED(76, color);
}

void DisplayDriver::Dice4(const rgb color) {
    this->_driver->SetLED(72, color);
    this->_driver->SetLED(74, color);
    this->_driver->SetLED(76, color);
    this->_driver->SetLED(78, color);

}

void DisplayDriver::Dice5(const rgb color) {
    this->_driver->SetLED(72, color);
    this->_driver->SetLED(74, color);
    this->_driver->SetLED(75, color);
    this->_driver->SetLED(76, color);
    this->_driver->SetLED(78, color);
}

void DisplayDriver::Dice6(const rgb color) {
    this->_driver->SetLED(72, color);
    this->_driver->SetLED(73, color);
    this->_driver->SetLED(74, color);
    this->_driver->SetLED(76, color);
    this->_driver->SetLED(77, color);
    this->_driver->SetLED(78, color);
}

void DisplayDriver::Dice7(const rgb color) {
    this->_driver->SetLED(72, color);
    this->_driver->SetLED(73, color);
    this->_driver->SetLED(74, color);
    this->_driver->SetLED(75, color);
    this->_driver->SetLED(76, color);
    this->_driver->SetLED(77, color);
    this->_driver->SetLED(78, color);
}

void DisplayDriver::ClearMultiplyer(const uint8_t player) {
    switch (player) {
    case 0:
        this->_driver->SetLED(14, 0x00, 0x00, 0x00);
        this->_driver->SetLED(13, 0x00, 0x00, 0x00);
        this->_driver->SetLED(4, 0x00, 0x00, 0x00);
    case 1:
        this->_driver->SetLED(68, 0x00, 0x00, 0x00);
        this->_driver->SetLED(67, 0x00, 0x00, 0x00);
        this->_driver->SetLED(59, 0x00, 0x00, 0x00);
    case 2:
        this->_driver->SetLED(50, 0x00, 0x00, 0x00);
        this->_driver->SetLED(49, 0x00, 0x00, 0x00);
        this->_driver->SetLED(40, 0x00, 0x00, 0x00);
    case 3:
        this->_driver->SetLED(32, 0x00, 0x00, 0x00);
        this->_driver->SetLED(31, 0x00, 0x00, 0x00);
        this->_driver->SetLED(23, 0x00, 0x00, 0x00);
    default:
        return;
    }
}
void DisplayDriver::Multiplyer(const uint8_t player,
                               const uint8_t no,
                               const rgb color) {
    switch (player) {
    case 0:
        switch (no) {
        case 3:
            this->_driver->SetLED(14, color);
        break;
        case 2:
            this->_driver->SetLED(13, color);
        break;
        case 1:
            this->_driver->SetLED(4, color);
        break;
        }
    break;
    case 1:
        switch (no) {
        case 3:
            this->_driver->SetLED(68, color);
        break;
        case 2:
            this->_driver->SetLED(67, color);
        break;
        case 1:
            this->_driver->SetLED(59, color);
        break;
        }
    break;
    case 2:
        switch (no) {
        case 3:
            this->_driver->SetLED(50, color);
        break;
        case 2:
            this->_driver->SetLED(49, color);
        break;
        case 1:
            this->_driver->SetLED(40, color);
        break;
        }
    break;
    case 3:
        switch (no) {
        case 3:
            this->_driver->SetLED(32, color);
        break;
        case 2:
            this->_driver->SetLED(31, color);
        break;
        case 1:
            this->_driver->SetLED(23, color);
        break;
        }
    break;
    default:
        return;
    }
}

void DisplayDriver::House(const uint8_t player,
                          const uint8_t no,
                          const rgb color) {
    int start;

    switch (player) {
    case 0:
        start = 0;
    break;
    case 1:
        start = 55;
    break;
    case 2:
        start = 36;
    break;
    case 3:
        start = 19;
    break;
    default:
        return;
    }

    switch (no) {
    case 3:
        this->_driver->SetLED(start + 3, color);
    break;
    case 2:
        this->_driver->SetLED(start + 2, color);
    break;
    case 1:
        this->_driver->SetLED(start + 1, color);
    break;
    case 0:
        this->_driver->SetLED(start + 0, color);
    break;
    }
}

void DisplayDriver::Goal(const uint8_t player,
                         const uint8_t no,
                         const rgb color) {
    switch (player) {
    case 0:
        this->GoalPlayer1(no, color);
    break;
    case 1:
        this->GoalPlayer2(no, color);
    break;
    case 2:
        this->GoalPlayer3(no, color);
    break;
    case 3:
        this->GoalPlayer4(no, color);
    break;
    default:
        return;
    }
}

void DisplayDriver::GoalPlayer1(const uint8_t count,
                                const rgb color) {
    switch (count) {
    case 3:
        this->_driver->SetLED(9, color);
    break;
    case 2:
        this->_driver->SetLED(10, color);
    break;
    case 1:
        this->_driver->SetLED(11, color);
    break;
    case 0:
        this->_driver->SetLED(12, color);
    break;
    }
}

void DisplayDriver::GoalPlayer2(const uint8_t count,
                                const rgb color) {
    switch (count) {
    case 3:
        this->_driver->SetLED(63, color);
    break;
    case 2:
        this->_driver->SetLED(64, color);
    break;
    case 1:
        this->_driver->SetLED(65, color);
    break;
    case 0:
        this->_driver->SetLED(66, color);
    break;
    }
}

void DisplayDriver::GoalPlayer3(const uint8_t count,
                                const rgb color) {
    switch (count) {
    case 3:
        this->_driver->SetLED(45, color);
    break;
    case 2:
        this->_driver->SetLED(46, color);
    break;
    case 1:
        this->_driver->SetLED(47, color);
    break;
    case 0:
        this->_driver->SetLED(48, color);
    break;
    }
}

void DisplayDriver::GoalPlayer4(const uint8_t count,
                                const rgb color) {
    switch (count) {
    case 3:
        this->_driver->SetLED(27, color);
    break;
    case 2:
        this->_driver->SetLED(28, color);
    break;
    case 1:
        this->_driver->SetLED(29, color);
    break;
    case 0:
        this->_driver->SetLED(30, color);
    break;
    }
}

void DisplayDriver::Field(const uint8_t pos,
                          const rgb color) {
    uint8_t mapping[ROUND_LEN] = { 4,
                                   5,
                                   6,
                                   7,
                                   8,
                                   71,
                                   70,
                                   69,
                                   68,
                                   67,
                                   59,
                                   60,
                                   61,
                                   62,
                                   54,
                                   53,
                                   52,
                                   51,
                                   50,
                                   49,
                                   40,
                                   41,
                                   42,
                                   43,
                                   44,
                                   35,
                                   34,
                                   33,
                                   32,
                                   31,
                                   23,
                                   24,
                                   25,
                                   26,
                                   18,
                                   17,
                                   16,
                                   15,
                                   14,
                                   13

    };

    uint8_t led = mapping[pos % ROUND_LEN];
    this->_driver->SetLED(led, color);
}
