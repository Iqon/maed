/**
 * \file	rgb.h
 * \author 	Aiko Isselhard
 * \brief	Represents a rgb color with 3 8-Bit channels.
 */

#ifndef RGB_H_
#define RGB_H_

#include <inttypes.h>

/**
 * \brief 	Simple datastructure for interaction with the display. Allows easy manipulation
 * 			of the rgb color channels.
 */
typedef struct rgb_t {
    uint8_t r;
    uint8_t g;
    uint8_t b;

    bool operator==(const rgb_t &c1) const {
        return this->r == c1.r && this->b == c1.b && this->g == c1.g;
    }

    bool operator!=(const rgb_t &c1) const {
        return !(this->operator ==(c1));
    }

    rgb_t operator *(const float f) const {
        struct rgb_t result;
        result.r = this->r * f;
        result.g = this->g * f;
        result.b = this->b * f;
        return result;
    }

} rgb, *p_rgb;

#endif /* RGB_H_ */
