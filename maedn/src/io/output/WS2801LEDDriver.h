/**
 * \file	WS2801LEDDriver.h
 * \brief	Basic LED Driver. Updates always all LEDs, even if there were no changes.
 * 			This allows the usage of only one display buffer, but updates take longer.
 * \author 	Aiko Isselhard
 */

#ifndef FULLLEDDRIVER_H_
#define FULLLEDDRIVER_H_

#include <stm32f4xx_spi.h>
#include <global.h>
#include <io/output/ILEDDriver.h>
#include <io/output/rgb.h>

/**
 * \brief	Number of leds in the current stripe setup. Equals the number of writes to completly fill the matrix.
 * 79
 */
#define NUM_LEDS	79

/**
 * \brief	The dice leds are much brighter. This factor is used to darken the dice.
 */
#define DICE_BRIGHTNESS 1.0f

/**
 * @brief	Basic LED Driver. Updates always all LEDs, even if there were no changes.
 * 			This allows the usage of only one display buffer, but updates take longer.
 */
class WS2801LEDDriver : public ILEDDriver {
public:
    virtual ~WS2801LEDDriver( );

    virtual void SetLED(uint16_t pos,
                        uint8_t r,
                        uint8_t g,
                        uint8_t b);

    virtual void Update(void);

    virtual void Clear(void);

    virtual void SetBrightness(float brightness);

    /**
     * \brief Returns the singleton instance of the WS2801LEDDriver.
     *
     * \returns the singleton instance of WS2801LEDDriver.
     */
    static WS2801LEDDriver * GetInstance( ) {
        static WS2801LEDDriver *driver = new WS2801LEDDriver();
        return driver;
    }

private:
    WS2801LEDDriver( );

    uint16_t _led_count;
    rgb _drawingBuffer[NUM_LEDS];
    float _brightness;

    void InitSPI(void);
    void SPISend(uint8_t data);
};

#endif /* FULLLEDDRIVER_H_ */
