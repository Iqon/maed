/**
 * \file    colors.h
 * \author 	Aiko Isselhard
 * @brief	Default definions for colors.
 * */

#ifndef COLORS_H_
#define COLORS_H_

#include <io/output/rgb.h>

#define COLOR_OFF COLOR_BLACK

#define COLOR_PLAYER0 COLOR_RED
#define COLOR_PLAYER1 COLOR_GREEN
#define COLOR_PLAYER2 COLOR_BLUE
#define COLOR_PLAYER3 COLOR_YELLOW

extern rgb COLOR_WHITE;
extern rgb COLOR_BLACK;

extern rgb COLOR_RED;
extern rgb COLOR_GREEN;
extern rgb COLOR_BLUE;
extern rgb COLOR_YELLOW;
extern rgb COLOR_PINK;

#endif /* COLORS_H_ */
