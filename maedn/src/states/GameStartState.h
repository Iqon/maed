/**
 * \file	GameStartState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Main entry state. Starts a new game, resets and restarts the game.
 * */

#ifndef GAMESTARTSTATE_H_
#define GAMESTARTSTATE_H_

#include <global.h>

#include <state/EnterReturnState.h>
#include <states/gamestart/SelectNoOfPlayerState.h>
#include <states/dice/DiceState.h>

#include <states/game/dice/SelectRollDiceState.h>
#include <states/game/preparemove/FindMovesState.h>

/**
 * \brief	Main entry state. Starts a new game, resets and restarts the game.
 */
class GameStartState : public State {
public:
    GameStartState( );
    virtual ~GameStartState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );
};

#endif /* GAMESTARTSTATE_H_ */
