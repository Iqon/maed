/**
 * \file 	StartMoveState.h
 * \author	Iqon
 * \date: 	09.07.2017
 * \brief	ADD DESCRIPTION.
 **/

#ifndef STATES_GAME_STARTMOVESTATE_H_
#define STATES_GAME_STARTMOVESTATE_H_

#include <global.h>

#include <states/dice/DiceResult.h>

#include <state/StateBase.h>
#include <state/EnterReturnState.h>

#include <states/game/preparemove/FindMovesState.h>
#include <states/game/dice/SelectRollDiceState.h>

class StartMoveState : public EnterReturnState<DiceResult, DiceResult>{
public:
    StartMoveState( );
    virtual ~StartMoveState( );

    virtual void Enter(DiceResult *result);
    virtual void Update(uint32_t diff);
    virtual DiceResult *Leave( );

private:
    DiceResult *_result;
};

#endif /* STATES_GAME_STARTMOVESTATE_H_ */
