/**
 * \file	FindNextPlayerState.h
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Finds the next active player and goes into the SelectRollDiceState.
 * */

#ifndef FINDNEXTPLAYERSTATE_H_
#define FINDNEXTPLAYERSTATE_H_

#include <global.h>
#include <state/State.h>

#include <states/game/dice/SelectRollDiceState.h>

/**
 * \brief 	Finds the next active player and goes into the SelectRollDiceState.
 */
class FindNextPlayerState : public State {
public:
    FindNextPlayerState( );
    virtual ~FindNextPlayerState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );
};

#endif /* FINDNEXTPLAYERSTATE_H_ */
