/**
 * \file	FindNextPlayerState.cpp
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/FindNextPlayerState.h>

FindNextPlayerState::FindNextPlayerState( ) {
}

FindNextPlayerState::~FindNextPlayerState( ) {
}

void FindNextPlayerState::Enter( ) {
    // TODO: Check if somebody won.
    uint8_t player = this->Game()->GetCurrentPlayer();

    do {
        player = (player + 1) % this->Game()->GetPlayerCount();
    } while (!this->Game()->GetPlayer(player)->IsActive());

    this->Game()->SetCurrentPlayer(player);
    StateBase *state = new SelectRollDiceState();
    this->EnterSubState(state);
}

void FindNextPlayerState::Update(uint32_t diff) {
}

void FindNextPlayerState::Leave( ) {
}
