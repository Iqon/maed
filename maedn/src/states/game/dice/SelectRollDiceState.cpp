/**
 * \file	SelectRollDiceState.cpp
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/dice/SelectRollDiceState.h>

SelectRollDiceState::SelectRollDiceState( ) {
}

SelectRollDiceState::~SelectRollDiceState( ) {
}

void SelectRollDiceState::Enter( ) {
    IPlayer *player = this->Game()->GetPlayer(this->Game()->GetCurrentPlayer());

    for (uint8_t i = 0x00; i < 0x04; ++i) {
        IMan *man = player->GetMan(i);
        if (man->GetFieldType() == FieldTypeField) // one in the field -> roll only one time
                {
            StateBase *state = new RollDice1TimeState();
            this->EnterNextState(state);
            return;
        }
    }

    // no man on the field -> roll up to 3 times
    StateBase *state = new RollDice3TimesState();
    this->EnterNextState(state);
}

void SelectRollDiceState::Update(uint32_t diff) {
}

void SelectRollDiceState::Leave( ) {
}
