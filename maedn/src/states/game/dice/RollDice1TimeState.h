/**
 * \file	RollDice1TimeState.h
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Allows the player to roll the dice once. Calls the RollDiceState.
 * */

#ifndef ROLL1TIMESTATE_H_
#define ROLL1TIMESTATE_H_

#include <state/EnterReturnState.h>

#include <states/dice/DiceResult.h>
#include <states/dice/DiceState.h>

#include <states/game/FindNextPlayerState.h>
#include <states/game/StartMoveState.h>

/**
 * \brief	Allows the player to roll the dice once. Calls the RollDiceState.
 */
class RollDice1TimeState : public EnterReturnState<DiceResult, DiceResult> {
public:
    RollDice1TimeState( );
    virtual ~RollDice1TimeState( );

    virtual void Enter(DiceResult *result);
    virtual void Update(uint32_t diff);
    virtual DiceResult *Leave( );

private:
    DiceResult *_result;
};

#endif /* ROLL1TIMESTATE_H_ */
