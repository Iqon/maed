/**
 * \file	SelectRollDiceState.h
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	The throw state used in the game. If there is any man on the field
 * 			the player is only allowed to throw one time (RollDice1TimeState).
 * 			Otherwise he is allowed to throw three times. (RollDice3TimesState).
 * 			Will return to the calling state after this.
 * */

#ifndef SELECTROLLDICESTATE_H_
#define SELECTROLLDICESTATE_H_

#include <state/State.h>
#include <game/IPlayer.h>
#include <states/game/dice/RollDice1TimeState.h>
#include <states/game/dice/RollDice3TimesState.h>

/**
 * \brief	The throw state used in the game. If there is any man on the field
 * 			the player is only allowed to throw one time (RollDice1TimeState).
 * 			Otherwise he is allowed to throw three times. (RollDice3TimesState).
 * 			Will return to the calling state after this.
 */
class SelectRollDiceState : public State {
public:
    SelectRollDiceState( );
    virtual ~SelectRollDiceState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );
};

#endif /* SELECTROLLDICESTATE_H_ */
