/**
 * \file	RollDice3TimesState.cpp
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/dice/RollDice3TimesState.h>

RollDice3TimesState::RollDice3TimesState( ) {
    this->_result = new DiceResult();
    this->_retry = 0x00;
}

RollDice3TimesState::~RollDice3TimesState( ) {
    delete this->_result;
}

void RollDice3TimesState::Enter(DiceResult *result) {
    if (result != 0x00) {
        this->_result->dice = result->dice;
        this->_result->multiplyer = result->multiplyer;

        if (this->_result->dice == 0x06) {
            StateBase *findMoves = new StartMoveState();
            this->EnterNextState(findMoves);
            return;
        }
    }

    if (this->_retry < 0x03) {
        this->_retry++;
        StateBase *state = new DiceState(this->Game()->GetCurrentPlayer());
        this->EnterSubState(state);
    } else {
        StateBase *state = new FindNextPlayerState();
        this->EnterNextState(state);
        return;
    }
}

void RollDice3TimesState::Update(uint32_t diff) {
}

DiceResult* RollDice3TimesState::Leave( ) {
    return this->_result;
}
