/**
 * \file	RollDice3TimesState.h
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Allows the player to throw up to 3 times (unless he throws a 6).
 * 			Will return the DiceResult. Also accepts a DiceResult because
 * 			it will reenter itself and needs the previous value to count.
 * 			Calls DiceState to start rolling. If the rolling failed 3 times in a
 * 			row to achive a 6, the call is forwarded to the FindNextPlayerState.
 * */

#ifndef ROLLDICE3TIMESSTATE_H_
#define ROLLDICE3TIMESSTATE_H_

#include <state/EnterReturnState.h>

#include <states/dice/DiceResult.h>
#include <states/dice/DiceState.h>

#include <states/game/FindNextPlayerState.h>
#include <states/game/StartMoveState.h>

/**
 * \brief	Allows the player to throw up to 3 times (unless he throws a 6).
 * 			Will return the DiceResult. Also accepts a DiceResult because
 * 			it will reenter itself and needs the previous value to count.
 * 			Calls DiceState to start rolling. If the rolling failed 3 times in a
 * 			row to achive a 6, the call is forwarded to the FindNextPlayerState.
 */
class RollDice3TimesState : public EnterReturnState<DiceResult, DiceResult> {
public:
    RollDice3TimesState( );
    virtual ~RollDice3TimesState( );

    virtual void Enter(DiceResult *result);
    virtual void Update(uint32_t diff);
    virtual DiceResult *Leave( );

private:
    DiceResult *_result;
    uint8_t _retry;
};

#endif /* ROLLDICE3TIMESSTATE_H_ */
