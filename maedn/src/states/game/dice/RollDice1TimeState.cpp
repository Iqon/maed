/**
 * \file	RollDice1TimeState.cpp
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/dice/RollDice1TimeState.h>

RollDice1TimeState::RollDice1TimeState( ) {
    this->_result = new DiceResult();
}

RollDice1TimeState::~RollDice1TimeState( ) {
    delete this->_result;
}

void RollDice1TimeState::Enter(DiceResult *result) {
    if (result != 0x00) {
        this->_result->dice = result->dice;
        this->_result->multiplyer = result->multiplyer;

        StateBase *findMoves = new StartMoveState();
        this->EnterNextState(findMoves);
        return;
    }

    StateBase *state = new DiceState(this->Game()->GetCurrentPlayer());
    this->EnterSubState(state);
}

void RollDice1TimeState::Update(uint32_t diff) {
}

DiceResult *RollDice1TimeState::Leave( ) {
    return this->_result;
}
