/**
 * \file 	NormalMoveState.cpp
 * \author	Aiko Isselhard
 * \date: 	09.07.2017
 * \brief	Implementation
 **/

#include <states/game/executemove/NormalMoveState.h>

NormalMoveState::NormalMoveState( ) {
    this->_move = NULL;
    this->_count = 0x00;
    this->_manToPunish = NULL;
}

NormalMoveState::~NormalMoveState( ) {
    if (this->_move != NULL) {
        delete this->_move;
    }
}

void NormalMoveState::Enter(Move* move) {
    if (move != NULL) { // means we come from a own sub state.
        this->_move = new Move(move);
    }
}

void NormalMoveState::Update(uint32_t diff) {
    if (this->_manToPunish != NULL) {
        StateBase *walkHome = new WalkManHomeState(this->_manToPunish);
        this->EnterSubState(walkHome);
        this->_manToPunish = NULL;
        return;
    }

    if (this->_count >= MOVE_MAX_STEP_COUNT) {
        this->Stop();
        return;
    }

    Step *step = this->_move->GetStep(this->_count++);
    if (!step->IsActive()) {
        this->Stop();
        return;
    }

    FieldCell *stopCell = this->Game()->GetField(step->GetStopPosition());
    this->_manToPunish = stopCell->GetMan();

    IMan *man = this->_move->GetMan();
    StateBase *moveMan = new MoveManState(step, man);
    this->EnterSubState(moveMan);
}

void NormalMoveState::Leave( ) {
}
