/**
 * \file 	MoveManState.cpp
 * \author	Aiko Isselhard
 * \date: 	09.07.2017
 * \brief	Implementation.
 **/

#include <states/game/executemove/MoveManState.h>

MoveManState::MoveManState(const Step *step,
                           IMan *man) {
    this->_step = new Step(step);
    this->_pos = this->_step->GetStartPosition();
    this->_fieldType = this->_step->GetStartField();
    this->_man = man;
    this->_player = this->_man->GetParentPlayer();
    this->_color = this->_man->GetParentPlayer()->GetColor();
}

MoveManState::~MoveManState( ) {
    delete this->_step;
}

void MoveManState::Enter( ) {
    this->_man->ClearPosition();
}

void MoveManState::Leave( ) {

}

void MoveManState::Update(uint32_t diff) {
    if (this->_runningTime > 2 * WALK_STEP_DELAY) this->_runningTime = WALK_STEP_DELAY;
    if (this->_runningTime > WALK_STEP_DELAY) {
        this->_runningTime -= WALK_STEP_DELAY;

        if (this->_pos == this->_step->GetStopPosition() && this->_fieldType == this->_step->GetStopField()) { // END -> Player in house
            this->_man->SetPosition(this->_pos, this->_fieldType);
            this->Stop();
            return;
        }

        MovePlayer();
    }

    Draw();
}

void MoveManState::MovePlayer( ) {
    switch (this->_fieldType) {
    case FieldTypeHouse: {
        this->_fieldType = FieldTypeField;
        this->_pos = this->_player->GetStartPos();
    } break;
    case FieldTypeGoal: {
        this->_pos = this->_pos + 1;
    } break;
    case FieldTypeField: {
        uint8_t playerEnd = (this->_player->GetStartPos() + (ROUND_LEN - 1)) % ROUND_LEN;
        if (this->_pos == playerEnd) {
            this->_pos = 0x00;
            this->_fieldType = FieldTypeGoal;
        } else {
            this->_pos = (this->_pos + 1) % ROUND_LEN;
        }
    } break;
    case FieldTypeHidden: {
    } break;
    }
}

void MoveManState::Draw( ) {
    switch (this->_fieldType) {
    case FieldTypeHouse: {
        this->Display()->House(this->_player->GetPlayer(), this->_pos, this->_color);
    } break;
    case FieldTypeGoal: {
        this->Display()->Goal(this->_player->GetPlayer(), this->_pos, this->_color);
    } break;
    case FieldTypeField: {
        this->Display()->Field(this->_pos, this->_color);
    } break;
    case FieldTypeHidden: {
    } break;
    }
}
