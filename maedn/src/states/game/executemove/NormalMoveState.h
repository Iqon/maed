/**
 * \file 	NormalMoveState.h
 * \author	Iqon
 * \date: 	09.07.2017
 * \brief	ADD DESCRIPTION.
 **/

#ifndef STATES_GAME_EXECUTEMOVE_NORMALMOVESTATE_H_
#define STATES_GAME_EXECUTEMOVE_NORMALMOVESTATE_H_

#include <global.h>

#include <state/StateBase.h>
#include <state/EnterState.h>

#include <states/game/executemove/WalkManHomeState.h>
#include <states/game/executemove/MoveManState.h>

#include <game/Move.h>
#include <game/IMan.h>
#include <game/Man.h>

enum NormalMove {

};

/**
 * \brief   Executes the move. Moves the man and kicks all player out, if there was a hit.
 **/
class NormalMoveState : public EnterState<Move> {
public:
    NormalMoveState( );
    virtual ~NormalMoveState( );

    virtual void Enter(Move *move);
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    Move *_move;
    uint8_t _count;
    const IMan * _manToPunish;
};

#endif /* STATES_GAME_EXECUTEMOVE_NORMALMOVESTATE_H_ */
