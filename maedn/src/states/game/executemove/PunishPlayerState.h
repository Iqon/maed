/**
 * \file	PunishPlayerState.h
 * \date	Jul 19, 2016
 * \author 	Aiko Isselhard
 * \brief	Punishes a player for making a mistake.
 * */

#ifndef PUNISHPLAYERSTATE_H_
#define PUNISHPLAYERSTATE_H_

#include <global.h>

#include <state/StateBase.h>
#include <state/EnterState.h>

#include <game/Move.h>

#include <states/game/FindNextPlayerState.h>
#include <states/game/executemove/WalkManHomeState.h>

/**
 * \brief 	Punishes a player for making a mistake.
 */
class PunishPlayerState : public EnterState<Move> {
public:
    PunishPlayerState( );
    virtual ~PunishPlayerState( );

    virtual void Enter(Move *move);
    virtual void Update(uint32_t diff);
    virtual void Leave( );
private:
};

#endif /* PUNISHPLAYERSTATE_H_ */
