/**
 * \file 	ChangeColorState.h
 * \author	Aiko Isselhard
 * \date: 	09.07.2017
 * \brief	Changes the given players color to PINK with a short animation. Is a forwarding state.
 **/

#ifndef STATES_GAME_EXECUTEMOVE_CHANGECOLORSTATE_H_
#define STATES_GAME_EXECUTEMOVE_CHANGECOLORSTATE_H_

#include <global.h>

#include <state/StateBase.h>
#include <state/ForwardingState.h>

#include <game/Move.h>

#define CHANGE_COLOR_RUNNING_TIME      (4 * S_TICKS)
#define CHANGE_COLOR_START_STEP        ((uint16_t)500 * MS_TICKS)
#define CHANGE_COLOR_STEP_REDUCE_TICK  ((uint16_t)100 * MS_TICKS)
#define CHANGE_COLOR_STEP_REDUCE       ((uint16_t)10 * MS_TICKS)

class ChangeColorState : public ForwardingState<Move> {
public:
    ChangeColorState(uint8_t player,
                     StateBase *next)
            : ForwardingState<Move>(next) {
        this->_player = player;
    }

    virtual ~ChangeColorState( ) {
    }

    virtual void Enter( ) {
    }

    virtual void Update(uint32_t diff) {
        IPlayer *player = this->Game()->GetPlayer(this->_player);

        if (this->_runningTime > CHANGE_COLOR_RUNNING_TIME) {
            for(int i = 0x00; i < this->Game()->GetPlayerCount(); ++i) {
                this->Game()->GetPlayer(i)->ResetColor();
            }

            player->SetColor(COLOR_PINK);
            this->Stop();
            return;
        }

        uint16_t rotate = CHANGE_COLOR_START_STEP - (uint16_t) (((float) this->_runningTime / (float)CHANGE_COLOR_STEP_REDUCE_TICK) * (float) CHANGE_COLOR_STEP_REDUCE);
        uint8_t pos = (this->_runningTime / rotate) % 2;
        if(pos) {
            player->SetColor(COLOR_PINK);
        }
        else {
            player->ResetColor();
        }
    }

    virtual void Leave( ) {
    }

private:
    uint8_t  _player;
    rgb _color;
};

#endif /* STATES_GAME_EXECUTEMOVE_CHANGECOLORSTATE_H_ */
