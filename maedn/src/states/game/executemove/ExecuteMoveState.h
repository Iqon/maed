/**
 * \file	ExecuteMoveState.h
 * \date	Jul 19, 2016
 * \author 	Aiko Isselhard
 * \brief	Executes the move.
 * */

#ifndef EXECUTEMOVESTATE_H_
#define EXECUTEMOVESTATE_H_

#include <global.h>

#include <state/StateBase.h>
#include <state/EnterReturnState.h>

#include <states/game/executemove/PunishPlayerState.h>
#include <states/game/executemove/ChangeColorState.h>
#include <states/game/executemove/NormalMoveState.h>

#include <game/Move.h>
#include <game/MoveCollection.h>

/**
 * \brief	Executes the move.
 */
class ExecuteMoveState : public EnterReturnState<Move, Move> {
public:
    ExecuteMoveState(MoveCollection *moves);
    virtual ~ExecuteMoveState( );

    virtual void Enter(Move *obj);
    virtual void Update(uint32_t diff);
    virtual Move *Leave( );

private:
    MoveCollection *_moves;
    Move *_move;
};

#endif /* EXECUTEMOVESTATE_H_ */
