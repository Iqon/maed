/**
 * \file	ExecuteMoveState.cpp
 * \date	Jul 19, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/executemove/ExecuteMoveState.h>

ExecuteMoveState::ExecuteMoveState(MoveCollection *moves) {
    this->_move = NULL;
    this->_moves = new MoveCollection();
    moves->CopyTo(this->_moves);
}

ExecuteMoveState::~ExecuteMoveState( ) {
    delete this->_moves;
    if (this->_move != NULL) {
        delete this->_move;
    }
}

void ExecuteMoveState::Enter(Move *obj) {
    this->_move = new Move(obj);
    for (uint8_t i = 0x00; i < this->_moves->GetCount(); ++i) {
        Move *cmp = this->_moves->Next();
        if ((*cmp) != (*obj)) {
            if (cmp->GetHitCount() > obj->GetHitCount()) {
                StateBase *state = new PunishPlayerState();
                StateBase *color = new ChangeColorState(0, state);
                this->EnterNextState(color);
                return;
            }
        }
    }

    StateBase *state = new NormalMoveState();
    this->EnterNextState(state);
}

void ExecuteMoveState::Update(uint32_t diff) {
}

Move *ExecuteMoveState::Leave( ) {
    return this->_move;
}
