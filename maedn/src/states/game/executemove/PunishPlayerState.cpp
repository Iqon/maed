/**
 * \file	PunishPlayerState.cpp
 * \date	Jul 19, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/executemove/PunishPlayerState.h>

PunishPlayerState::PunishPlayerState( ) {

}

PunishPlayerState::~PunishPlayerState( ) {

}

void PunishPlayerState::Enter(Move *move) {
    IMan *man = move->GetMan();
    StateBase *state = new WalkManHomeState(man);
    this->EnterNextState(state);
}

void PunishPlayerState::Update(uint32_t diff) {
}

void PunishPlayerState::Leave( ) {
}
