/**
 * \file	WalkManHomeState.cpp
 * \date	Jul 19, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/executemove/WalkManHomeState.h>

WalkManHomeState::WalkManHomeState(const IMan *man) {
    this->_man = (IMan *) man;
    this->_player = man->GetParentPlayer();
    this->_playerStart = this->_player->GetStartPos();
    this->_pos = man->GetPosition();
    this->_color = this->_player->GetColor();

    this->_man->ClearPosition();
}

WalkManHomeState::~WalkManHomeState( ) {
}

void WalkManHomeState::Enter( ) {
}

void WalkManHomeState::Update(uint32_t diff) {
    if (this->_runningTime > 2 * WALK_STEP_DELAY) this->_runningTime = WALK_STEP_DELAY;
    if (this->_runningTime > WALK_STEP_DELAY) {
        this->_runningTime -= WALK_STEP_DELAY;

        if (this->_pos == this->_playerStart) { // END -> Player in house
            const uint8_t housePos = this->GetFreeHouseField();
            this->_man->SetPosition(housePos, FieldTypeHouse);
            this->Stop();
            return;
        }

        if(this->_pos == 0x00) {
            this->_pos = ROUND_LEN;
        }
        this->_pos = (this->_pos - 1);
    }

    this->Display()->Field(this->_pos, this->_color);
}

const uint8_t WalkManHomeState::GetFreeHouseField() const {
    bool used[0x04];
    memset(used, FALSE, sizeof(bool) * 0x04);

    for(uint8_t i = 0x00; i < 0x04; ++i) {
        IMan *man = this->_player->GetMan(i);
        FieldType type = man->GetFieldType();
        if(type == FieldTypeHouse) {
            uint8_t position = man->GetPosition();
            used[position] = TRUE;
        }
    }

    for(uint8_t i = 0x00; i < 0x04; ++i) {
        if(used[i] == FALSE) {
            return i;
        }
    }

    // ERROR. This should never happen.
    return 0x04;

}

void WalkManHomeState::Leave( ) {
}
