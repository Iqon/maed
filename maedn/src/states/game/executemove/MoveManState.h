/**
 * \file	MoveManState.h
 * \date	Jul 19, 2016
 * \author 	Aiko Isselhard
 * \brief	Executes the selected move.
 * */

#ifndef MOVEMANSTATE_H_
#define MOVEMANSTATE_H_

#include <global.h>

#include <state/StateBase.h>
#include <state/State.h>

#include <game/Step.h>
#include <game/IMan.h>

/**
 * \brief	Executes the selected move.
 */
class MoveManState : public State {
public:
    MoveManState(const Step *step,
                 IMan *man);
    virtual ~MoveManState();

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    const Step *_step;
    IMan *_man;
    IPlayer *_player;
    uint8_t _pos;
    FieldType _fieldType;
    rgb _color;

    void MovePlayer( );
    void Draw( ) ;
};

#endif /* MOVEMANSTATE_H_ */
