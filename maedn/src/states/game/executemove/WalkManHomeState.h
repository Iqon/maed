/**
 * \file	WalkManHomeState.h
 * \date	Jul 19, 2016
 * \author 	Aiko Isselhard
 * \brief	Walks a man home as punishment or after a man was beaten.
 * */

#ifndef WALKMANHOMESTATE_H_
#define WALKMANHOMESTATE_H_

#include <global.h>

#include <state/StateBase.h>
#include <state/State.h>

#include <game/IMan.h>
#include <game/IPlayer.h>
#include <io/output/rgb.h>

/**
 * \brief	Walks a man home as punishment or after a man was beaten.
 */
class WalkManHomeState : public State {
public:
    WalkManHomeState(const IMan *man);
    virtual ~WalkManHomeState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    IMan *_man;
    IPlayer *_player;
    uint8_t _playerStart;
    uint8_t _pos;
    rgb _color;

    const uint8_t GetFreeHouseField() const;
};

#endif /* WALKMANHOMESTATE_H_ */
