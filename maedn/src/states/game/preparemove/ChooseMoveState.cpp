/**
 * \file	ChooseMoveState.cpp
 * \date	Jul 17, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/preparemove/ChooseMoveState.h>

ChooseMoveState::ChooseMoveState( ) {
    this->_moveAnimPos = 0x00;
    this->_state = true;
    this->_displayMove = NULL;
    this->_moves = new MoveCollection();
    this->_move = NULL;
}

ChooseMoveState::~ChooseMoveState( ) {
    delete this->_moves;
    if (this->_move != NULL) {
        delete this->_move;
    }
}

void ChooseMoveState::Enter(MoveCollection *obj) {
    this->_player = this->Game()->GetCurrentPlayer();

    obj->CopyTo(this->_moves);
    uint8_t count = _moves->GetCount();

    if (count == 0x00) { // no possible moves. Select next player.
        // TODO: Looser animation for no possible move.
        this->Stop();
    } else if (count == 0x01) { // only one move, select this one and play it.
        this->_move = new Move(this->_moves->GetCurrentMove());
        StateBase *state = new ExecuteMoveState(this->_moves);
        this->EnterNextState(state);
        return;
    }

    // Otherwise let player choose the move.
    this->_move = this->_moves->GetCurrentMove();
    this->_displayMove = new DisplayMoveState(this->_move);
    this->AddChildState(this->_displayMove);
}

void ChooseMoveState::Update(uint32_t diff) {
    ButtonState btn1 = this->Input()->GetPlayerButton(this->_player, ButtonTypeConfirm);
    ButtonState btn2 = this->Input()->GetPlayerButton(this->_player, ButtonTypeChange);

    if (btn1 == ButtonStateReleased) {
        this->_move = new Move(this->_moves->GetCurrentMove());
        StateBase *state = new ExecuteMoveState(this->_moves);
        this->EnterNextState(state);
        return;
    }

    if (btn2 == ButtonStateReleased) {
        this->_moveAnimPos = 0x01;
        this->_runningTime = 0x00;
        this->_state = true;

        this->_move = this->_moves->Next();
        this->_displayMove->Stop();
        this->_displayMove = new DisplayMoveState(this->_move);
        this->AddChildState(this->_displayMove);
    }

    MoveAnimation();
}

Move *ChooseMoveState::Leave( ) {
    return this->_move;
}

void ChooseMoveState::MoveAnimation( ) {
    if (this->_move->GetDice() < 2) return;

    if (this->_state) {
        if (this->_runningTime > CHOOSE_MOVE_STATE_STEP) {
            this->_runningTime = this->_runningTime - CHOOSE_MOVE_STATE_STEP;
            this->_moveAnimPos++;

            if (this->_moveAnimPos > this->_move->GetDice()) {
                this->_state = false;
                return;
            }
        }

        if (this->_moveAnimPos < this->_move->GetDice()) DrawMoveAnim(this->_moveAnimPos);

        if (this->_moveAnimPos > 1) DrawMoveAnim(this->_moveAnimPos - 1);
    } else {
        if (this->_runningTime > CHOOSE_MOVE_STATE_WAIT) {
            this->_moveAnimPos = 0x01;
            this->_runningTime = 0x00;
            this->_state = true;
        }
    }
}

void ChooseMoveState::DrawMoveAnim(uint8_t moveAnimPos) {
    DisplayDriver *driver = this->Display();
    const IPlayer * player = this->Game()->GetPlayer(this->_player);
    const rgb color = player->GetColor();

    for (uint8_t i = 0x00; i < MOVE_MAX_STEP_COUNT; ++i) {
        Step *step = this->_move->GetStep(i);
        if (!step->IsActive()) continue;

        StepType type = step->GetStepType();
        if (type == StepTypeField) {
            uint8_t pos = step->GetStartPosition() + moveAnimPos;
            driver->Field(pos, color);
        } else if (type == StepTypeTransitionFieldGoal) {
            uint8_t pos = step->GetStartPosition();
            uint8_t startPos = player->GetStartPos();
            uint8_t endPos = ((startPos + ROUND_LEN - 1) % ROUND_LEN);

            if ((pos < endPos) &&							// start is before goal
                    ((pos + this->_moveAnimPos) > endPos))	// end is after goal
                    {												// -> move in goal
                uint8_t left = (pos + moveAnimPos) - endPos;
                driver->Goal(this->_player, left, color);
            } else // -> move fits in the field
            {
                driver->Field(pos + moveAnimPos, color);
            }
        }
    }
}
