/**
 * \file	DisplayMoveState.h
 * \date	Jul 17, 2016
 * \author 	Aiko Isselhard
 * \brief	Displays one possible move.
 * */

#ifndef DISPLAYMOVESTATE_H_
#define DISPLAYMOVESTATE_H_

#include <global.h>

#include <state/State.h>

#include <states/animations/PulsingState.h>

#include <game/Move.h>

/**
 * \brief	Displays one possible move.
 */
class DisplayMoveState : public State {
public:
    DisplayMoveState(Move * move);
    virtual ~DisplayMoveState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    uint8_t _player;
    Move *_move;
    PulsingState *_pulsingState;

    void DisplayJumps(const rgb color,
                      DisplayDriver *driver);
    void DisplayJump(const rgb color,
                     DisplayDriver *driver,
                     const FieldType type,
                     const uint8_t pos);
};

#endif /* DISPLAYMOVESTATE_H_ */
