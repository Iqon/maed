/**
 * \file	ChooseMoveState.h
 * \date	Jul 17, 2016
 * \author 	Aiko Isselhard
 * \brief	Allows the player to chose a move. If there is no possible move,
 * 			this state will continue to the FindNextPlayerState. If there is
 * 			only one possible state it will automatically select this state
 * 			entering ExecuteMoveState. Otherwise it will let the player select
 * 			any state and execute this state.
 * */

#ifndef CHOOSEMOVESTATE_H_
#define CHOOSEMOVESTATE_H_

#include <global.h>

#include <state/StateBase.h>
#include <state/EnterReturnState.h>

#include <states/game/FindNextPlayerState.h>
#include <states/game/preparemove/DisplayMoveState.h>
#include <states/game/executemove/ExecuteMoveState.h>

#include <game/Move.h>
#include <game/MoveCollection.h>

#define CHOOSE_MOVE_STATE_WAIT (2000 * MS_TICKS)
#define CHOOSE_MOVE_STATE_STEP (100  * MS_TICKS)

/**
 * \brief	Allows the player to chose a move. If there is no possible move,
 * 			this state will continue to the FindNextPlayerState. If there is
 * 			only one possible state it will automatically select this state
 * 			entering ExecuteMoveState. Otherwise it will let the player select
 * 			any state and execute this state.
 */
class ChooseMoveState : public EnterReturnState<MoveCollection, Move> {
public:
    ChooseMoveState( );
    virtual ~ChooseMoveState( );

    virtual void Enter(MoveCollection *obj);
    virtual void Update(uint32_t diff);
    virtual Move *Leave( );

private:
    bool _state;
    int8_t _moveAnimPos;

    uint8_t _player;
    DisplayMoveState *_displayMove;
    MoveCollection * _moves;
    Move *_move;

    void MoveAnimation( );
    void DrawMoveAnim(uint8_t moveAnimPos);
};

#endif /* CHOOSEMOVESTATE_H_ */
