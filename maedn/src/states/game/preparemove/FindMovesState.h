/**
 * \file	FindMovesState.h
 * \date	Jun 14, 2016
 * \author 	Aiko Isselhard
 * \brief	Accepts the DiceResult and finds all possible moves given the DiceResult and
 * 			the current board situation. Will return a MoveCollection and enter the
 * 			ChooseMoveState
 * */

#ifndef FINDMOVESSTATE_H_
#define FINDMOVESSTATE_H_

#include <global.h>
#include <limits.h>

#include <game/IPlayer.h>
#include <game/IMan.h>

#include <state/EnterReturnState.h>

#include <states/dice/DiceResult.h>
#include <states/game/preparemove/ChooseMoveState.h>

#include <game/MoveCollection.h>
#include <game/Move.h>

/**
 * \brief	Accepts the DiceResult and finds all possible moves given the DiceResult and
 * 			the current board situation. Will return a MoveCollection and enter the
 * 			ChooseMoveState
 */
class FindMovesState : public EnterReturnState<DiceResult, MoveCollection> {
public:
    FindMovesState( );
    virtual ~FindMovesState( );

    virtual void Enter(pDiceResult obj);
    virtual void Update(uint32_t diff);
    virtual MoveCollection *Leave( );

private:
    pDiceResult _dice;
    MoveCollection *_moves;

    bool MovePossible(const IPlayer * const player,
                      const FieldType targetType,
                      const uint8_t targetPos);
    bool CheckHit(const IPlayer * const player,
                  const uint8_t targetPos);
    void FindMoves(const IPlayer * const player);
    bool CheckField(const IPlayer * const player,
                    uint8_t *pos,
                    FieldType *type,
                    Step* step);
    bool CheckHouse(const IPlayer * const player,
                    uint8_t *pos,
                    FieldType *type,
                    Step* step,
                    bool *houseMove);
    bool CheckGoal(const IPlayer * const player,
                   uint8_t *pos,
                   FieldType *type,
                   Step* step);
};

#endif /* FINDMOVESSTATE_H_ */
