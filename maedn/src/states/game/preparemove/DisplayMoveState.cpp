/**
 * \file	DisplayMoveState.cpp
 * \date	Jul 17, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/preparemove/DisplayMoveState.h>

DisplayMoveState::DisplayMoveState(Move *move) {
    this->_move = new Move(move);

    this->_pulsingState = new PulsingState(RISING_TIME,
    DELAY_TIME);
    this->AddChildState(this->_pulsingState);
}

DisplayMoveState::~DisplayMoveState( ) {
    delete this->_move;
}

void DisplayMoveState::Enter( ) {
    this->_player = this->Game()->GetCurrentPlayer();
}

void DisplayMoveState::Update(uint32_t diff) {
    const rgb color = this->_pulsingState->GetColor(this->Game()->GetPlayer(this->_player)->GetColor());
    DisplayDriver *driver = this->Display();
    DisplayJumps(color, driver);
}

void DisplayMoveState::Leave( ) {
}

void DisplayMoveState::DisplayJumps(const rgb color,
                                    DisplayDriver *driver) {
    for (uint8_t i = 0x00; i < MOVE_MAX_STEP_COUNT; ++i) {
        Step *step = this->_move->GetStep(i);
        if (!step->IsActive()) continue;

        this->DisplayJump(color, driver, step->GetStartField(), step->GetStartPosition());
        this->DisplayJump(color, driver, step->GetStopField(), step->GetStopPosition());
    }
}

void DisplayMoveState::DisplayJump(const rgb color,
                                   DisplayDriver *driver,
                                   const FieldType type,
                                   const uint8_t pos) {
    switch (type) {
    case FieldTypeHidden:
    break;
    case FieldTypeHouse:
        driver->House(this->_player, pos, color);
    break;
    case FieldTypeGoal:
        driver->Goal(this->_player, pos, color);
    break;
    case FieldTypeField:
        FieldCell *cell = this->Game()->GetField(pos);
        const IPlayer *enemy = cell->GetPlayer();
        const IPlayer * player = this->Game()->GetPlayer(this->_player);
        if (enemy != player && enemy != null) {
            rgb merged = this->_pulsingState->GetMergedColor(this->Game()->GetPlayer(this->_player)->GetColor(), enemy->GetColor());
            driver->Field(pos, merged);
        } else {
            driver->Field(pos, color);
        }
    break;
    }
}
