/**
 * \file	FindMovesState.cpp
 * \date	Jun 14, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/game/preparemove/FindMovesState.h>

FindMovesState::FindMovesState( ) {
    this->_moves = new MoveCollection();
    this->_dice = new DiceResult();
}

FindMovesState::~FindMovesState( ) {
    delete this->_dice;
    delete this->_moves;
}

void FindMovesState::Enter(pDiceResult result) {
    this->_dice->dice = result->dice;
    this->_dice->multiplyer = result->multiplyer;

    uint8_t playerNo = this->Game()->GetCurrentPlayer();
    IPlayer *player = this->Game()->GetPlayer(playerNo);

    this->FindMoves(player);

    StateBase *state = new ChooseMoveState();
    this->EnterNextState(state);
}

void FindMovesState::Update(uint32_t diff) {
}

MoveCollection *FindMovesState::Leave( ) {
    return this->_moves;
}

bool FindMovesState::MovePossible(const IPlayer * const player,
                                  const FieldType targetType,
                                  const uint8_t targetPos) {
    if (targetType == FieldTypeHouse) {
        return false;
    } // should never happen.
    else if (targetType == FieldTypeGoal) {
        if (targetPos > 0x03) return false; // out of range

        for (uint8_t i = 0x00; i < 0x04; ++i) {
            IMan * man = player->GetMan(i);
            if (man->GetFieldType() == FieldTypeGoal && man->GetPosition() == targetPos) {
                return false;
            }
        }

        return true;
    } else if (targetType == FieldTypeField) {
        const FieldCell * const cell = this->Game()->GetField(targetPos);
        const IPlayer * const cellPlayer = cell->GetPlayer();
        return cellPlayer != player;
    }

    // should never happen.
    return false;
}

bool FindMovesState::CheckHit(const IPlayer * const player,
                              const uint8_t targetPos) {
    const FieldCell * const cell = this->Game()->GetField(targetPos);
    const IPlayer * const cellPlayer = cell->GetPlayer();
    return cellPlayer != player && cellPlayer != NULL;
}

void FindMovesState::FindMoves(const IPlayer * const player) {
    bool houseMove = false;
    for (uint8_t i = 0x00; i < 0x04; ++i) {
        IMan * const man = player->GetMan(i);
        Move * move = new Move(this->_dice->dice, man);

        FieldType type = man->GetFieldType();
        uint8_t pos = man->GetPosition();
        for (uint8_t j = 0x00; j < this->_dice->multiplyer; ++j) {
            Step * step = move->GetStep(j);
            step->SetStartField(type);
            step->SetStartPosition(pos);

            if (type == FieldTypeHouse && !houseMove && this->_dice->dice == 0x06) {
                if (!CheckHouse(player, &pos, &type, step, &houseMove)) {
                    j = UINT8_MAX - 1;
                    continue;
                }
            } else if (type == FieldTypeField) {
                if (!CheckField(player, &pos, &type, step)) {
                    j = UINT8_MAX - 1;
                    continue;
                } else if (CheckHit(player, pos)) {
                    move->IncrementHitCount();
                }
            } else if (type == FieldTypeGoal) {
                if (!CheckGoal(player, &pos, &type, step)) {
                    j = UINT8_MAX - 1;
                    continue;
                }
            }
        }

        if (move->GetStepCount() > 0x00) {
            this->_moves->AddMove(move);
        } else {
            delete move;
        }
    }
}

bool FindMovesState::CheckField(const IPlayer * const player,
                                uint8_t *pos,
                                FieldType *type,
                                Step* step) {
    uint8_t startPos = player->GetStartPos();
    uint8_t endPos = ((startPos + ROUND_LEN - 1) % ROUND_LEN);

    if ((*pos < endPos) &&							// start is before goal
            ((*pos + this->_dice->dice) > endPos))	// end is after goal
            {												// -> move in goal
        uint8_t left = (*pos + this->_dice->dice) - endPos;

        *type = FieldTypeGoal;
        *pos = left;

        if (!this->MovePossible(player, *type, *pos)) {
            return false;
        }

        step->SetStopField(*type);
        step->SetStopPosition(*pos);
        step->SetActive(true);
    } else // -> move fits in the field
    {
        *pos = *pos + this->_dice->dice;
        if (!this->MovePossible(player, *type, *pos)) {
            return false;
        }

        step->SetStopField(*type);
        step->SetStopPosition(*pos);
        step->SetActive(true);
    }

    return true;
}

bool FindMovesState::CheckHouse(const IPlayer * const player,
                                uint8_t *pos,
                                FieldType *type,
                                Step* step,
                                bool *houseMove) {
    *type = FieldTypeField;
    *pos = player->GetStartPos();

    if (!this->MovePossible(player, *type, *pos)) {
        return false;
    }

    *houseMove = true;
    step->SetStopField(*type);
    step->SetStopPosition(*pos);
    step->SetActive(true);
    return true;
}

bool FindMovesState::CheckGoal(const IPlayer * const player,
                               uint8_t *pos,
                               FieldType *type,
                               Step* step) {
    (*pos) = (*pos) + this->_dice->dice;

    if (!this->MovePossible(player, *type, *pos)) {
        return false;
    }

    step->SetStopField(*type);
    step->SetStopPosition(*pos);
    step->SetActive(true);
    return true;
}
