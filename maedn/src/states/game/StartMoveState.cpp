/**
 * \file 	StartMoveState.cpp
 * \author	Iqon
 * \date: 	09.07.2017
 * \brief	ADD DESCRIPTION.
 **/

#include <states/game/StartMoveState.h>

StartMoveState::StartMoveState( ) {
    this->_result = NULL;
}

StartMoveState::~StartMoveState( ) {
    if(this->_result != NULL) {
        delete this->_result;
    }
}

void StartMoveState::Enter(DiceResult* result) {
    if(result != NULL) {
        this->_result = new DiceResult();
        this->_result->dice = result->dice;
        this->_result->multiplyer = result->multiplyer;

        if(this->_result->dice == 0x06) {
            StateBase *findMoves = new FindMovesState();
            this->EnterSubState(findMoves);
            return;
        }

        // Not entering substate will result in coming back to FindNextPlayerState;
        StateBase *findMoves = new FindMovesState();
        this->EnterNextState(findMoves);
        return;
    }

    // if result == null, we started FindMovesState as sub state.
    // this means the same player rolls again.
    StateBase *findMoves = new SelectRollDiceState();
    this->EnterNextState(findMoves);
}

void StartMoveState::Update(uint32_t diff) {
}

DiceResult* StartMoveState::Leave( ) {
    return this->_result;
}
