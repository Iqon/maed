/**
 * \file	AnimateDisplayTestState.h
 * \date	05.01.2017
 * \author	Aiko Isselhard
 * \brief	Test animation for showing the complete display. Will add one led at a time.
 * */

#ifndef STATES_ANIMATEDISPLAYTESTSTATE_H_
#define STATES_ANIMATEDISPLAYTESTSTATE_H_

#include <global.h>

#include <state/State.h>

#define ANIMATEDISPLAYTESTSTATE_DELAY	(200 * MS_TICKS)

#define ANIMATEDISPLAYTESTSTATE_COLOR 	COLOR_WHITE

/**
 * \brief	Test animation for showing the complete display. Will add one led at a time.
 */
class AnimateDisplayTestState : public State {
public:
    AnimateDisplayTestState( );
    virtual ~AnimateDisplayTestState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    rgb _color;
    uint8_t _currentCount;

    void DisplayHouses(DisplayDriver* display);
    void DisplayGoals(DisplayDriver* display);
    void DisplayDice(DisplayDriver* display);
    void DisplayField(DisplayDriver* display);
};

#endif /* STATES_ANIMATEDISPLAYTESTSTATE_H_ */
