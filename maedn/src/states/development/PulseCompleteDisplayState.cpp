/**
 * \file	PulseCompleteDisplayState.cpp
 * \date	Jul 19, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/development/PulseCompleteDisplayState.h>

#define INC_COLOLR(color)					\
	{										\
		if(color == 0x00)					\
			color = ((0xFF / 2) - 1);		\
		else if(color <= ((0xFF / 2) - 1))	\
			color = 0xFF;					\
		else if(color <= 0xFF)				\
			color = 0x00;					\
	}

PulseCompleteDisplayState::PulseCompleteDisplayState( ) {
    this->_color = TESTDISPLAYSTATE_COLOR;
    this->_pulsingState = new PulsingState( RISING_TIME,
    RISING_TIME,
                                           DELAY_TIME, 1000 * MS_TICKS);
    this->AddChildState(this->_pulsingState);
}

PulseCompleteDisplayState::~PulseCompleteDisplayState( ) {
}

void PulseCompleteDisplayState::Enter( ) {
}

void PulseCompleteDisplayState::DisplayHouse(rgb color,
                                             DisplayDriver* display,
                                             uint8_t player) {
    display->House(player, 0x00, color);
    display->House(player, 0x01, color);
    display->House(player, 0x02, color);
    display->House(player, 0x03, color);
}

void PulseCompleteDisplayState::DisplayHouses(rgb color,
                                              DisplayDriver* display) {
    DisplayHouse(color, display, 0x00);
    DisplayHouse(color, display, 0x01);
    DisplayHouse(color, display, 0x02);
    DisplayHouse(color, display, 0x03);
}

void PulseCompleteDisplayState::DisplayGoal(rgb color,
                                            DisplayDriver* display,
                                            uint8_t player) {
    display->Goal(player, 0x00, color);
    display->Goal(player, 0x01, color);
    display->Goal(player, 0x02, color);
    display->Goal(player, 0x03, color);
}

void PulseCompleteDisplayState::DisplayGoals(rgb color,
                                             DisplayDriver* display) {
    DisplayGoal(color, display, 0x00);
    DisplayGoal(color, display, 0x01);
    DisplayGoal(color, display, 0x02);
    DisplayGoal(color, display, 0x03);
}

void PulseCompleteDisplayState::DisplayField(rgb color,
                                             DisplayDriver* display) {
    for (uint8_t i = 0x00; i < ROUND_LEN; ++i) {
        display->Field(i, color);
    }
}

void PulseCompleteDisplayState::Update(uint32_t diff) {
    if (this->Input()->GetPlayerButton(0, ButtonTypeConfirm) == ButtonStateReleased || this->Input()->GetPlayerButton(0, ButtonTypeChange) == ButtonStateReleased) {
        this->_color = COLOR_BLACK;
    }

    if (this->Input()->GetPlayerButton(1, ButtonTypeConfirm) == ButtonStateReleased) {
        INC_COLOLR(this->_color.r);
    }

    if (this->Input()->GetPlayerButton(2, ButtonTypeConfirm) == ButtonStateReleased || this->Input()->GetPlayerButton(2, ButtonTypeChange) == ButtonStateReleased) {
        INC_COLOLR(this->_color.g);
    }

    if (this->Input()->GetPlayerButton(3, ButtonTypeConfirm) == ButtonStateReleased || this->Input()->GetPlayerButton(3, ButtonTypeChange) == ButtonStateReleased) {
        INC_COLOLR(this->_color.b);
    }

    DisplayDriver *display = this->Display();
    rgb color = this->_pulsingState->GetColor(this->_color);

    display->Dice(0x07, color);
    DisplayHouses(color, display);
    DisplayGoals(color, display);
    DisplayField(color, display);
}

void PulseCompleteDisplayState::Leave( ) {
}
