/**
 * \file	TestGameState.h
 * \date	05.01.2017
 * \author	Aiko Isselhard
 * \brief	Main entry state. For development testing.
 * */

#ifndef STATES_TESTGAMESTATE_H_
#define STATES_TESTGAMESTATE_H_

#include <global.h>

#include <state/EnterReturnState.h>
#include <states/gamestart/SelectNoOfPlayerState.h>
#include <states/dice/DiceState.h>

#include <states/game/dice/SelectRollDiceState.h>
#include <states/game/preparemove/FindMovesState.h>

/**
 * \brief	Main entry state. For development testing.
 */
class TestGameState : public EnterReturnState<void, void> {
public:
    TestGameState( );
    virtual ~TestGameState( );

    virtual void Enter(void *);
    virtual void Update(uint32_t diff);
    virtual void* Leave( );
};

#endif /* STATES_TESTGAMESTATE_H_ */
