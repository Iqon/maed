/**
 * \file	TestInputState.cpp
 * \date	07.01.2017
 * \author	Iqon
 * \brief	Implementation
 * */

#include <states/development/TestInputState.h>

TestInputState::TestInputState(ButtonState state) {
    this->_switchingState = state;
    this->_pulsingState = new PulsingState( DICE_STATE_RISING_TIME,
    DICE_STATE_RISING_TIME,
                                           DICE_STATE_DELAY_TIME,
                                           DICE_STATE_DELAY_TIME);
}

TestInputState::~TestInputState( ) {
}

void TestInputState::Enter( ) {
    this->AddChildState(this->_pulsingState);

    this->_dice = 0x00;
    this->_color = COLOR_OFF;
}

void TestInputState::Update(uint32_t diff) {
    if (this->Input()->GetButtonState(DevButtonChange) == this->_switchingState) {
        this->_color = COLOR_WHITE;
        this->_dice = 0x02;
    }

    if (this->Input()->GetButtonState(DevButtonConfirm) == this->_switchingState) {
        this->_color = COLOR_WHITE;
        this->_dice = 0x01;
    }

    if (this->Input()->GetButtonState(Player0Change) == this->_switchingState) {
        this->_color = COLOR_PLAYER0;
        this->_dice = 0x02;
    }

    if (this->Input()->GetButtonState(Player0Confirm) == this->_switchingState) {
        this->_color = COLOR_PLAYER0;
        this->_dice = 0x01;
    }

    if (this->Input()->GetButtonState(Player1Change) == this->_switchingState) {
        this->_color = COLOR_PLAYER1;
        this->_dice = 0x02;
    }

    if (this->Input()->GetButtonState(Player1Confirm) == this->_switchingState) {
        this->_color = COLOR_PLAYER1;
        this->_dice = 0x01;
    }

    if (this->Input()->GetButtonState(Player2Change) == this->_switchingState) {
        this->_color = COLOR_PLAYER2;
        this->_dice = 0x02;
    }

    if (this->Input()->GetButtonState(Player2Confirm) == this->_switchingState) {
        this->_color = COLOR_PLAYER2;
        this->_dice = 0x01;
    }

    if (this->Input()->GetButtonState(Player3Change) == this->_switchingState) {
        this->_color = COLOR_PLAYER3;
        this->_dice = 0x02;
    }

    if (this->Input()->GetButtonState(Player3Confirm) == this->_switchingState) {
        this->_color = COLOR_PLAYER3;
        this->_dice = 0x01;
    }

    rgb color = this->_pulsingState->GetColor(this->_color);
    this->Display()->Dice(this->_dice, color);
}

void TestInputState::Leave( ) {
}
