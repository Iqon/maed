/**
 * \file	TestADCState.cpp
 * \date	07.02.2017
 * \author	Iqon
 * \brief	Implementation
 * */

#include <states/development/TestADCState.h>

#define INC_COLOLR(color)					\
	{										\
		if(color == 0x00)					\
			color = ((0xFF / 2) - 1);		\
		else if(color <= ((0xFF / 2) - 1))	\
			color = 0xFF;					\
		else if(color <= 0xFF)				\
			color = 0x00;					\
	}

TestADCState::TestADCState( ) {
    this->_color = COLOR_WHITE;
}

TestADCState::~TestADCState( ) {
}

void TestADCState::Enter( ) {
}

void TestADCState::Update(uint32_t diff) {
    if (this->Input()->GetPlayerButton(0, ButtonTypeConfirm) == ButtonStateReleased) {
        this->_color = COLOR_BLACK;
    }

    if (this->Input()->GetPlayerButton(1, ButtonTypeConfirm) == ButtonStateReleased || this->Input()->GetPlayerButton(1, ButtonTypeChange) == ButtonStateReleased) {
        INC_COLOLR(this->_color.r);
    }

    if (this->Input()->GetPlayerButton(2, ButtonTypeConfirm) == ButtonStateReleased || this->Input()->GetPlayerButton(2, ButtonTypeChange) == ButtonStateReleased) {
        INC_COLOLR(this->_color.g);
    }

    if (this->Input()->GetPlayerButton(3, ButtonTypeConfirm) == ButtonStateReleased || this->Input()->GetPlayerButton(3, ButtonTypeChange) == ButtonStateReleased) {
        INC_COLOLR(this->_color.b);
    }

    float brightness = this->Input()->GetBrightness();
    for (uint8_t i = 0; i < (brightness * ROUND_LEN); ++i) {
        this->Display()->Field(i, this->_color);
    }

    this->Display()->Dice(brightness * 0x06 + 0x01, this->_color);
}

void TestADCState::Leave( ) {
}
