/**
 * \file	TestADCState.h
 * \date	07.01.2017
 * \author	Iqon
 * \brief	Development state for testing the adcs range. Just displays the complete field full white,
 * 			allowing to use the buttion to iterate through some predefined colors.
 * */

#ifndef STATES_TESTADCSTATE_H_
#define STATES_TESTADCSTATE_H_

#include <global.h>

#include <state/State.h>

/**
 * \brief	Development state for testing the adcs range. Just displays the complete field full white,
 * 			allowing to use the buttion to iterate through some predefined colors.
 */
class TestADCState : public State {
public:
    TestADCState( );
    virtual ~TestADCState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    rgb _color;
};

#endif /* STATES_TESTADCSTATE_H_ */
