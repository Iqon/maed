/**
 * \file	PulseCompleteDisplayState.h
 * \date	Jun 19, 2016
 * \author 	Aiko Isselhard
 * \brief	State for testing the current display.
 * */

#ifndef PULSE_COMPLETE_DISPLAY_STATE_H
#define PULSE_COMPLETE_DISPLAY_STATE_H

#include <global.h>

#include <state/State.h>
#include <states/animations/PulsingState.h>

#define TESTDISPLAYSTATE_COLOR COLOR_RED

/**
 * \brief	State for testing the current display. Will pulsate the dispaly in a solid color.
 */
class PulseCompleteDisplayState : public State {
public:
    PulseCompleteDisplayState( );
    virtual ~PulseCompleteDisplayState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    rgb _color;
    PulsingState *_pulsingState;

    void DisplayField(rgb color,
                      DisplayDriver* display);
    void DisplayHouses(rgb color,
                       DisplayDriver* display);
    void DisplayHouse(rgb color,
                      DisplayDriver* display,
                      uint8_t player);
    void DisplayGoals(rgb color,
                      DisplayDriver* display);
    void DisplayGoal(rgb color,
                     DisplayDriver* display,
                     uint8_t player);
};

#endif /* PULSE_COMPLETE_DISPLAY_STATE_H */
