/**
 * \file	TestGameState.cpp
 * \date	05.01.2017
 * \author	Iqon
 * \brief	Implementation
 * */

#include <states/development/TestGameState.h>

TestGameState::TestGameState( ) {
}

TestGameState::~TestGameState( ) {
}

void TestGameState::Enter(void *) {
}

void TestGameState::Update(uint32_t diff) {
    this->Game()->GetPlayer(0x00)->SetActive(true);
    this->Game()->GetPlayer(0x00)->GetMan(0)->SetPosition(0, FieldTypeHouse);
    this->Game()->GetPlayer(0x00)->GetMan(1)->SetPosition(1, FieldTypeHouse);
    this->Game()->GetPlayer(0x00)->GetMan(2)->SetPosition(24, FieldTypeField);
    this->Game()->GetPlayer(0x00)->GetMan(3)->SetPosition(0, FieldTypeField);

    this->Game()->GetPlayer(0x01)->SetActive(true);
    this->Game()->GetPlayer(0x01)->GetMan(3)->SetPosition(6, FieldTypeField);
    this->Game()->GetPlayer(0x02)->GetMan(3)->SetPosition(12, FieldTypeField);
    this->Game()->GetPlayer(0x03)->GetMan(3)->SetPosition(18, FieldTypeField);

    this->Game()->GetPlayer(0x02)->SetActive(true);
    this->Game()->GetPlayer(0x03)->SetActive(true);

    this->Game()->SetCurrentPlayer(0x00);

    StateBase *state = new FindNextPlayerState();
    this->EnterNextState(state);
}

void* TestGameState::Leave( ) {
    pDiceResult res = new DiceResult();
    res->dice = 0x06;
    res->multiplyer = 0x03;
    return res;
}
