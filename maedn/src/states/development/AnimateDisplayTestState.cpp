/**
 * \file	AnimateDisplayTestState.cpp
 * \date	05.01.2017
 * \author	Iqon
 * \brief	Implementation
 * */

#include <states/development/AnimateDisplayTestState.h>

AnimateDisplayTestState::AnimateDisplayTestState( ) {
    this->_color = ANIMATEDISPLAYTESTSTATE_COLOR;
    this->_currentCount = 0x00;
}

AnimateDisplayTestState::~AnimateDisplayTestState( ) {
}

void AnimateDisplayTestState::Enter( ) {
}

void AnimateDisplayTestState::Update(uint32_t diff) {
    if (this->_runningTime > 2 * ANIMATEDISPLAYTESTSTATE_DELAY) this->_runningTime = ANIMATEDISPLAYTESTSTATE_DELAY;
    if (this->_runningTime > ANIMATEDISPLAYTESTSTATE_DELAY) {
        this->_runningTime -= ANIMATEDISPLAYTESTSTATE_DELAY;

        ++this->_currentCount;
    }

    DisplayDriver *display = this->Display();
    this->DisplayHouses(display);
    this->DisplayGoals(display);
    this->DisplayDice(display);
    this->DisplayField(display);
}

void AnimateDisplayTestState::Leave( ) {
}

void AnimateDisplayTestState::DisplayHouses(DisplayDriver *display) {
    for (uint8_t i = 0x00; i < 4; ++i) {
        uint8_t cur = this->_currentCount % 5;
        switch (cur) {
        case 4:
            display->House(i, 0x03, this->_color);
        case 3:
            display->House(i, 0x02, this->_color);
        case 2:
            display->House(i, 0x01, this->_color);
        case 1:
            display->House(i, 0x00, this->_color);
        }
    }
}

void AnimateDisplayTestState::DisplayGoals(DisplayDriver *display) {
    for (uint8_t i = 0x00; i < 4; ++i) {
        rgb playerColor = this->Game()->GetPlayer(i)->GetColor();
        display->Goal(i, 0x00, playerColor);
        display->Goal(i, 0x01, playerColor);
        display->Goal(i, 0x02, playerColor);
        display->Goal(i, 0x03, playerColor);

        uint8_t cur = this->_currentCount % 5;
        switch (cur) {
        case 4:
            display->Goal(i, 0x03, this->_color);
        case 3:
            display->Goal(i, 0x02, this->_color);
        case 2:
            display->Goal(i, 0x01, this->_color);
        case 1:
            display->Goal(i, 0x00, this->_color);
        }
    }
}

void AnimateDisplayTestState::DisplayDice(DisplayDriver *display) {
    uint8_t cur = this->_currentCount % 6;
    display->Dice(cur + 1, this->_color);
}

void AnimateDisplayTestState::DisplayField(DisplayDriver *display) {
    uint8_t cur = this->_currentCount % ROUND_LEN;
    for (uint8_t i = 0x00; i <= cur; ++i) {
        display->Field(i, this->_color);
    }
}
