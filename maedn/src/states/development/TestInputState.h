/**
 * \file	TestInputState.h
 * \date	07.01.2017
 * \author	Iqon
 * \brief	Tests all game input
 * */

#ifndef STATES_TESTINPUTSTATE_H_
#define STATES_TESTINPUTSTATE_H_

#include <global.h>

#include <state/State.h>
#include <states/animations/PulsingState.h>

/**
 * \brief Tests all game input.
 */
class TestInputState : public State {
public:
    TestInputState(ButtonState state);
    virtual ~TestInputState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    PulsingState *_pulsingState;
    ButtonState _switchingState;

    uint8_t _dice;
    rgb _color;
};

#endif /* STATES_TESTINPUTSTATE_H_ */
