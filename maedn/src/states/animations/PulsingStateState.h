/**
 * \file	PulsingStateState.h
 * \date	Jun 15, 2016
 * \author 	Aiko Isselhard
 * \brief	Enumeration for the possible states the pulsing state can have.
 * */

#ifndef PULSINGSTATESTATE_H_
#define PULSINGSTATESTATE_H_

enum PulsingStateState {
    PulsingStateStateRising, PulsingStateStateUp, PulsingStateStateFalling, PulsingStateStateDown
};

#endif /* PULSINGSTATESTATE_H_ */
