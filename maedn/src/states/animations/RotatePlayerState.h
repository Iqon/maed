/**
 * \file	RotatePlayerState.h
 * \date	Jun 7, 2016
 * \author 	Aiko Isselhard
 * \brief	Will light up all leds of one player while rotating the active displayed player.
 * 			Short animation.
 * */

#ifndef ROTATEPLAYERSTATE_H_
#define ROTATEPLAYERSTATE_H_

#include <global.h>
#include <io/output/rgb.h>
#include <state/StateBase.h>
#include <state/ForwardingState.h>

#define ROTATE_PLAYER_RUNNING_TIME		(3 * S_TICKS)
#define ROTATE_PLAYER_STEP				(80 * MS_TICKS)

/**
 * \brief	Will light up all leds of one player while rotating the active displayed player.
 * 			Short animation.
 */
template<typename TForward>
class RotatePlayerState : public ForwardingState<TForward> {
public:
    RotatePlayerState(uint8_t start,
                      StateBase *next)
            : ForwardingState<TForward>(next) {
        this->_pos = start;
    }

    virtual ~RotatePlayerState( ) {
    }

    virtual void Enter( ) {
    }

    virtual void Update(uint32_t diff) {
        this->_pos = (this->_runningTime / ROTATE_PLAYER_STEP) % 4;

        if (this->_runningTime > ROTATE_PLAYER_RUNNING_TIME) {
            this->Stop();
            return;
        }

        IPlayer *player = this->Game()->GetPlayer(this->_pos);
        rgb color = player->GetColor();
        this->Display()->Goal(this->_pos, 0x00, color);
        this->Display()->Goal(this->_pos, 0x01, color);
        this->Display()->Goal(this->_pos, 0x02, color);
        this->Display()->Goal(this->_pos, 0x03, color);

        int8_t pos = player->GetStartPos() - 1;
        if (pos < 0) {
            pos = ROUND_LEN + pos;
        }
        this->Display()->Field(pos, color);
    }

    virtual void Leave( ) {
    }

private:
    uint8_t _pos;
};

#endif /* ROTATEPLAYERSTATE_H_ */
