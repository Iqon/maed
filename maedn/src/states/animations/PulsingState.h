/**
 * \file	PulsingState.h
 * \date	Jun 7, 2016
 * \author 	Aiko Isselhard
 * \brief	Allows to blink an led. Will blink the given color in the given rising and downTime.
 * 			Is mostly used as substate. Yields a certain color changing in its intensity which
 * 			can be used by the parent to display blinking leds.
 * */
#ifndef PULSINGSTATE_H_
#define PULSINGSTATE_H_

#include <io/output/rgb.h>
#include <state/State.h>
#include <states/animations/PulsingStateState.h>

/**
 * \brief	Allows to blink an led. Will blink the given color in the given rising and downTime.
 * 			Is mostly used as substate. Yields a certain color changing in its intensity which
 * 			can be used by the parent to display blinking leds.
 */
class PulsingState : public State {
public:
    PulsingState(uint32_t risingMs,
                 uint32_t fallingMs,
                 uint32_t downTimeMs,
                 uint32_t upTimeMs);
    PulsingState(uint32_t risingMs,
                 uint32_t upTimeMs);

    virtual ~PulsingState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

    /**
     * \brief	Returns the color at the current pulses brightness
     * \returns the color at the current pulses brightness
     */
    rgb GetColor(rgb color);

    /**
     * \brief	Returns the color at the current pulses inverted brightness
     * \returns the color at the current pulses inverted brightness
     */
    rgb GetInvertedColor(rgb color);

    /**
     * \brief	Returns the colors merged at the current pulses brightness
     * \returns the colors merged at the current pulses inverted brightness
     */
    rgb GetMergedColor(rgb left,
                       rgb right);

private:
    PulsingStateState _state;

    float _intensity;
    uint32_t _risingMs;
    uint32_t _fallingMs;
    uint32_t _downTimeMs;
    uint32_t _upTimeMs;

    uint8_t CalculateColorChannel(uint8_t channel);
    uint8_t CalculateInvertedColorChannel(uint8_t channel);
};

#endif /* PULSINGSTATE_H_ */
