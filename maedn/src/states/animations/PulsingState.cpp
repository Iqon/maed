/**
 * \file	PulsingState.cpp
 * \date	Jun 7, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */
#include <states/animations/PulsingState.h>

PulsingState::PulsingState(uint32_t risingMs,
                           uint32_t fallingMs,
                           uint32_t downTimeMs,
                           uint32_t upTimeMs) {
    this->_state = PulsingStateStateRising;
    this->_intensity = 0.0f;
    this->_risingMs = risingMs / MS_TICKS;
    this->_fallingMs = risingMs / MS_TICKS;
    this->_downTimeMs = downTimeMs / MS_TICKS;
    this->_upTimeMs = upTimeMs / MS_TICKS;
}

PulsingState::PulsingState(uint32_t risingMs,
                           uint32_t upTimeMs) {
    this->_state = PulsingStateStateRising;
    this->_intensity = 0.0f;
    this->_risingMs = risingMs / MS_TICKS;
    this->_fallingMs = risingMs / MS_TICKS;
    this->_downTimeMs = upTimeMs / MS_TICKS;
    this->_upTimeMs = upTimeMs / MS_TICKS;
}

PulsingState::~PulsingState( ) {
}

void PulsingState::Enter( ) {
}

void PulsingState::Update(uint32_t diff) {
    switch (this->_state) {
    case PulsingStateStateRising:
        if (this->_runningTime > this->_risingMs) {
            this->_runningTime = 0x00;
            this->_state = PulsingStateStateUp;
            this->_intensity = 1.0f;
        } else {
            // I don't get why, but in the edge cases this causes flickering. (approaching to 1f)
            this->_intensity = (float) this->_runningTime / (float) this->_risingMs;
        }
    break;
    case PulsingStateStateUp:
        if (this->_runningTime > this->_upTimeMs) {
            this->_runningTime = 0x00;
            this->_state = PulsingStateStateFalling;
        }

        this->_intensity = 1.0f;
    break;
    case PulsingStateStateFalling:
        if (this->_runningTime > this->_fallingMs) {
            this->_runningTime = 0x00;
            this->_state = PulsingStateStateDown;
            this->_intensity = 0.0f;
        } else {
            // I don't get why, but in the edge cases this causes flickering. (approaching to 0f)
            this->_intensity = 1.0f - ((float) this->_runningTime / (float) this->_fallingMs);
        }

    break;
    case PulsingStateStateDown:
        if (this->_runningTime > this->_downTimeMs) {
            this->_runningTime = 0x00;
            this->_state = PulsingStateStateRising;
        }

        this->_intensity = 0.0f;
    break;
    }

    // Should never happen... just a safety measure and paranoia.
    if (this->_intensity < 0.0f) {
        this->_intensity = 0.0f;
    }
    if (this->_intensity > 1.0f) {
        this->_intensity = 1.0f;
    }
}

void PulsingState::Leave( ) {
}

rgb PulsingState::GetColor(rgb color) {
    rgb ret;
    ret.r = color.r * this->_intensity;
    ret.g = color.g * this->_intensity;
    ret.b = color.b * this->_intensity;

    return ret;
}

rgb PulsingState::GetInvertedColor(rgb color) {
    rgb ret;
    ret.r = color.r * (1.0f - this->_intensity);
    ret.g = color.g * (1.0f - this->_intensity);
    ret.b = color.b * (1.0f - this->_intensity);

    return ret;
}

rgb PulsingState::GetMergedColor(rgb left,
                                 rgb right) {
    rgb color;
    color.r = (left.r * (1.0f - this->_intensity)) + (right.r * this->_intensity);
    color.g = (left.g * (1.0f - this->_intensity)) + (right.g * this->_intensity);
    color.b = (left.b * (1.0f - this->_intensity)) + (right.b * this->_intensity);

    return color;
}
