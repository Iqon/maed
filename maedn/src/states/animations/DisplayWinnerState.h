/**
 * \file	DisplayWinnerState.h
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Displays the given player.
 * */

#ifndef DISPLAYWINNERSTATE_H_
#define DISPLAYWINNERSTATE_H_

#include <state/StateBase.h>
#include <state/ForwardingState.h>
#include <states/animations/PulsingState.h>

#define DISPLAY_WINNER_STATE_TIME	(10 * S_TICKS)

/**
 * \brief	Displays the given player.
 */
template<typename TForward>
class DisplayWinnerState : public ForwardingState<TForward> {
public:
    DisplayWinnerState(uint8_t maxPlayer,
                       StateBase *next)
             : ForwardingState<TForward>(next) {
        this->_result = maxPlayer;
        this->_pulsingState = new PulsingState(RISING_TIME,
                                               RISING_TIME,
                                               DELAY_TIME,
                                               DELAY_TIME);
        this->AddChildState(this->_pulsingState);
    }

    virtual ~DisplayWinnerState( ) {
    }

    virtual void Enter( ) {
    }

    virtual void Update(uint32_t diff) {
        if (this->_runningTime > DISPLAY_WINNER_STATE_TIME || this->Input()->GetPlayerButton(this->_result, ButtonTypeChange) == ButtonStateReleased
                || this->Input()->GetPlayerButton(this->_result, ButtonTypeConfirm) == ButtonStateReleased) {
            this->Stop();
            return;
        }

        IPlayer *player = this->Game()->GetPlayer(this->_result);
        rgb color = this->_pulsingState->GetColor(player->GetColor());
        this->Display()->House(this->_result, 0x00, color);
        this->Display()->House(this->_result, 0x01, color);
        this->Display()->House(this->_result, 0x02, color);
        this->Display()->House(this->_result, 0x03, color);
        this->Display()->Goal(this->_result, 0x00, color);
        this->Display()->Goal(this->_result, 0x01, color);
        this->Display()->Goal(this->_result, 0x02, color);
        this->Display()->Goal(this->_result, 0x03, color);

        int8_t pos = player->GetStartPos() - 1;
        if (pos < 0) {
            pos = ROUND_LEN + pos;
        }
        this->Display()->Field(pos, color);
    }

    virtual void Leave( ) {
    }

private:
    PulsingState *_pulsingState;
    uint8_t _result;
};

#endif /* DISPLAYWINNERSTATE_H_ */
