/**
 * \file	FindFirstPlayerState.h
 * \date	Jun 7, 2016
 * \author 	Aiko Isselhard
 * \brief 	Finds the first player by letting each active player roll.
 * */
#ifndef FINDFIRSTPLAYERSTATE_H_
#define FINDFIRSTPLAYERSTATE_H_

#include <global.h>

#include <state/EnterState.h>

#include <states/dice/DiceState.h>
#include <states/dice/DiceResult.h>

#include <states/animations/DisplayWinnerState.h>
#include <states/animations/RotatePlayerState.h>

#include <states/game/dice/SelectRollDiceState.h>
#include <states/game/FindNextPlayerState.h>

#include <states/gamestart/SelectNoOfPlayerState.h>

/**
 * \brief 	Finds the first player by letting each active player roll.
 */
class FindFirstPlayerState : public EnterState<DiceResult_t> {
public:
    FindFirstPlayerState(uint8_t* scores);
    virtual ~FindFirstPlayerState( );

    virtual void Enter(DiceResult_t *dice);
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    uint8_t* _scores;
    bool* _active;
    int8_t _currentPos;

    PulsingState *_pulsingState;

    void GoToNextState(void);
    void InitializeActivePlayer(uint8_t *scores);
};

#endif /* FINDFIRSTPLAYERSTATE_H_ */
