/**
 * \file	SelectNoOfPlayerState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Allows each player to be activated, selecting the number of active player.
 * 			Will continue to the FindFirstPlayerState.
 * */

#ifndef SELECTNOOFPLAYERSTATE_H_
#define SELECTNOOFPLAYERSTATE_H_

#include <global.h>
#include <state/State.h>
#include <game/IPlayer.h>
#include <states/gamestart/FindFirstPlayerState.h>

#define SELECTNOFPLAYERSATE_DELAY	(1000 * MS_TICKS)

/**
 * \brief	Internal state for each player.
 */
enum SelectedPlayerState {
    NotPlaying, Playing, LockedIn
};

/**
 * \brief	Allows each player to be activated, seleting the number of active player.
 * 			Will continue to the FindFirstPlayerState.
 */
class SelectNoOfPlayerState : public State {
public:
    SelectNoOfPlayerState( );
    virtual ~SelectNoOfPlayerState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    bool _countdownRunning;
    uint8_t _dice;
    SelectedPlayerState _player[4];
    PulsingState *_pulsingState;

    void CountDown( );
    void StartOrStopCountDown( );

    void Dice( );
    void ReadInput( );
    void Draw( );
    void Draw(uint8_t player);
    rgb GetColor(uint8_t player);

};

#endif /* SELECTNOOFPLAYERSTATE_H_ */
