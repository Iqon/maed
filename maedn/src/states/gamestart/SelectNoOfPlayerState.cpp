/**
 * \file	SelectNoOfPlayerState.cpp
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/gamestart/SelectNoOfPlayerState.h>

SelectNoOfPlayerState::SelectNoOfPlayerState( ) {
    memset(this->_player, NotPlaying, sizeof(SelectedPlayerState) * 4);

    this->_countdownRunning = false;
    this->_dice = 0x07;
    this->_pulsingState = new PulsingState( DICE_STATE_RISING_TIME,
    DICE_STATE_RISING_TIME,
                                           DICE_STATE_DELAY_TIME,
                                           DICE_STATE_DELAY_TIME);
    this->AddChildState(this->_pulsingState);
}

SelectNoOfPlayerState::~SelectNoOfPlayerState( ) {
}

void SelectNoOfPlayerState::Enter( ) {
    this->Game()->SetDraw(false);
}

void SelectNoOfPlayerState::Update(uint32_t diff) {
    if (this->_dice == 0x00) {
        for (uint8_t i = 0x00; i < 0x04; ++i) {
            IPlayer *player = this->Game()->GetPlayer(i);
            bool active = this->_player[i] != NotPlaying;
            player->SetActive(active);
        }

        StateBase *nextState = new FindFirstPlayerState(NULL);
        this->EnterNextState(nextState);
        return;
    }

    this->CountDown();
    this->Dice();
    this->ReadInput();
    this->Draw();
}

void SelectNoOfPlayerState::ReadInput( ) {
    for (uint8_t i = 0x00; i < 0x04; ++i) {
        if (this->Input()->GetPlayerButton(i, ButtonTypeChange) == ButtonStateReleased) {
            this->_player[i] = this->_player[i] == NotPlaying ? Playing : NotPlaying;
        }

        if (this->Input()->GetPlayerButton(i, ButtonTypeConfirm) == ButtonStateReleased && this->_player[i] != NotPlaying) {
            this->_player[i] = this->_player[i] == Playing ? LockedIn : Playing;
        }
    }
}

void SelectNoOfPlayerState::Leave( ) {
    this->Game()->SetDraw(true);
}

void SelectNoOfPlayerState::Draw( ) {
    for (uint8_t i = 0x00; i < 0x04; ++i)
        this->Draw(i);
}

void SelectNoOfPlayerState::Draw(uint8_t player) {
    rgb color = this->GetColor(player);

    this->Display()->Goal(player, 0, color);
    this->Display()->Goal(player, 1, color);
    this->Display()->Goal(player, 2, color);
    this->Display()->Goal(player, 3, color);

    this->Display()->House(player, 0, color);
    this->Display()->House(player, 1, color);
    this->Display()->House(player, 2, color);
    this->Display()->House(player, 3, color);
}

rgb SelectNoOfPlayerState::GetColor(uint8_t player) {
    if (this->_player[player] == Playing) {
        IPlayer *playerObject = this->Game()->GetPlayer(player);
        return this->_pulsingState->GetColor(playerObject->GetColor());
    }

    if (this->_player[player] == LockedIn) {
        IPlayer *playerObject = this->Game()->GetPlayer(player);
        return playerObject->GetColor();
    }

    //if(this->_player[player] == NotPlaying) // Is the default
    return COLOR_OFF;
}

void SelectNoOfPlayerState::Dice( ) {
    if (this->_countdownRunning) {
        for (uint8_t player = 0x00; player < 0x04; ++player) {
            rgb color = this->Game()->GetPlayer(player)->GetColor();
            for (uint8_t multiplier = 0x00; multiplier < this->_dice; ++multiplier) {
                this->Display()->Multiplyer(player, multiplier + 1, color);
            }
        }

        this->Display()->Dice(_dice, COLOR_WHITE);
    } else {
        this->Display()->Dice(_dice, this->_pulsingState->GetColor(COLOR_WHITE));
    }
}

void SelectNoOfPlayerState::CountDown( ) {
    this->StartOrStopCountDown();

    if (!_countdownRunning) // countdown was not started, so skip
        return;

    if (this->_runningTime > 2 * SELECTNOFPLAYERSATE_DELAY) this->_runningTime = SELECTNOFPLAYERSATE_DELAY;
    if (this->_runningTime > SELECTNOFPLAYERSATE_DELAY) {
        this->_runningTime -= SELECTNOFPLAYERSATE_DELAY;

        --this->_dice;
    }
}

void SelectNoOfPlayerState::StartOrStopCountDown( ) {
    uint8_t start = (this->_player[0] != Playing 	// No player left in playing state
    && this->_player[1] != Playing && this->_player[2] != Playing && this->_player[3] != Playing) && (this->_player[1] == LockedIn 	// At least on player locked in
    || this->_player[2] == LockedIn || this->_player[3] == LockedIn || this->_player[4] == LockedIn);

    if (start && !_countdownRunning) {
        _countdownRunning = true;
        this->_runningTime = 0x00;
    } else if (!start && _countdownRunning) {
        this->_countdownRunning = false;
        this->_dice = 0x07;
    }
}
