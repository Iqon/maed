/**
 * \file	FindFirstPlayerState.cpp
 * \date	Jun 7, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/gamestart/FindFirstPlayerState.h>

FindFirstPlayerState::FindFirstPlayerState(uint8_t *scores) {
    this->_currentPos = -1;
    this->InitializeActivePlayer(scores);

    this->_pulsingState = new PulsingState( RISING_TIME,
    RISING_TIME,
                                           DELAY_TIME,
                                           DELAY_TIME);
    this->AddChildState(_pulsingState);
}

void FindFirstPlayerState::InitializeActivePlayer(uint8_t *scores) {
    this->_scores = new uint8_t[4];
    memset(this->_scores, 0x00, sizeof(uint8_t) * 4);

    this->_active = new bool[4];

    // copy previous scores and find max score.
    uint8_t maxScore = 0x00;
    if (scores != 0x00) {
        for (uint8_t i = 0x00; i < 0x04; ++i) {
            this->_scores[i] = scores[i];
            if (this->_scores[i] > maxScore) maxScore = this->_scores[i];
        }
    }

    // Find active players.
    for (uint8_t i = 0x00; i < 0x04; ++i) {
        bool isActive = this->Game()->GetPlayer(i)->IsActive();
        this->_active[i] = this->_scores[i] == maxScore && isActive;
    }
}

FindFirstPlayerState::~FindFirstPlayerState(void) {
    delete this->_scores;
    delete this->_active;
}

void FindFirstPlayerState::Enter(DiceResult_t * dice) {
    // if coming back from a roll dice, set the result
    if (dice != 0x00) {
        this->_scores[this->_currentPos] = dice->dice * dice->multiplyer;
    }

    ++this->_currentPos;
}

void FindFirstPlayerState::Update(uint32_t diff) {
    // Let all active players roll.
    while (this->_currentPos < 0x04) {
        if (this->_active[this->_currentPos] == true) {
            // Roll Dice
            DiceState *state = new DiceState(this->_currentPos);
            this->EnterSubState(state);
            return;
        }

        ++this->_currentPos;
    }

    // end this state and continue
    this->GoToNextState();
}

void FindFirstPlayerState::GoToNextState(void) {
    // Check if there are two or more equal scores
    bool isThereADraw = false;
    uint8_t maxScore = 0x00;
    uint8_t maxPlayer = 0x00;

    for (uint8_t i = 0x00; i < 0x04; ++i) {
        if (this->_scores[i] > maxScore) {
            maxPlayer = i;
            maxScore = this->_scores[i];
            isThereADraw = false;
        } else if (maxScore == _scores[i]) isThereADraw = true;
    }

    if (isThereADraw) { // still in draw, reactivate self
        StateBase *self = new FindFirstPlayerState(this->_scores);
        this->EnterNextState(self);
    } else { // draw solved, continue to game.
        uint8_t prevPlayer = maxPlayer;
        if(prevPlayer == 0) {
            prevPlayer = this->Game()->GetPlayerCount();
        }

        prevPlayer = prevPlayer - 1; // decrease, because find next will select the following active player
        this->Game()->SetCurrentPlayer(prevPlayer);

        StateBase *rollDice = new FindNextPlayerState();
        StateBase *displayWinnerState = new DisplayWinnerState<uint8_t>(maxPlayer, rollDice);
        StateBase *animState = new RotatePlayerState<uint8_t>(maxPlayer, displayWinnerState);
        this->EnterNextState(animState);
    }

}

void FindFirstPlayerState::Leave( ) {
}
