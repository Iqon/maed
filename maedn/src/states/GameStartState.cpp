/**
 * \file	GameStartState.cpp
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/GameStartState.h>

GameStartState::GameStartState( ) {
}

GameStartState::~GameStartState( ) {
}

void GameStartState::Enter( ) {
    GameMgr::Reset();

    SelectNoOfPlayerState *state = new SelectNoOfPlayerState();
    this->EnterSubState(state);
}

void GameStartState::Update(uint32_t diff) {
}

void GameStartState::Leave( ) {
}
