/**
 * \file	MultiplierState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Displays the roll multiplier animation and waits for the player to
 * 			release the button or waits a set time. Accepts the number achieved
 * 			in the RollDiceState and will return a DiceResult containing the full
 * 			number rolled. The next state is the DisplayDiceResultState.
 * */

#ifndef MULTIPLIERSTATE_H_
#define MULTIPLIERSTATE_H_

#include <global.h>
#include <game/Random.h>
#include <state/EnterReturnState.h>
#include <states/dice/DiceResult.h>
#include <states/dice/DisplayDiceResultState.h>

#define MULTIPLYER_DICE_DELAY (70 * MS_TICKS)
#define MULTIPLYER_MAX_TIME (4 * S_TICKS)
#define MULTIPLYER_MIN_TIME (250 * MS_TICKS)

/**
 * \brief	Displays the roll multiplier animation and waits for the player to
 * 			release the button or waits a set time. Accepts the number achieved
 * 			in the RollDiceState and will return a DiceResult containing the full
 * 			number rolled. The next state is the DisplayDiceResultState.
 */
class MultiplierState : public EnterReturnState<uint8_t, DiceResult> {
public:
    MultiplierState(uint8_t player);
    virtual ~MultiplierState( );

    virtual void Enter(uint8_t *);
    virtual void Update(uint32_t diff);
    virtual pDiceResult Leave( );

private:
    Random *_rnd;
    uint8_t _player;
    pDiceResult _result;

    bool RollConfirmed(void);
};

#endif /* MULTIPLIERSTATE_H_ */
