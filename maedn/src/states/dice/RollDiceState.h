/**
 * \file	RollDiceState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Displays the roll dice animation and waits for the player to
 * 			release the button or waits a set time. Will return the number
 * 			the player got. The next state is the MultiplierState.
 * */

#ifndef ROLLDICESTATE_H_
#define ROLLDICESTATE_H_

#include <global.h>

#include <state/ReturnState.h>
#include <game/Random.h>

#include <states/dice/MultiplierState.h>

#define ROLL_DICE_DELAY (100 * MS_TICKS)
#define ROLL_DICE_MAX_TIME (6 * S_TICKS)
#define ROLL_DICE_MIN_TIME (250 * MS_TICKS)

/**
 * \brief	Displays the roll dice animation and waits for the player to
 * 			release the button or waits a set time. Will return the number
 * 			the player got. The next state is the MultiplierState.
 */
class RollDiceState : public ReturnState<uint8_t> {
public:
    RollDiceState(uint8_t player);
    virtual ~RollDiceState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual uint8_t *Leave( );

private:
    Random *_rnd;
    uint8_t _player;
    uint8_t *_result;
    uint8_t _dice;

    bool RollConfirmed(void);
};

#endif /* ROLLDICESTATE_H_ */
