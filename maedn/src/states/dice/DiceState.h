/**
 * \file	DiceState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	First step in the roll dice chain. Will wait for the player to
 * 			press a button and continue with the RollDiceState.
 * */

#ifndef DICESTATE_H_
#define DICESTATE_H_

#include <global.h>

#include <state/State.h>
#include <states/dice/DiceResult.h>
#include <states/dice/RollDiceState.h>
#include <states/dice/MultiplierState.h>
#include <states/animations/PulsingState.h>
#include <states/dice/DisplayDiceResultState.h>

/**
 * \brief	First step in the roll dice chain. Will wait for the player to
 * 			press a button and continue with the RollDiceState.
 */
class DiceState : public State {
public:
    DiceState(uint8_t player);
    virtual ~DiceState( );

    virtual void Enter( );
    virtual void Update(uint32_t diff);
    virtual void Leave( );

private:
    uint8_t _player;
    PulsingState *_pulsingState;
};

#endif /* DICESTATE_H_ */
