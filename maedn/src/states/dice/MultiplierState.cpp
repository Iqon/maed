/**
 * \file	MultiplierState.cpp
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/dice/MultiplierState.h>

MultiplierState::MultiplierState(uint8_t player) {
    this->_rnd = Random::GetInstance();
    this->_player = player;

    this->_result = new DiceResult();
    this->_result->multiplyer = 0x00;
    this->_result->dice = 0x00;
}

MultiplierState::~MultiplierState( ) {
    delete this->_result;
}

void MultiplierState::Enter(uint8_t *obj) {
    this->_result->dice = *obj;
}

void MultiplierState::Update(uint32_t diff) {
    uint8_t multipliyer = (this->_runningTime / MULTIPLYER_DICE_DELAY) % 0x03;
    this->_result->multiplyer = multipliyer;

    if (this->RollConfirmed()) {
        DisplayDiceResultState *state = new DisplayDiceResultState(this->_player);
        this->EnterNextState(state);
        return;
    }

    // Display roll
    rgb color = this->Game()->GetPlayer(this->_player)->GetColor();

    this->Display()->ClearMultiplyer(this->_player);
    this->Display()->Multiplyer(this->_player, this->_result->multiplyer + 1, color);
    this->Display()->Dice(this->_result->dice, color);
}

bool MultiplierState::RollConfirmed(void) {
    ButtonState btn1 = this->Input()->GetPlayerButton(this->_player, ButtonTypeConfirm);
    ButtonState btn2 = this->Input()->GetPlayerButton(this->_player, ButtonTypeChange);

    bool confirmed = (this->_runningTime > MULTIPLYER_MAX_TIME && btn1 == ButtonStateUp && btn2 == ButtonStateUp)
            || (this->_runningTime > MULTIPLYER_MIN_TIME && (btn1 == ButtonStateReleased || btn2 == ButtonStateReleased));
    return confirmed;
}

pDiceResult MultiplierState::Leave( ) {
    // Todo: Leave at 33% chance, or assign different possibilities to the multiplier
    //uint32_t rnd = this->_rnd->GetRandomUInt32(0, 100);

    //if(rnd < 20)
    //{
    //	this->_result->multiplyer = 0x03;
    //}
    //if(rnd < 50)
    //{
    //	this->_result->multiplyer = 0x02;
    //}
    //else
    //{
    //	this->_result->multiplyer = 0x01;
    //}

    this->_result->multiplyer = this->_rnd->GetRandomUInt32(1, 4);
    return this->_result;
}
