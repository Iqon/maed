/**
 * \file	DisplayDiceResultState.cpp
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/dice/DisplayDiceResultState.h>

DisplayDiceResultState::DisplayDiceResultState(uint8_t player) {
    this->_player = player;
    this->_result = new DiceResult();
}

DisplayDiceResultState::~DisplayDiceResultState( ) {
    delete this->_result;
}

void DisplayDiceResultState::Enter(pDiceResult result) {
    this->_pulsingState = new PulsingState( DICE_STATE_RISING_TIME,
    DICE_STATE_RISING_TIME,
                                           DICE_STATE_DELAY_TIME,
                                           DICE_STATE_DELAY_TIME);
    this->AddChildState(this->_pulsingState);

    this->_result->dice = result->dice;
    this->_result->multiplyer = result->multiplyer;
}

void DisplayDiceResultState::Update(uint32_t diff) {
    if (this->_runningTime > DISPLAY_DICE_RESULT_TIME) {
        this->Stop();
        return;
    }

    rgb color = this->_pulsingState->GetColor(this->Game()->GetPlayer(this->_player)->GetColor());
    this->Display()->Multiplyer(this->_player, this->_result->multiplyer, color);
    this->Display()->Dice(this->_result->dice, color);
}

pDiceResult DisplayDiceResultState::Leave( ) {
    return this->_result;
}
