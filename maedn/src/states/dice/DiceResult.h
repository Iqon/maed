/**
 * \file	DiceResult.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Contains the result of a throw dice run
 * */

#ifndef DICERESULT_H_
#define DICERESULT_H_

/**
 * \brief Contains the result of a throw dice run
 */
typedef struct DiceResult_t {
    uint8_t dice;
    uint8_t multiplyer;
} DiceResult, *pDiceResult;

#endif /* DICERESULT_H_ */
