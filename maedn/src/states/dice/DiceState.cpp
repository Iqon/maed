/**
 * \file	DiceState.cpp
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/dice/DiceState.h>

DiceState::DiceState(uint8_t player) {
    this->_player = player;

    this->_pulsingState = new PulsingState( DICE_STATE_RISING_TIME,
    DICE_STATE_RISING_TIME,
                                           DICE_STATE_DELAY_TIME,
                                           DICE_STATE_DELAY_TIME);
    this->AddChildState(this->_pulsingState);
}

DiceState::~DiceState( ) {
}

void DiceState::Enter( ) {
}

void DiceState::Update(uint32_t diff) {
    rgb color = this->_pulsingState->GetColor(this->Game()->GetPlayer(this->_player)->GetColor());

    this->Display()->Multiplyer(this->_player, 0x01, color);
    this->Display()->Multiplyer(this->_player, 0x02, color);
    this->Display()->Multiplyer(this->_player, 0x03, color);
    this->Display()->Dice(0x07, color);

    if (this->Input()->GetPlayerButton(this->_player, ButtonTypeConfirm) == ButtonStateDown || this->Input()->GetPlayerButton(this->_player, ButtonTypeChange) == ButtonStateDown) {
        RollDiceState *state = new RollDiceState(this->_player);
        this->EnterNextState(state);
        return; // Important because this state is deleted upon switching.
    }
}

void DiceState::Leave( ) {
}
