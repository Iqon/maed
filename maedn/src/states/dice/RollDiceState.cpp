/**
 * \file	RollDiceState.cpp
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <states/dice/RollDiceState.h>

RollDiceState::RollDiceState(uint8_t player) {
    this->_rnd = Random::GetInstance();
    this->_player = player;
    this->_result = new uint8_t();
    this->_dice = 0x00;
}

RollDiceState::~RollDiceState( ) {
    delete this->_result;
}

void RollDiceState::Enter( ) {
}

bool RollDiceState::RollConfirmed(void) {
    ButtonState btn1 = this->Input()->GetPlayerButton(this->_player, ButtonTypeConfirm);
    ButtonState btn2 = this->Input()->GetPlayerButton(this->_player, ButtonTypeChange);

    bool confirmed = (this->_runningTime > ROLL_DICE_MAX_TIME && btn1 == ButtonStateUp && btn2 == ButtonStateUp)
            || (this->_runningTime > ROLL_DICE_MIN_TIME && (btn1 == ButtonStateReleased || btn2 == ButtonStateReleased));
    return confirmed;
}

void RollDiceState::Update(uint32_t diff) {
    uint8_t dice = (this->_runningTime / ROLL_DICE_DELAY) % 0x06;
    this->_dice = dice;

    if (RollConfirmed()) {
        MultiplierState *state = new MultiplierState(this->_player);
        this->EnterNextState(state);
        return;
    }

    // Display roll
    rgb color = this->Game()->GetPlayer(this->_player)->GetColor();

    this->Display()->Dice(this->_dice + 1, color);
}

uint8_t *RollDiceState::Leave( ) {
    uint32_t rnd = this->_rnd->GetRandomUInt32(1, 7);
    *this->_result = (uint8_t) rnd;
    return this->_result;
}
