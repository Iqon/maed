/**
 * \file	DisplayDiceResultState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Displays the dice result until the player intervents or a certain delay
 * 			has elapsed. Will yield the throw dice results.
 * */

#ifndef DISPLAYDICERESULTSTATE_H_
#define DISPLAYDICERESULTSTATE_H_

#include <global.h>
#include <state/EnterReturnState.h>
#include <states/dice/DiceResult.h>
#include <states/animations/PulsingState.h>

#define DISPLAY_DICE_RESULT_TIME (3 * S_TICKS)

/**
 * \brief	Displays the dice result until the player intervents or a certain delay
 * 			has elapsed. Will yield the throw dice results.
 */
class DisplayDiceResultState : public EnterReturnState<DiceResult, DiceResult> {
public:
    DisplayDiceResultState(uint8_t player);
    virtual ~DisplayDiceResultState( );

    virtual void Enter(pDiceResult obj);
    virtual void Update(uint32_t diff);
    virtual pDiceResult Leave( );

private:
    pDiceResult _result;
    uint8_t _player;
    PulsingState *_pulsingState;
};

#endif /* DISPLAYDICERESULTSTATE_H_ */
