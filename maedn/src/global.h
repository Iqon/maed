/**
 * \file    global.h
 * \author 	Aiko Isselhard
 * @brief	Global definitions and short helper functions.
 * */

#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <inttypes.h>
#include <limits.h>
#include <string.h>

#include <colors.h>

#ifndef NULL
#	define NULL 	(0x00)
#endif

#ifndef null
#	define null		(NULL)
#endif

#ifndef TRUE
#   define TRUE (1 == 1)
#endif

#ifndef FALSE
#   define FALSE (1 != 1)
#endif

#define MS_TICKS 	((uint16_t)2)
#define S_TICKS 	(1000 * (MS_TICKS))

#define RISING_TIME 	(200 * MS_TICKS)
#define DELAY_TIME 		(200 * MS_TICKS)

#define DICE_STATE_RISING_TIME 	(200 * MS_TICKS)
#define DICE_STATE_DELAY_TIME 	(300 * MS_TICKS)

#define WALK_STEP_DELAY (120 * MS_TICKS)

#endif /* GLOBAL_H_ */
