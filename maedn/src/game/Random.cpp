/**
 * \file	Random.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <game/Random.h>

Random::Random( ) {
    RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);

    /* RNG Peripheral enable */
    RNG_Cmd(ENABLE);
}

Random::~Random( ) {
    RNG_Cmd(DISABLE);
}

uint32_t Random::GetRandomUInt32(uint32_t min,
                                 uint32_t max) {
    /* Wait until one RNG number is ready */
    while (RNG_GetFlagStatus(RNG_FLAG_DRDY) == RESET) {
    }

    /* Get a 32bit Random number */
    uint32_t rnd = RNG_GetRandomNumber();
    rnd = (rnd % (max - min)) + min;
    return rnd;
}
