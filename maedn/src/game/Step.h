/**
 * \file	Step.h
 * \date	Jun 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Represents one Step which are aggregate to a Move. A Move consists of up to 3 steps.
 * */

#ifndef STEP_H_
#define STEP_H_

#include <global.h>

#include <game/StepType.h>
#include <game/FieldType.h>

/**
 * \brief Represents one Step which are aggregate to a Move. A Move consists of up to 3 steps.
 */
class Step {
public:
    Step( );
    Step(const Step *step);
    virtual ~Step( );

    /**
     * \brief	Returns the FieldType this Step starts on.
     * \returns	Returns the FieldType this Step starts on.
     */
    FieldType GetStartField( ) const;

    /**
     * \brief	Sets the FieldType this Step starts on.
     * \param	startField	the FieldType this Step starts on.
     */
    void SetStartField(FieldType startField);

    /**
     * \brief	Returns the position this Step starts on.
     * \returns	Returns the position this Step starts on.
     */
    uint8_t GetStartPosition( ) const;

    /**
     * \brief	Sets the position this Step starts on.
     * \param	startPosition	the position this Step starts on.
     */
    void SetStartPosition(uint8_t startPosition);
    /**
     * \brief	Returns the FieldType this Step stops on.
     * \returns	Returns the FieldType this Step stops on.
     */
    FieldType GetStopField( ) const;

    /**
     * \brief	Sets the FieldType this Step stops on.
     * \param	stopField	the FieldType this Step stops on.
     */
    void SetStopField(FieldType stopField);

    /**
     * \brief	Returns the position this Step stops on.
     * \returns	Returns the position this Step stops on.
     */
    uint8_t GetStopPosition( ) const;

    /**
     * \brief	Sets the position this Step stops on.
     * \param	stopPosition	the position this Step stops on.
     */
    void SetStopPosition(uint8_t stopPosition);

    /**
     * \brief	Gets if the this Step is active.
     * \returns	true if this step is active.
     */
    bool IsActive( ) const;

    /**
     * \brief	Actiates this Step.
     * \param	active	true will activate this Step; false deactivate.
     */
    void SetActive(bool active);

    /**
     * \brief	Returns the type of this step (putting a step into the goal, or
     * 			which transmission this Step will make.
     * \returns the type of this Step.
     */
    StepType GetStepType( ) const;

    bool operator==(const Step &obj) const;
    bool operator!=(const Step &obj) const;

private:
    bool _active;

    FieldType _startField;
    uint8_t _startPosition;

    FieldType _stopField;
    uint8_t _stopPosition;
};

#endif /* STEP_H_ */
