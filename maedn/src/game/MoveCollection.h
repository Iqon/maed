/**
 * \file	MoveCollection.h
 * \date	Jun 14, 2016
 * \author 	Aiko Isselhard
 * \brief	Contains all possible moves in the current situation.
 * */

#ifndef MOVECOLLECTION_H_
#define MOVECOLLECTION_H_

#include <global.h>
#include <vector>

#include <game/Move.h>

/**
 * \brief	Contains all possible moves in the current situation. Iterates over all possible moves.
 */
class MoveCollection {
public:
    MoveCollection( );
    virtual ~MoveCollection( );

    /**
     * \brief 	copies all moves to the given MoveCollection.
     * \param	collection	the target MoveCollection.
     */
    void CopyTo(MoveCollection *collection);

    /**
     * \brief	Gets the current Move, while iteration over all Move s
     * \returns the current Move.
     */
    Move *GetCurrentMove( ) const;

    /**
     * \brief	Selects the next move in the current iteration
     * \returns The next Move. If the last move was reached, the first will be returned.
     */
    Move *Next( );

    /**
     * \brief 	Adds the given Move to this MoveCollection.
     * \param	move	the Move that will be added to this MoveCollection
     */
    void AddMove(Move *move);

    /**
     * \brief 	Returns the number of Move s in this MoveCollection.
     * \returns Returns the number of Move s in this MoveCollection.
     */
    uint8_t GetCount( ) const;

private:
    uint8_t _pos;
    uint8_t _count;
    Move *_moves[0x04];
};

#endif /* MOVECOLLECTION_H_ */
