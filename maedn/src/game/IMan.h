/**
 * \file	IMan.h
 * \date	Jun 15, 2016
 * \author 	Aiko Isselhard
 * \brief	Represents one figure of one player.
 * */

#ifndef IMAN_H_
#define IMAN_H_

#include <global.h>
#include <game/FieldType.h>
#include <game/IPlayer.h>

class IPlayer;

/**
 * \brief	Represents one figure of one player.
 */
class IMan {
public:
    IMan( ) {
    }
    virtual ~IMan( ) {
    }

    /**
     * \brief	Returns the FieldType this IMan is stationed on.
     * \returns FieldType this IMan is stationed on.
     */
    virtual FieldType GetFieldType( ) const = 0;

    /**
     * \brief	Returns the position of this IMan on the Field, depends on
     * 			the current FieldType.
     * \returns	Returns the position of this IMan on the Field.
     */
    virtual uint8_t GetPosition( ) const = 0;

    /**
     * \brief	Clears the current position this IMan. Will remove him from any FieldCell he occupies
     *			and will hide the player.
     */
    virtual void ClearPosition( ) = 0;

    /**
     * \brief	Repositions this IMan to the given position on the given FieldType.
     * \param	position	the position of the IMan in the given FieldType
     * \param	manPosition	The FieldType this player will be placed in.
     */
    virtual void SetPosition(uint8_t position,
                             FieldType manPosition) = 0;

    /**
     * \brief 	Returns the parent IPlayer, owning this IMan.
     * \returns The Parent IPlayer
     */
    virtual IPlayer* GetParentPlayer( ) const = 0;
};

#endif /* IMAN_H_ */
