/**
 * \file	FieldType.h
 * \date	Jun 15, 2016
 * \author 	Aiko Isselhard
 * \brief	Indicates which objects indicates a field cell
 * */

#ifndef FIELDTYPE_H_
#define FIELDTYPE_H_

/**
 * \brief 	The MEADN field consists of several different areas. Each spot in this area can be
 * 			differentiated with this enumeration.
 */
enum FieldType {
    FieldTypeHouse = 0x00,
    FieldTypeGoal = 0x01,
    FieldTypeField = 0x02,
    FieldTypeHidden = 0x03
};

#endif /* FIELDTYPE_H_ */
