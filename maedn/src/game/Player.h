/**
 * \file	Player.h
 * \date	Jun 14, 2016
 * \author 	Aiko Isselhard
 * \brief	Represents one player.
 * */

#ifndef PLAYER_H_
#define PLAYER_H_

#include <global.h>

#include <game/IPlayer.h>
#include <game/FieldType.h>
#include <game/Man.h>

#include <io/output/DisplayDriver.h>
#include <io/output/rgb.h>

/**
 * \brief Represents one player.
 */
class Player : public IPlayer {
public:
    Player(rgb color,
           uint8_t player);
    virtual ~Player( );

    void Draw( );

    void ResetColor( );
    void SetColor(const rgb color);

    bool IsActive( ) const;
    void SetActive(const bool active);

    IMan *GetMan(const uint8_t no) const;
    rgb GetColor( ) const;

    uint8_t GetPlayer( ) const;
    const uint8_t GetStartPos( ) const;

private:
    DisplayDriver *_driver;
    rgb _color;
    rgb _origColor;
    bool _active;
    IMan *_mans[4];
    uint8_t _player;
    uint8_t _startPost;
};

#endif /* PLAYER_H_ */
