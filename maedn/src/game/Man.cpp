/**
 * \file	Man.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <game/Man.h>

Man::Man(IPlayer *parent,
         FieldType manPosition,
         uint8_t position) {
    this->_parentPlayer = parent;
    this->_manPosition = manPosition;
    this->_position = position;
}

Man::~Man( ) {
}

FieldType Man::GetFieldType( ) const {
    return this->_manPosition;
}

uint8_t Man::GetPosition( ) const {
    return this->_position;
}

void Man::ClearPosition( ) {
    if (this->_manPosition == FieldTypeField) {
        GameMgr::GetInstance()->ClearField(this->_position);
    }

    this->_manPosition = FieldTypeHidden;
}

void Man::SetPosition(uint8_t position,
                      FieldType manPosition) {
    if (this->_manPosition == FieldTypeField) {
        GameMgr::GetInstance()->ClearField(this->_position);
    }

    if (manPosition == FieldTypeField) {
        GameMgr::GetInstance()->SetField(position, this);
    }

    this->_manPosition = manPosition;
    this->_position = position;
}

IPlayer* Man::GetParentPlayer( ) const {
    return _parentPlayer;
}
