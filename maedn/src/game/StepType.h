/**
 * \file	StepType.h
 * \date	Jul 12, 2016
 * \author 	Aiko Isselhard
 * \brief	Indicates which transition this step is going to execute (e.g. from the field into the goal, etc.).
 * */

#ifndef STEPTYPE_H_
#define STEPTYPE_H_

/**
 * \brief Indicates which transition this step is going to execute (e.g. from the field into the goal, etc.).
 */
enum StepType {
    StepTypeUndefined = 0x00,
    StepTypeTransitionHouseField = 0x01,
    StepTypeTransitionFieldGoal = 0x02,
    StepTypeField = 0x03,
    StepTypeGoal = 0x4
};

#endif /* STEPTYPE_H_ */
