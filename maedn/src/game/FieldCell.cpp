/**
 * \file	FieldCell.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <game/FieldCell.h>

FieldCell::FieldCell( ) {
    this->Clear();
}

FieldCell::~FieldCell( ) {
}

void FieldCell::Clear( ) {
    this->_man = NULL;
}

const IMan* FieldCell::GetMan( ) const {
    return this->_man;
}

void FieldCell::SetMan(IMan* man) {
    this->_man = man;
}

const IPlayer* FieldCell::GetPlayer( ) const {
    if (this->_man != NULL) return this->_man->GetParentPlayer();

    return NULL;
}
