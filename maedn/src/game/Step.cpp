/**
 * \file	Step.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <game/Step.h>

Step::Step( ) {
    this->_active = false;

    this->_startField = FieldTypeHouse;
    this->_startPosition = 0x00;

    this->_stopField = FieldTypeHouse;
    this->_stopPosition = 0x00;
}

Step::Step(const Step *step) {
    this->_active = step->_active;

    this->_startField = step->_startField;
    this->_startPosition = step->_startPosition;

    this->_stopField = step->_stopField;
    this->_stopPosition = step->_stopPosition;
}

Step::~Step( ) {
}

bool Step::operator==(const Step &obj) const {
    return this->_active == obj._active && this->_startField == obj._startField && this->_startPosition == obj._startPosition && this->_stopField == obj._stopField
            && this->_stopPosition == obj._stopPosition;
}

bool Step::operator!=(const Step &obj) const {
    return !this->operator ==(obj);
}

FieldType Step::GetStartField( ) const {
    return this->_startField;
}

void Step::SetStartField(FieldType startField) {
    this->_startField = startField;
}

uint8_t Step::GetStartPosition( ) const {
    return this->_startPosition;
}

void Step::SetStartPosition(uint8_t startPosition) {
    this->_startPosition = startPosition;
}

FieldType Step::GetStopField( ) const {
    return this->_stopField;
}

void Step::SetStopField(FieldType stopField) {
    this->_stopField = stopField;
}

uint8_t Step::GetStopPosition( ) const {
    return this->_stopPosition;
}

void Step::SetStopPosition(uint8_t stopPosition) {
    this->_stopPosition = stopPosition;
}

bool Step::IsActive( ) const {
    return this->_active;
}

void Step::SetActive(bool active) {
    this->_active = active;
}

StepType Step::GetStepType( ) const {
    if (this->_startField == FieldTypeGoal && this->_stopField == FieldTypeField) {
        return StepTypeTransitionHouseField;
    }

    if (this->_startField == FieldTypeField && this->_stopField == FieldTypeField) {
        return StepTypeField;
    }

    if (this->_startField == FieldTypeField && this->_stopField == FieldTypeGoal) {
        return StepTypeTransitionFieldGoal;
    }

    if (this->_startField == FieldTypeGoal && this->_stopField == FieldTypeGoal) {
        return StepTypeGoal;
    }

    return StepTypeUndefined;
}
