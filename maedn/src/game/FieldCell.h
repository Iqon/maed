/**
 * \file	FieldCell.h
 * \date	Jun 15, 2016
 * \author 	Aiko Isselhard
 * \brief	Represents one dot on the field.
 * */

#ifndef FIELDCELL_H_
#define FIELDCELL_H_

#include <game/IMan.h>
#include <game/IPlayer.h>

/**
 * \brief	Represents one cell on the MAEDN field. A cell can either be empty or contain
 * 			any of the IPlayer 's.
 */
class FieldCell {
public:
    FieldCell( );
    virtual ~FieldCell( );

    /**
     * \brief	Returns the IMan currently positioned on this FieldCell, or null if no IMan
     * 			is stationed here.
     * \returns	the IMan currently positioned on this FieldCell, or null if no IMan is
     * 			stationed here
     */
    const IMan* GetMan( ) const;

    /**
     * \brief	Places the given IMan on this FieldCell
     * \param	man		the IMan to be placed on this FieldCell (or null)
     */
    void SetMan(IMan* man);

    /**
     * \brief	Returns the IPlayer of the IMan positioned on this FieldCell, or null if no
     * 			IMan occupies this FieldCell
     * \returns	IPlayer of the IMan positioned on this FieldCell, or null if no
     * 			IMan occupies this FieldCell
     */
    const IPlayer* GetPlayer( ) const;

    /**
     * \brief	Clears this FieldCell, removing the IMan stationed here.
     */
    void Clear( );

private:
    IMan *_man;
};

#endif /* FIELDCELL_H_ */
