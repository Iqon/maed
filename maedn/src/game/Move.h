/**
 * \file	Move.h
 * \date	Jun 14, 2016
 * \author 	Aiko Isselhard
 * \brief	Contains one possible move for the a play figure in the current situation.
 * */

#ifndef MOVE_H_
#define MOVE_H_

#include <global.h>

#include <game/IMan.h>
#include <game/FieldType.h>
#include <game/Step.h>

#define MOVE_MAX_STEP_COUNT 0x03 // for multiplier 3

/**
 * \brief	Contains one possible move for the a play figure in the current situation.
 */
class Move {
public:
    Move(uint8_t dice,
         IMan *man);
    Move(Move * move);
    virtual ~Move( );

    /**
     * \brief	Returns the number of IMan 's this Move will hit.
     * \returns Returns the number of IMan 's this Move will hit.
     */
    uint8_t GetHitCount( ) const;

    /**
     * \brief	Increments the number of IMan 's this Move will hit.
     */
    void IncrementHitCount( );

    /**
     * \brief	Returns the IMan possibly executing this Move.
     * \returns	the IMan possibly executing this Move.
     */
    IMan *GetMan( ) const;

    /**
     * \brief	Gets the number of Step s this Move will execute. Is set by the dice multipliyer
     * 			and the repetitions current field allows..
     * \returns	Gets the number of Step s this Move will execute.
     */
    uint8_t GetStepCount( ) const;

    /**
     * \brief	Returns the dice count that produced this Move.
     * \returns	Returns the dice count that produced this Move.
     */
    uint8_t GetDice( ) const;

    /**
     * \brief	Returns the specified Step.
     * \returns the specified Step.
     */
    Step *GetStep(const uint8_t no);

    bool operator==(const Move &obj) const;
    bool operator!=(const Move &obj) const;

private:
    uint8_t _dice;
    IMan * _man;
    uint8_t _hitCount;
    Step *_steps[MOVE_MAX_STEP_COUNT];
};

#endif /* MOVE_H_ */
