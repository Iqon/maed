/**
 * \file	Player.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <game/Player.h>

Player::Player(rgb color,
               uint8_t player) {
    this->_active = true;
    this->_driver = DisplayDriver::GetInstance();
    this->_player = player;
    this->_color = color;
    this->_origColor = color;
    this->_startPost = (ROUND_LEN / 0x04) * player;

    for (unsigned char i = 0x00; i < 0x04; ++i) {
        this->_mans[i] = new Man(this, FieldTypeHouse, i);
    }
}

Player::~Player( ) {
    for (unsigned char i = 0x00; i < 0x04; ++i) {
        delete this->_mans[i];
    }
}

void Player::ResetColor( ) {
    this->_color = this->_origColor;
}

void Player::SetColor(const rgb color) {
    this->_color = color;
}

void Player::Draw( ) {
    for (uint8_t i = 0x00; i < 0x04; ++i) {
        switch (this->_mans[i]->GetFieldType()) {
        case FieldTypeHouse:
            this->_driver->House(this->_player, this->_mans[i]->GetPosition(), this->_color);
        break;

        case FieldTypeGoal:
            this->_driver->Goal(this->_player, this->_mans[i]->GetPosition(), this->_color);
        break;

        case FieldTypeField:
            this->_driver->Field(this->_mans[i]->GetPosition(), this->_color);
        break;

        case FieldTypeHidden:
        break;
        }
    }
}

bool Player::IsActive( ) const {
    return this->_active;
}

void Player::SetActive(bool active) {
    this->_active = active;
}

rgb Player::GetColor( ) const {
    return this->_color;
}

IMan *Player::GetMan(uint8_t no) const {
    return this->_mans[no % 0x04];
}

uint8_t Player::GetPlayer( ) const {
    return this->_player;
}

const uint8_t Player::GetStartPos( ) const {
    return this->_startPost;
}
