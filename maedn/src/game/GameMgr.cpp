/**
 * \file	GameMgr.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <game/GameMgr.h>

GameMgr * GameMgr::s_instance = new GameMgr();

GameMgr::GameMgr( ) {
    this->_draw = true;

    this->_player[0] = new Player(COLOR_PLAYER0, 0);
    this->_player[1] = new Player(COLOR_PLAYER1, 1);
    this->_player[2] = new Player(COLOR_PLAYER2, 2);
    this->_player[3] = new Player(COLOR_PLAYER3, 3);

    for (uint8_t i = 0x00; i < ROUND_LEN; ++i) {
        this->_field[0].SetMan(0);
    }
}

GameMgr::~GameMgr( ) {
    delete this->_player[0];
    delete this->_player[1];
    delete this->_player[2];
    delete this->_player[3];
}

void GameMgr::Draw( ) {
    if (!this->_draw) return;

    for (uint8_t i = 0x00; i < GAME_MGR_PLAYER_COUNT; ++i) {
        if (this->_player[i]->IsActive()) this->_player[i]->Draw();
    }
}

bool GameMgr::IsDraw( ) const {
    return this->_draw;
}

void GameMgr::SetDraw(bool draw) {
    this->_draw = draw;
}

IPlayer *GameMgr::GetPlayer(uint8_t no) {
    return this->_player[no % GAME_MGR_PLAYER_COUNT];
}

uint8_t GameMgr::GetCurrentPlayer( ) const {
    return _currentPlayer;
}

uint8_t GameMgr::GetPlayerCount( ) const {
    return GAME_MGR_PLAYER_COUNT;
}

void GameMgr::SetCurrentPlayer(int currentPlayer) {
    _currentPlayer = currentPlayer;
}

FieldCell* GameMgr::GetField(uint8_t i) {
    return &this->_field[i % ROUND_LEN];
}

void GameMgr::ClearField(uint8_t i) {
    this->_field[i % ROUND_LEN].Clear();
}

void GameMgr::SetField(uint8_t i,
                       IMan *man) {
    this->_field[i % ROUND_LEN].SetMan(man);
}
