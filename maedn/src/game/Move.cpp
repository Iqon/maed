/**
 * \file	Move.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <game/Move.h>

Move::Move(uint8_t dice,
           IMan *man) {
    this->_dice = dice;
    this->_man = man;
    this->_hitCount = 0x00;

    for (uint8_t i = 0x00; i < MOVE_MAX_STEP_COUNT; ++i) {
        this->_steps[i] = new Step();
    }
}

Move::Move(Move *move) {
    this->_dice = move->_dice;
    this->_man = move->_man;
    this->_hitCount = move->_hitCount;

    for (uint8_t i = 0x00; i < MOVE_MAX_STEP_COUNT; ++i) {
        this->_steps[i] = new Step(move->_steps[i]);
    }
}

Move::~Move( ) {
    for (uint8_t i = 0x00; i < MOVE_MAX_STEP_COUNT; ++i) {
        delete this->_steps[i];
    }
}

uint8_t Move::GetHitCount( ) const {
    return this->_hitCount;
}

uint8_t Move::GetDice( ) const {
    return this->_dice;
}

void Move::IncrementHitCount( ) {
    this->_hitCount++;
}

IMan *Move::GetMan( ) const {
    return this->_man;
}

Step *Move::GetStep(const uint8_t no) {
    return this->_steps[no % MOVE_MAX_STEP_COUNT];
}

uint8_t Move::GetStepCount( ) const {
    for (uint8_t i = 0x00; i < MOVE_MAX_STEP_COUNT; ++i) {
        if (this->_steps[i]->IsActive() == false) return i;
    }

    return MOVE_MAX_STEP_COUNT;
}

bool Move::operator==(const Move &obj) const {
    if (this->_dice == obj._dice && this->_hitCount == obj._hitCount && this->_man == obj._man) {
        for (uint8_t i = 0x00; i < MOVE_MAX_STEP_COUNT; ++i) {
            if ((*this->_steps[i]) != (*(obj._steps[i]))) {
                return false;
            }
        }
        return true;
    }
    return false;
}

bool Move::operator!=(const Move &obj) const {
    return !(this->operator ==(obj));
}
