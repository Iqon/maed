/**
 * \file	Random.h
 * \date	Jun 14, 2016
 * \author 	Aiko Isselhard
 * \brief	Allows easy access to the random number generator.
 * */

#ifndef RANDOM_H_
#define RANDOM_H_

#include <global.h>
#include <stm32f4xx_rng.h>

/**
 * \brief	Allows easy access to the random number generator.
 */
class Random {
public:
    virtual ~Random( );

    /**
     * \brief 	Returns a random number min <= rnd < max.
     * \param 	min 	the lower border for the random number (inclusive)
     * \param	max		the upper border for the random number (exclusive)
     * \returns the random number min <= rnd < max.
     */
    uint32_t GetRandomUInt32(uint32_t min,
                             uint32_t max);

    /**
     * \brief Returns the singleton instance of the Random.
     *
     * \returns the singleton instance of Random.
     */
    static Random* GetInstance( ) {
        static Random *rnd = new Random();
        return rnd;
    }

private:
    Random( );
};

#endif /* RANDOM_H_ */
