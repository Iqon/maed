/**
 * \file	Man.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Represents one figure of one player.
 * */

#ifndef MAN_H_
#define MAN_H_

#include <global.h>

#include <game/FieldType.h>
#include <game/GameMgr.h>
#include <game/IPlayer.h>
#include <game/IMan.h>

/**
 * \brief	Represents one figure of one player.
 */
class Man : public IMan {
public:
    Man(IPlayer *parent,
        FieldType manPosition,
        uint8_t position);
    virtual ~Man( );

    FieldType GetFieldType( ) const;

    uint8_t GetPosition( ) const;
    void ClearPosition( );
    void SetPosition(uint8_t position,
                     FieldType manPosition);

    IPlayer* GetParentPlayer( ) const;

private:
    FieldType _manPosition;
    uint8_t _position;
    IPlayer *_parentPlayer;
};

#endif /* MAN_H_ */
