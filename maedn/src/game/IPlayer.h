/**
 * \file	IPlayer.h
 * \date	Jun 15, 2016
 * \author 	Aiko Isselhard
 * \brief	Represents one player.
 * */

#ifndef IPLAYER_H_
#define IPLAYER_H_

#include <global.h>
#include <game/IMan.h>
#include <io/output/rgb.h>

class IMan;

/**
 * \brief	Represents one player.
 */
class IPlayer {
public:
    IPlayer( ) {
    }
    virtual ~IPlayer( ) {
    }

    /**
     * \brief 	Draws all IMan of the current player.
     */
    virtual void Draw( ) = 0;

    /**
     * \brief   Resets the IPlayer s color the the originial color, given in its constructor.
     */
    virtual void ResetColor( ) = 0;

    /**
     * \brief   Sets the IPlayer s temporarily color.
     */
    virtual void SetColor(const rgb color) = 0;

    /**
     * \brief 	Determines if the current player is active (participating in the current game).
     * \returns	true if the current player is participating in the current game; otherwise false.
     */
    virtual bool IsActive( ) const = 0;

    /**
     * \brief 	Set the current player active status (participating in the current game).
     * \param 	active 	true if the current player is participating in the current game; otherwise false.
     */
    virtual void SetActive(const bool active) = 0;

    /**
     * \brief	Returns the specified IMan of this IPlayer.
     * \param	no	The number of the IMan.
     */
    virtual IMan *GetMan(const uint8_t no) const = 0;

    /**
     * \brief	Returns the current IPlayer 's color
     * \returns Returns the current IPlayer 's color.
     */
    virtual rgb GetColor( ) const = 0;

    /**
     * \brief 	Returns the current IPlayers number.
     * \returns Returns the current IPlayers number.
     */
    virtual uint8_t GetPlayer( ) const = 0;

    /**
     * \brief 	Returns the current IPlayers start position.
     * \returns Returns the current IPlayers start position.
     */
    virtual const uint8_t GetStartPos( ) const = 0;
};

#endif /* IPLAYER_H_ */
