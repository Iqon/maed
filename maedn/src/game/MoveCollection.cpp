/**
 * \file	MoveCollection.cpp
 * \brief	Implementation.
 * \author 	Aiko Isselhard
 */

#include <game/MoveCollection.h>

MoveCollection::MoveCollection( ) {
    this->_count = 0x00;
    this->_pos = 0x00;

    for (uint8_t i = 0x00; i < 0x04; ++i)
        this->_moves[i] = 0x00;
}

MoveCollection::~MoveCollection( ) {
    for (uint8_t i = 0x00; i < 0x04; ++i)
        if (this->_moves[i] != 0x00) delete this->_moves[i];
}

void MoveCollection::CopyTo(MoveCollection *collection) {
    for (uint8_t i = 0x00; i < 0x04; ++i) {
        collection->_moves[i] = this->_moves[i];
        this->_moves[i] = 0x00;
    }

    collection->_count = this->_count;
    this->_count = 0x00;
}

Move *MoveCollection::GetCurrentMove( ) const {
    return this->_moves[this->_pos];
}

Move *MoveCollection::Next( ) {
    this->_pos = (this->_pos + 1) % this->_count;
    return this->GetCurrentMove();
}

void MoveCollection::AddMove(Move *move) {
    this->_moves[this->_count] = move;
    ++this->_count;
}

uint8_t MoveCollection::GetCount( ) const {
    return this->_count;
}
