/**
 * \file	GameMgr.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Manages the game's field.
 */

#ifndef GAME_H_
#define GAME_H_

#include <global.h>
#include <game/Player.h>
#include <game/FieldCell.h>
#include <io/output/DisplayDriver.h>

#define GAME_MGR_PLAYER_COUNT 0x04

/**
 * \brief 	The GameMgr holds and manages the games state. It knowns the field, every player
 * 			and is responsible for drawing the game.
 */
class GameMgr {
public:
    virtual ~GameMgr( );

    /**
     * \brief 	Draws the gurrent game's state. Will draw all available and active players.
     */
    void Draw( );

    /**
     * \brief 	Returns the singleton instance of the WS2801LEDDriver.
     * \return	The singleton instance of WS2801LEDDriver.
     */
    static GameMgr* GetInstance( ) {
        return GameMgr::s_instance;
    }

    /**
     * \brief 	Resets the games state, effectively restarting the game.
     */
    static void Reset( ) {
        delete GameMgr::s_instance;
        GameMgr::s_instance = new GameMgr();
    }

    /**
     * \brief 	Returns the IPlayer instance for the given player.
     * \param 	no	the number of the player returned
     * \return	the IPlayer instance.
     */
    IPlayer *GetPlayer(uint8_t no);

    /**
     * \brief 	Checks whether the current game state is drawn or if drawing was disabled.
     * \return 	true if the drawing is enabled; otherwise false.
     */
    bool IsDraw( ) const;

    /**
     * \brief	Enables or disables the drawing of the current games state.
     * \param	draw	true if the drawing should be enabled; otherwise drawing is disabled.
     */
    void SetDraw(bool draw);

    /**
     * \brief	Gets the player number of the current active IPlayer.
     * \return	player number of the current active IPlayer.
     */
    uint8_t GetCurrentPlayer( ) const;

    /**
     * \brief   Returns the count of the current IPlayer.
     * \return  Returns the count of the current IPlayer.
     */
    uint8_t GetPlayerCount( ) const;

    /**
     * \brief	Sets the current active IPlayer.
     * \param	currentPlayer	the number of the next active IPlayer.
     */
    void SetCurrentPlayer(int currentPlayer);

    /**
     * \brief	Returns the instance of the given FieldCell.
     * \param	pos		The index of the to be retrieved FieldCell
     * \return	The instance of the FieldCell
     */
    FieldCell* GetField(uint8_t pos);

    /**
     * \brief 	Places the given man on the specified FieldCell.
     * \param	pos		The index of the target FieldCell
     * \param	man		The man placed on the specified FieldCell
     */
    void SetField(uint8_t pos,
                  IMan *man);

    /**
     * \brief 	Clears the specified FieldCell
     * \param	pos		The index of the to be cleared FieldCell
     */
    void ClearField(uint8_t pos);

private:
    GameMgr( );

    static GameMgr *s_instance;
    bool _draw;
    uint8_t _currentPlayer;
    IPlayer *_player[GAME_MGR_PLAYER_COUNT];

    FieldCell _field[ROUND_LEN];
};

#endif /* GAME_H_ */
