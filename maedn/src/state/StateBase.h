/**
 * \file	StateBase.h
 * \date	Jun 4, 2016
 * \author 	Aiko Isselhard
 * \brief	Common base class for all states. Contains all functionality shared
 * 			between all types of states.
 * 			Manages all behind the scenes stuff and all sub and child states.
 * */

#ifndef STATEBASE_H_
#define STATEBASE_H_

#include <list>

#include <state/StateMgr.h>
#include <state/IState.h>
#include <io/input/InputMgr.h>
#include <game/GameMgr.h>
#include <io/output/DisplayDriver.h>

#define MAX_SUBSTATES   5

/**
 * \brief	Common base class for all states. Contains all functionality shared
 * 			between all types of states.
 * 			Manages all behind the scenes stuff and all sub and child states.
 */
class StateBase : public IState {
public:
    StateBase( );
    virtual ~StateBase( );

    virtual void InternalEnter(void *);
    virtual void Update(uint32_t diff) = 0;
    virtual void * InternalLeave( );
    virtual void InternalUpdate(uint32_t diff);
    StateBase* GetParent( );
    virtual void Stop( );
    virtual bool IsCurrentActiveState( );

    /**
     * \brief	Allowing each state to implement custom enter functionality.
     * \param	para	Optional enter value
     */
    virtual void EnterImplementation(void * para) = 0;

    /**
     * \brief	Allowing each state to implement custom leave functionality.
     * \returns optional return value.
     **/
    virtual void * LeaveImplementation( ) = 0;

protected:

    void AddChildState(StateBase *state);
    void AddChildState(StateBase *state,
                       void * para);
    void EnterSubState(StateBase *state);
    void EnterNextState(StateBase *state);

    GameMgr *Game( ) const {
        return GameMgr::GetInstance();
    }
    DisplayDriver *Display( ) const {
        return DisplayDriver::GetInstance();
    }
    StateMgr *States( ) const {
        return StateMgr::GetInstance();
    }
    InputMgr *Input( ) const {
        return InputMgr::GetInstance();
    }

    uint32_t _runningTime;

private:
    StateBase * _parent;
    StateBase *_childStates[MAX_SUBSTATES];

    void RemoveTraces( );
    void RemoveFromParent( );
    void RemoveChildren( );

    bool IsChildState(StateBase *base,
                      std::list<StateBase*>::iterator *out);
};

#endif /* STATEBASE_H_ */
