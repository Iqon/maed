/**
 * \file	StateBase.cpp
 * \date	Jun 1, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <state/StateBase.h>

StateBase::StateBase( ) {
    this->_runningTime = 0x00;
    this->_parent = NULL;

    for(int i = 0x00; i < MAX_SUBSTATES; ++i) {
        this->_childStates[i] = NULL;
    }
}

StateBase::~StateBase( ) {
    this->RemoveTraces();
}

void StateBase::InternalUpdate(uint32_t diff) {
    this->_runningTime += diff;

    for(int i = 0x00; i < MAX_SUBSTATES; ++i) {
        if(this->_childStates[i] != null) {
            this->_childStates[i]->InternalUpdate(diff);
        }
    }

    this->Update(diff);
}

StateBase* StateBase::GetParent( ) {
    return this->_parent;
}

void StateBase::Stop( ) {
    if (this->IsCurrentActiveState()) {
        this->States()->StopCurrentState();
    } else {
        this->InternalLeave();
        delete this;
    }
}

bool StateBase::IsCurrentActiveState( ) {
    return this->States()->CurrentActiveState() == this;
}

void StateBase::AddChildState(StateBase *state) {
    this->AddChildState(state, NULL);
}

void StateBase::AddChildState(StateBase *state,
                              void *para) {
    for(int i = 0x00; i < MAX_SUBSTATES; ++i) {
        if(this->_childStates[i] == null) {
            this->_childStates[i] = state;
            break;
        }
    }

    state->_parent = this;
    state->InternalEnter(para);
}

void StateBase::EnterSubState(StateBase *state) {
    state->_parent = this;
    this->States()->SwitchToSubState(state);
}

void StateBase::EnterNextState(StateBase *state) {
    state->_parent = this->_parent;
    this->States()->SwitchToState(state);
}

void StateBase::InternalEnter(void *para) {
    this->EnterImplementation(para);
}

void * StateBase::InternalLeave( ) {
    this->RemoveTraces();
    return this->LeaveImplementation();
}

void StateBase::RemoveFromParent( ) {
    if(this->_parent == NULL)
        return;

    for(int i = 0x00; i < MAX_SUBSTATES; ++i) {
        if(this->_parent->_childStates[i] == this) {
            this->_parent = NULL;
        }
    }
}

void StateBase::RemoveChildren( ) {
    for(int i = 0x00; i < MAX_SUBSTATES; ++i) {
        if(this->_childStates[i] != NULL) {
            delete _childStates[i];
            this->_childStates[i] = NULL;
        }
    }
}

void StateBase::RemoveTraces( ) {
    this->RemoveFromParent();
    this->RemoveChildren();
}
