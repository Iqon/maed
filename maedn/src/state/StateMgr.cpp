/**
 * \file	StateMgr.cpp
 * \date	Jun 1, 2016
 * \author 	Aiko Isselhard
 * \brief	Implementation
 * */

#include <state/StateMgr.h>

StateMgr::StateMgr( ) {
    this->_currentState = 0x00;
}

StateMgr::~StateMgr( ) {
    if (this->_currentState != 0) {
        delete this->_currentState;
    }
}

void StateMgr::Update(uint32_t diff) {
    if (this->_currentState != 0) {
        this->_currentState->InternalUpdate(diff);
    } else {
        // TODO: Go to in error state;
    }
}

void StateMgr::StopCurrentState( ) {
    IState *current = this->_currentState;
    IState *parent = this->_currentState->GetParent();
    void * res = this->_currentState->InternalLeave();

    if (parent != 0) {
        this->_currentState = parent;
        this->_currentState->InternalEnter(res);
    }

    delete current;
}

void StateMgr::SwitchToState(IState *state) {
    IState *current = this->_currentState;

    void *res = 0x00;
    if (this->_currentState != 0x00) {
        res = this->_currentState->InternalLeave();
    }

    this->_currentState = state;
    this->_currentState->InternalEnter(res);

    if (current != 0x00) {
        delete current;
    }
}

void StateMgr::SwitchToSubState(IState *state) {
    void * res = 0;
    if (this->_currentState != 0) {
        res = this->_currentState->InternalLeave();
    }

    this->_currentState = state;
    this->_currentState->InternalEnter(res);
}

IState *StateMgr::CurrentActiveState( ) {
    return this->_currentState;
}
