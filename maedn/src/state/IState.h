/**
 * \file	IState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	Base interface for a game state.
 * */

#ifndef ISTATE_H_
#define ISTATE_H_

#include <global.h>

/**
 * \brief	Base interface for a game state.
 */
class IState {
public:
    IState( ) {
    }
    virtual ~IState( ) {
    }

    /**
     * \brief	[FOR INTERNAL USAGE ONLY. DO NOT CALL.] Called when the IState gets activated. Optionally a pointer of some data can be passed.
     */
    virtual void InternalEnter(void *) = 0;

    /**
     * \brief	Called every frame to update all currently active states.
     * \param	diff	The dime difference in ms since the last update.
     */
    virtual void Update(uint32_t diff) = 0;

    /**
     * \brief	[FOR INTERNAL USAGE ONLY. DO NOT CALL.] Called when the IState gets deactivated.
     * \returns	Optionally a pointer of some data can be returned.
     */
    virtual void * InternalLeave( ) = 0;

    /**
     * \brief	[FOR INTERNAL USAGE ONLY. DO NOT CALL.] Called every frame to update all currently active states.
     * \param	diff	The dime difference in ms since the last update.
     */
    virtual void InternalUpdate(uint32_t diff) = 0;

    /**
     * \brief	Returns this IState parent IState, if there is one.
     * \returns this IState parent IState, if there is one.
     */
    virtual IState* GetParent( ) = 0;

    /**
     * \brief 	Stops this IState and all Child- and Sub- IState s and removes itself from the StateMgr.
     * \remarks Will delete this IState and free its memory.
     */
    virtual void Stop( ) = 0;

    /**
     * \brief 	Returns true if this IState is the current active IState.
     * \returns if this IState is the current active IState.
     */
    virtual bool IsCurrentActiveState( ) = 0;
};

#endif /* ISTATE_H_ */
