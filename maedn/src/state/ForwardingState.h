/**
 * \file	ForwardingState.h
 * \date	Jun 7, 2016
 * \author 	Aiko Isselhard
 * \brief	This state will accept any value and forward it. The value received on
 * 			entering will be returned on leaving.
 * */

#ifndef FORWADINGSTATE_H_
#define FORWADINGSTATE_H_

#include <stdlib.h>
#include <state/StateBase.h>

/**
 * \brief	This state will accept any value and forward it. The value received on
 * 			entering will be returned on leaving.
 */
template<typename TForward>
class ForwardingState : public StateBase {
public:
    ForwardingState(StateBase *next) {
        _next = next;
        this->_data = malloc(sizeof(TForward));
    }

    virtual ~ForwardingState( ) {
        free(this->_data);
    }

    virtual void Enter( ) = 0;
    virtual void Update(uint32_t diff) = 0;
    virtual void Leave( ) = 0;

    virtual void EnterImplementation(void * obj) {
        if(obj != NULL) {
            memcpy(this->_data, obj, sizeof(TForward));
        }
    }

    virtual void *LeaveImplementation( ) {
        return this->_data;
    }

protected:
    virtual void Stop( ) {
        this->EnterNextState(this->_next);
    }

private:
    StateBase *_next;
    void * _data;
};

#endif /* FORWADINGSTATE_H_ */
