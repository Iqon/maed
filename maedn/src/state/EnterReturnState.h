/**
 * \file	EnterReturnState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	This state accepts a value on entering and will return a value on leaving.
 * */

#ifndef ENTERRETURNSTATE_H_
#define ENTERRETURNSTATE_H_

#include <state/StateBase.h>

/**
 * \brief	This state accepts a value on entering and will return a value on leaving.
 */
template<typename TEnter, typename TLeave>
class EnterReturnState : public StateBase {
public:
    EnterReturnState( ) {
    }
    virtual ~EnterReturnState( ) {
    }

    virtual void Enter(TEnter *obj) = 0;
    virtual void Update(uint32_t diff) = 0;
    virtual TLeave *Leave( ) = 0;

    virtual void EnterImplementation(void *obj) {
        this->Enter((TEnter *) obj);
    }

    virtual void *LeaveImplementation( ) {
        return (void *) this->Leave();
    }
};

#endif /* ENTERRETURNSTATE_H_ */
