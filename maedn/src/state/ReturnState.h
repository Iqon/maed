/**
 * \file	ReturnState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	States of the type ReturnState will return a value upon leaving.
 * 			If the following state is of type EnterState, it will receive this
 * 			value on entering.
 * */

#ifndef RETURNSTATE_H_
#define RETURNSTATE_H_

#include <state/StateBase.h>

/**
 * \brief	States of the type ReturnState will return a value upon leaving.
 * 			If the following state is of type EnterState, it will receive this
 * 			value on entering.
 */
template<typename TLeave>
class ReturnState : public StateBase {
public:
    ReturnState( ) {
    }
    virtual ~ReturnState( ) {
    }

    virtual void Enter( ) = 0;
    virtual void Update(uint32_t diff) = 0;
    virtual TLeave *Leave( ) = 0;

    virtual void EnterImplementation(void *obj) {
        this->Enter();
    }

    virtual void *LeaveImplementation( ) {
        return (void *) this->Leave();
    }
};

#endif /* RETURNSTATE_H_ */
