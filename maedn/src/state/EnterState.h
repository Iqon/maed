/**
 * \file	EnterState.h
 * \date	Jun 5, 2016
 * \author 	Aiko Isselhard
 * \brief	A state which accepts a value on entering. The previous state must be of type ReturnState,
 * 			otherwise this state will receive the value null.
 * */

#ifndef ENTERSTATE_H_
#define ENTERSTATE_H_

#include <state/StateBase.h>

/**
 * \brief	A state which accepts a value on entering. The previous state must be of type ReturnState,
 * 			otherwise this state will receive the value null.
 */
template<typename TEnter>
class EnterState : public StateBase {
public:
    EnterState( ) {
    }
    virtual ~EnterState( ) {
    }

    virtual void Enter(TEnter *obj) = 0;
    virtual void Update(uint32_t diff) = 0;
    virtual void Leave( ) = 0;

    virtual void EnterImplementation(void *obj) {
        this->Enter((TEnter *) obj);
    }

    virtual void *LeaveImplementation( ) {
        this->Leave();
        return 0;
    }
};

#endif /* ENTERSTATE_H_ */
