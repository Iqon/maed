/**
 * \file	StateMgr.h
 * \date	Jun 1, 2016
 * \author 	Aiko Isselhard
 * \brief	The state manager manages and executes the game state.
 * 			Is responsible for releasing the states memory, for state
 * 			transitions and for executing all active states.
 * */

#ifndef STATEMGR_H_
#define STATEMGR_H_

#include <global.h>
#include <state/IState.h>

/**
 * \brief	The state manager manages and executes the game state.
 * 			Is responsible for releasing the states memory, for state
 * 			transitions and for executing all active states.
 */
class StateMgr {
public:
    virtual ~StateMgr( );

    /**
     * \brief 	Updates the current active states.
     * \param	diff	The time difference since the last update.
     */
    void Update(uint32_t diff);

    /**
     * \brief	Stops the current active IState and all its Child- and Sub- IState s.
     */
    void StopCurrentState( );

    /**
     * \brief	Will activate the given IState without releasing the current. If the given IState then stops,
     * 			the parent can be reactivated.
     * \param	state	the IState which will be activated.
     */
    void SwitchToSubState(IState *state);

    /**
     * \brief	Will release the current IState and activate the given IState.
     * \param	state	the IState which will be activated.
     */
    void SwitchToState(IState *state);

    /**
     * \brief   Returns the singleton instance of StateMgr.
     * \returns the singleton instance of StateMgr.
     */
    static StateMgr* GetInstance( ) {
        static StateMgr *mgr = new StateMgr();
        return mgr;
    }

    /**
     * \brief   Returns the the current active state.
     * \returns the the current active state.
     */
    IState *CurrentActiveState( );

private:
    StateMgr( );

    IState *_currentState;
};

#endif /* STATEMGR_H_ */
