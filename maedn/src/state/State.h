/**
 * \file	State.h
 * \date	Jun 1, 2016
 * \author 	Aiko Isselhard
 * \brief	Simple state which does not accept any values on entering and does not return any values on leaving.
 * */

#ifndef STATE_H_
#define STATE_H_

#include <vector>

#include <state/IState.h>
#include <state/StateBase.h>

/**
 * \brief 	Simple state which does not accept any values on entering and does not return any values on leaving.
 */
class State : public StateBase {
public:
    State( ) {
    }
    virtual ~State( ) {
    }

    virtual void Enter( ) = 0;
    virtual void Update(uint32_t diff) = 0;
    virtual void Leave( ) = 0;

    virtual void EnterImplementation(void *) {
        this->Enter();
    }

    virtual void *LeaveImplementation( ) {
        this->Leave();
        return 0;
    }
};

#endif /* STATE_H_ */
