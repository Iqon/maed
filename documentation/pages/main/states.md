\page states Game Logic
[TOC]

# Game Logic {#Gamelocig}

## Main Entry {#Entry}

The main state is the GameStartState. This state will never stop. It calls the SelectNoOfPlayerState. If this state stops it will clean and reset the games state and start over. For details see \ref Start.

\dot
   digraph g {
      graph [fontname="Segoe UI", fontsize=10];
      node [fontname="Segoe UI", fontsize=10];
      edge [fontname="Segoe UI", fontsize=10];
      GameStartState [URL="\ref GameStartState"];
      FindFirstPlayerState [URL="\ref FindFirstPlayerState"];
      SelectNoOfPlayerState [URL="\ref SelectNoOfPlayerState"];

      GameStartState -> SelectNoOfPlayerState [label="Starts the game (Substate)"];
      SelectNoOfPlayerState -> FindFirstPlayerState;
      FindFirstPlayerState -> FindFirstPlayerState [label="In case of a draw"];
      FindFirstPlayerState -> Continue;      
   }
\enddot

## Rolling the dice. {#Dice}

This describes the workflow of rolling a dice. If other sections reference the RollDiceState, it always means this workflow. The RollDiceState yields a DiceResult, containing the results of the throw. Will use the current active player from Game, as source the player throwing the dice.

\dot
   digraph g {
      graph [fontname="Segoe UI", fontsize=10];
      node [fontname="Segoe UI", fontsize=10];
      edge [fontname="Segoe UI", fontsize=10];
      DiceState [URL="\ref DiceState"];
      RollDiceState [URL="\ref RollDiceState"];
      MultiplierState [URL="\ref MultiplierState"];
      DisplayDiceResultState [URL="\ref DisplayDiceResultState"];

      DiceState->RollDiceState [label="Waits button press"];
      RollDiceState -> MultiplierState [label="uint8_t, while the player holds button"];
      MultiplierState -> DisplayDiceResultState [label="DiceResult, while the player holds button"];
      DisplayDiceResultState -> "Stop";
   }
\enddot

## Gamestart {#Start}

Gamestart directly called from GameStartState. After finding first player, go to SelectRollDiceState, see \ref Running.

\dot
   digraph g {
      graph [fontname="Segoe UI", fontsize=10];
      node [fontname="Segoe UI", fontsize=10];
      edge [fontname="Segoe UI", fontsize=10];
      SelectNoOfPlayerState [URL="\ref SelectNoOfPlayerState"];
      DiceState [URL="\ref DiceState"];
      FindFirstPlayerState [URL="\ref FindFirstPlayerState"];
      DisplayWinnerState [URL="\ref DisplayWinnerState"];
      SelectRollDiceState [URL="\ref SelectRollDiceState"];

      SelectNoOfPlayerState -> FindFirstPlayerState [label="Players activate their slots"];
      FindFirstPlayerState -> DisplayWinnerState [label="to get starting player and/or solve a draw"];
      FindFirstPlayerState -> FindFirstPlayerState [label="In case of draw, repeat self"];
      DisplayWinnerState -> SelectRollDiceState;

      FindFirstPlayerState -> DiceState [label="Throw dice for each active player"];
      DiceState -> FindFirstPlayerState [label="Returns throw results"];
   }
\enddot

## Execute move {#Execute}

The move is selected by ChooseMoveState. See \ref Choose for details.

\dot
      digraph g {
            graph [fontname="Segoe UI", fontsize=10];
            node [fontname="Segoe UI", fontsize=10];
            edge [fontname="Segoe UI", fontsize=10];
            ChooseMoveState [URL="\ref ChooseMoveState"];
            ExecuteMoveState [URL="\ref ExecuteMoveState"];
            PunishPlayerState [URL="\ref PunishPlayerState"];
            ChangeColorState [URL="\ref ChangeColorState"];
            NormalMoveState [URL="\ref NormalMoveState"];
            WalkManHomeState [URL="\ref WalkManHomeState"];
            WalkManHomeState2 [label="WalkManHomeState", URL="\ref WalkManHomeState"];
            MoveManState [URL="\ref MoveManState"];

            ChooseMoveState -> ExecuteMoveState; 
            ExecuteMoveState -> ChangeColorState [label="In case of punishment"];            
            ExecuteMoveState -> NormalMoveState;
            ChangeColorState -> PunishPlayerState [label="After changing player color to pink"];
            PunishPlayerState -> WalkManHomeState;
            WalkManHomeState -> "Stop";
            NormalMoveState -> MoveManState [label="Once for each step (max 3 in case of multiplyer of 3)"]
            MoveManState -> NormalMoveState;
            NormalMoveState -> WalkManHomeState2 [label="Walk opponent home on hit"]
            WalkManHomeState2 -> NormalMoveState;
            NormalMoveState -> "Stop";
      }
\enddot

## Choose move {#Choose}

Choose move, fins all possible moves and allows the player to choose between those.

\dot
   digraph g {
      graph [fontname="Segoe UI", fontsize=10];
      node [fontname="Segoe UI", fontsize=10];
      edge [fontname="Segoe UI", fontsize=10];
      FindMovesState [URL="\ref FindMovesState"];
      ChooseMoveState [URL="\ref ChooseMoveState"];
      ExecuteMoveState [URL="\ref ExecuteMoveState"];
      FindNextPlayerState [URL="\ref FindNextPlayerState"];
      DisplayMoveState [URL="\ref DisplayMoveState"];
      
      FindMovesState -> ChooseMoveState [label="After finding all states"];
      ChooseMoveState-> FindNextPlayerState [label="If there are no possible moves"];
      ChooseMoveState -> ExecuteMoveState [label="After selecting the one move"];
      ChooseMoveState -> DisplayMoveState [label="ChildState, used to display currently selected state"];
   }
\enddot

## Running game {#Running}

After rolling we go to \ref Choose.

\dot
   digraph g {
      graph [fontname="Segoe UI", fontsize=10];
      node [fontname="Segoe UI", fontsize=10];
      edge [fontname="Segoe UI", fontsize=10];
      FindNextPlayerState [URL="\ref FindNextPlayerState"];
      SelectRollDiceState [URL="\ref SelectRollDiceState"];
      RollDice1TimeState [URL="\ref RollDice1TimeState"];
      RollDice3TimesState [URL="\ref RollDice3TimesState"];
      FindMovesState [URL="\ref FindMovesState"];

      FindNextPlayerState -> SelectRollDiceState;
      SelectRollDiceState -> RollDice1TimeState;
      SelectRollDiceState -> RollDice3TimesState [label="If no 6 was thrown"];
      RollDice1TimeState -> FindMovesState;
      RollDice3TimesState -> FindNextPlayerState;
      RollDice3TimesState -> FindMovesState;
   }
\enddot
