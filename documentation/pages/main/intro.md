\page intro Introduction and general information

# Introduction and general information {#General}

This page contains the introduction into the documentation systen used in this procet as well as tips and tricks and a few general information about the documentation.

Will not contain any project specific help!

This page is deviced the following sections:
  - \ref Structure
  - \ref Markdown
  - \ref Dot

# Physical structure of the documentation {#Structure}

The documentation consists of two separate areas:
1. \ref Srcdoc "The source code documentation extracted from the comments on the source code."
2. \ref Mddoc "The general documentation written in markdown files."

## Source code documentation {#Srcdoc}

The source code documentation is written as java doc. Each class, file, function or any public member is preceded with a java doc.

    /**
     * Comment goes here
     */


There are several anotations to decorate the the comment:

| Tag | Descriptions |
|-----|--------------|
| \\brief | Short description \[Should not be omitted\] |
| \\file | The file name |
| \\author | The author |
| \\param arg desc | Description of the parameter|
| \\see ref | Link to another element |
| \\return | Description of the return value |
| \\version  | The author |

Example javadoc:

    /**
     * \\brief Sets the led at given position to the given color. Colors range from 0x00 to 0xFF
     *
     * \\param pos 	Position of the led to light
     * \\param r 	The red intensity [0x00-0xFF]
     * \\param g 	The green intensity [0x00-0xFF]
     * \\param b 	The blue intensity [0x00-0xFF]
     */


## Markdown documentation. {#Mddoc}

The general documentation is a collections of .md files. For the markdown syntax see <b>\\ref</b> markdown. The physical location has no impact on the structure. Each page has to start with the keyword <b>\\page [id] [Readable name]</b>. The first word after the <b>\\page</b> keyword is the id, which must not contain any special characters or white spaces.

To structure the pages, the parent page has to reference the current page with the <b>\\subpage [id] keyword</b>. A single page can only be referenced once by <b>\\subpage</b>, a second call will trigger an error. To reference a class simply type its name (e.g. Game), doxygen will automaticly replace the name with a link.

Other pages or custom anchors are referenced by the <b>\\ref [id] keyword</b>, which will insert a clickable link to the given id. Custom anchors ar created by writing <b>{\#ID}</b>.

## Tabke of Contents

To get a tabel of contents, the second line of the page must be

    [TOC]

Every heading must be followed by __{#Id}__. The ID must not containa any special
characters and the must be CamelCase.

# Markdown for writing the documentation {#Markdown}

Markown is a simple syntax for writing sctructured text. Markdown is aimed to be as less intrusive as possible. For complete description see [Doxygen documentation](https://www.stack.nl/~dimitri/doxygen/manual/markdown.html).

Header are preceded with \#. The top-level with one \#, the second-level with two and so on, up to 6 level.

> A heading must always be followed by a blank line, otherwise doxygen will not recognize it as a heading.

Block quotes are preceded by >. __Quotes always need to be closed with a whitespace!__

    > A multiline block
    > quote

Words are emphasized by surrounding them with \_. One underscor will print the text _cursive_. Two will print the text __bold__.

Code can be printed by indenting it with 4 spaces

    Some source code .

Lists are printed by using single dashes or a number.

To draw a table just surround the entries with - and |.

    | Header  | Text |
    |---------|------|
    | Content | xxxx |

The example above will be rendered to the following:

| Header  | Text |
|---------|------|
| Content | xxxx |

# How to draw diagrams {#Dot}

Diagrams are sourrounded by <b>\\dot</b> and <b>\\enddot</b>. Below are listed a few example diagrams with the code producing each diagram attached to it.

## Class Diagram {#Class}

    \\dot
       digraph g {
          graph [fontname="Segoe UI", fontsize=10];
          node [shape=record, fontname="Segoe UI", fontsize=10];
          edge [fontname="Segoe UI", fontsize=10];
          ILEDDriver [ URL="\ref ILEDDriver"];
          DisplayDriver [ URL="\ref DisplayDriver"];
          DisplayDriver -> ILEDDriver [ arrowhead="open", style="dashed" ];
       }
    \\enddot

\dot
   digraph g {
      graph [fontname="Segoe UI", fontsize=10];
      node [shape=record, fontname="Segoe UI", fontsize=10];
      edge [fontname="Segoe UI", fontsize=10];
      ILEDDriver [ URL="\ref ILEDDriver"];
      DisplayDriver [ URL="\ref DisplayDriver"];
      DisplayDriver -> ILEDDriver [ arrowhead="open", style="dashed" ];
   }
\enddot

## Decision-Tree, that could easily be used as state diagram. {#Tree}

    \\dot
       digraph G {
          graph [fontname="Segoe UI", fontsize=10];
          node [fontname="Segoe UI", fontsize=10];
          edge [fontname="Segoe UI", fontsize=10];
          main -> parse -> execute;
          main -> init;
          main -> cleanup;
          execute -> make_string;
          execute -> printf
          init -> make_string;
          main -> printf;
          execute -> compare;
       }
    \\enddot

\dot
   digraph G {
      graph [fontname="Segoe UI", fontsize=10];
      node [fontname="Segoe UI", fontsize=10];
      edge [fontname="Segoe UI", fontsize=10];
      main -> parse -> execute;
      main -> init;
      main -> cleanup;
      execute -> make_string;
      execute -> printf
      init -> make_string;
      main -> printf;
      execute -> compare;
   }
\enddot

## Decision-Tree with edge names {#DecisionEdge}

    \\dot
       digraph G {
          graph [fontname="Segoe UI", fontsize=10];
          node [fontname="Segoe UI", fontsize=10];
          edge [fontname="Segoe UI", fontsize=10];
             init [label="Init State"];
             dec1 [label="Decision 1"];
             dec2 [label="Decision 2"];
             init -> dec1 [label="Condition 1"];
             init -> dec2 [label="Condition 2"];
       }
    \\enddot

\dot
   digraph G {
      graph [fontname="Segoe UI", fontsize=10];
      node [fontname="Segoe UI", fontsize=10];
      edge [fontname="Segoe UI", fontsize=10];
         init [label="Init State"];
         dec1 [label="Decision 1"];
         dec2 [label="Decision 2"];
         init -> dec1 [label="Condition 1"];
         init -> dec2 [label="Condition 2"];
   }
\enddot

## Cylic Decision-Tree with edge names {#CyclicEdge}

    \\dot
       digraph G {
          graph [fontname="Segoe UI", fontsize=10];
          node [fontname="Segoe UI", fontsize=10];
          edge [fontname="Segoe UI", fontsize=10];
             init [label="Init State"];
             dec1 [label="Decision 1"];
             dec2 [label="Decision 2"];
             init -> dec1 [label="Condition 1"];
             init -> dec2 [label="Condition 2"];
             dec1 -> init;
             dec2 -> init;
       }
    \\enddot

\dot
   digraph G {
      graph [fontname="Segoe UI", fontsize=10];
      node [fontname="Segoe UI", fontsize=10];
      edge [fontname="Segoe UI", fontsize=10];
         init [label="Init State"];
         dec1 [label="Decision 1"];
         dec2 [label="Decision 2"];
         init -> dec1 [label="Condition 1"];
         init -> dec2 [label="Condition 2"];
         dec1 -> init;
         dec2 -> init;
   }
\enddot
