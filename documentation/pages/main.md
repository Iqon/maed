\mainpage Technical Documentation Overview
\tableofcontents

# Overview {#Overview}

Go to the following page to learn more about the documentation systen and how to use it: \subpage intro.

For the main gamelogic visit \ref states.

# Project structure {#MainStructure}

All business services and shared resources are implemented as singletons. The main areas are:

| Area | Description |
|------|-------------|
| Output | Allows access and easy control of the LED-Matrix. Main classes are ILEDDriver, WS2801LEDDriver and the DisplayDriver.  |
| Input | Allwows to read the games inputs. See InputMgr. |
| Game | Contains all shared resources. The global game-state, e.g. the active players, the current positions on the field. See GameMgr|

The game logic itself is implemented as a finite state machine (FSM). For a description on how the state machine works see \ref Fsm. The page \subpage states contains a list of all states and their transitions.

# Finite state machine {#Fsm}

A state is of type StateBase. There is always one active state. If a tate is activated the Enter-Function is called once, upon deactivating the state, the Leave-Functio is called once. While the state is active, the Update method is called repeatedly. Each update gets passed the delta in ticks. The delta is the number of ticks since the last update was called.

\dot
   digraph statelifecycle {
      label="State Lifecycle";
         graph [fontname="Segoe UI", fontsize=10];
         node [fontname="Segoe UI", fontsize=10];
         edge [fontname="Segoe UI", fontsize=10];
      Enter;
      Update;
      Leave;
      Enter -> Update;
      Update -> Update;
      Update -> Leave;
   }
\enddot

A state can change to its next state by using the StateMgr switch state. Each state has the private method __NextState__, which uses the StateMgr to switch to the given state on the same hierarchy level. Each switch causes the current state to be stopped and to enter the next state.

A state could switch to a __child state__, if the child state stops (meaning there is no active state on the child's hierarchy level left), the state machine will go back to the parent state and start executing the parent again.

A state can always use __Stop()__ to stop itself. The top-parent state must never be stopped!

Additionally each state can start a __sub state__. Sub states are executed parallel to the current state. If sub state stops itself, it does not affect the parent. Sub states are updated before updated the current active state, so the active state can use every result or state a sub state may provides. An example if this is the PulsingState. It has a public method __GetColor__ and an internal pulsing intensity. It allows the parent to convert a color to the pulsing intensity which allows the parent to render pulsing leds.

Additionaly to the shared resources as singletons, states can pass date to other states. The date is yielded as return while leaving the current state and passed as parameter while entering the state. There are 4 different types of states:

An IState should never be reused. After stopping it, recreate it. Childstate need to be added in the constructor or the Enter phase of the IState's lifecycle. All childstates will be removed after the leave phase.

| Type | Description |
|------|-------------|
| State | A state which does not yield or accept any value |
| ReturnState | A state which yields a result while leaving. The following state must be of type EnterState, otherwise the returned value is lost. |
| EnterState | A state which accepts a value while entering. The previous state must be of type ReturningState, otherwise the passed value is NULL. |
| ForwardingState | The forwarding state does yield and accept a state but does not care about the value. It will only forward the value received while entering. |
| EnterReturnState | This state yields and accepts a value. |

The type of the yielded or passed value is determined by the usage of templates. Be careful while using them. Because those value are internally stored in void pointers, cpp will happily cast them into any given type. This could not just yield in wrong results but damage the current software.
