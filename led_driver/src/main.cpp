/**
*****************************************************************************
**
**  File        : main.c
**
**  Abstract    : main function.
**
**  Functions   : main
**
**  Environment : Atollic TrueSTUDIO(R)
**                STMicroelectronics STM32F4xx Standard Peripherals Library
**
**  Distribution: The file is distributed "as is", without any warranty
**                of any kind.
**
**  (c)Copyright Atollic AB.
**  You may use this file as-is or modify it according to the needs of your
**  project. This file may only be built (assembled or compiled and linked)
**  using the Atollic TrueSTUDIO(R) product. The use of this file together
**  with other tools than Atollic TrueSTUDIO(R) is not permitted.
**
*****************************************************************************
*/

/* Includes */
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

#include <display/WS2801LEDDriver.h>
#include <display/DisplayDriver.h>


rgb color;

void Delay(__IO uint32_t time);
extern __IO uint32_t TimingDelay;

void TestAll(ILEDDriver *driver, DisplayDriver *display);
void TestLEDs(ILEDDriver *driver);
void TestDice(DisplayDriver *display);
void TestHouse(DisplayDriver *display);
void TestMultiplyer(DisplayDriver *display);
void TestGoal(DisplayDriver *display);
void TestPlayer(DisplayDriver *display);

/**
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/
int main(void)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	/* Initialize LEDs */
	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDInit(LED4);
	STM_EVAL_LEDInit(LED5);
	STM_EVAL_LEDInit(LED6);

	/* Turn on LEDs */
	STM_EVAL_LEDOn(LED3);
	STM_EVAL_LEDOn(LED4);
	STM_EVAL_LEDOn(LED5);
	STM_EVAL_LEDOn(LED6);

	SysTick_Config(SystemCoreClock/1000);

	ILEDDriver *driver = new WS2801LEDDriver(NUM_LEDS);
	DisplayDriver *display = new DisplayDriver(driver);

	color.r = 100;
	color.g = 0x00;
	color.b = 0;

	while (1)
	{
		Delay(250);
		driver->Clear();

		TestAll(driver, display);

		driver->Update();
		STM_EVAL_LEDToggle(LED6);
	}
}

void TestAll(ILEDDriver *driver, DisplayDriver *display)
{
	//TestDice(display);
	TestHouse(display);
	//TestMultiplyer(display);
	TestGoal(display);
	TestPlayer(display);
	//TestLEDs(driver);
}
void TestLEDs(ILEDDriver *driver)
{
	static int i = 0x00;

	int j = 0x00;
	do
	{
		driver->SetLED(j, 36, 128, 249);
		j++;
	} while(j < i);

	i += 1;
	i %= (NUM_LEDS + 1);
}

void TestDice(DisplayDriver *display)
{
	static int i = 0x00;

	display->Dice(i + 1, color);

	i += 1;
	i %= 6;
}

void TestHouse(DisplayDriver *display)
{
	static int i = 0x00;

	display->House(0, i + 1, color);
	display->House(1, i + 1, color);
	display->House(2, i + 1, color);
	display->House(3, i + 1, color);

	i += 1;
	i %= 4;
}

void TestMultiplyer(DisplayDriver *display)
{
	static int i = 0x00;

	display->Multiplyer(0, i + 1, color);
	display->Multiplyer(1, i + 1, color);
	display->Multiplyer(2, i + 1, color);
	display->Multiplyer(3, i + 1, color);

	i += 1;
	i %= 3;
}

void TestGoal(DisplayDriver *display)
{
	static int i = 0x00;

	display->Goal(0, i + 1, color);
	display->Goal(1, i + 1, color);
	display->Goal(2, i + 1, color);
	display->Goal(3, i + 1, color);

	i += 1;
	i %= 4;
}

void TestPlayer(DisplayDriver *display)
{
	static int pos1 = 0;
	static int pos2 = 10;
	static int pos3 = 20;
	static int pos4 = 30;

	rgb player1;
	player1.r = 0xFF;
	player1.g = 0x00;
	player1.b = 0x00;

	rgb player2;
	player2.r = 0x00;
	player2.g = 0xFF;
	player2.b = 0x00;

	rgb player3;
	player3.r = 0x00;
	player3.g = 0x00;
	player3.b = 0xFF;

	rgb player4;
	player4.r = 0xFF;
	player4.g = 0xFF;
	player4.b = 0x00;

	display->Field(pos1, player1);
	display->Field(pos2, player2);
	display->Field(pos3, player3);
	display->Field(pos4, player4);

	pos1 += 1;
	pos1 %= ROUND_LEN;
	pos2 += 1;
	pos2 %= ROUND_LEN;
	pos3 += 1;
	pos3 %= ROUND_LEN;
	pos4 += 1;
	pos4 %= ROUND_LEN;
}

void Delay(__IO uint32_t time)
{
	TimingDelay = time;
	while(TimingDelay !=0);
}

/*
 * Callback used by stm32f4_discovery_audio_codec.c.
 * Refer to stm32f4_discovery_audio_codec.h for more info.
 */
extern "C" void EVAL_AUDIO_TransferComplete_CallBack(uint32_t pBuffer, uint32_t Size)
{
	return;
}

/*
 * Callback used by stm324xg_eval_audio_codec.c.
 * Refer to stm324xg_eval_audio_codec.h for more info.
 */
extern "C" uint16_t EVAL_AUDIO_GetSampleCallBack(void)
{
	return -1;
}
