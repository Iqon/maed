/*
 * rgb.h
 *
 *  Created on: May 27, 2016
 *      Author: Aiko Isselhard
 */

#ifndef RGB_H_
#define RGB_H_

#include <inttypes.h>
#include <string.h>

typedef struct rgb_t {
	uint8_t r;
	uint8_t g;
	uint8_t b;
} rgb, *p_rgb;

#endif /* RGB_H_ */
