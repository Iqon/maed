/*
 * DisplayDriver.h
 *
 *  Created on: May 27, 2016
 *      Author: Aiko Isselhard
 */

#ifndef DISPLAYDRIVER_H_
#define DISPLAYDRIVER_H_

#include <display/ILEDDriver.h>
#include <display/rgb.h>

#define ROUND_LEN 40

class DisplayDriver {
public:
	DisplayDriver(ILEDDriver *driver);
	virtual ~DisplayDriver();

	void Dice(uint8_t no, rgb color);
	void Multiplyer(uint8_t player, uint8_t no, rgb color);
	void House(uint8_t player, uint8_t no, rgb color);
	void Goal(uint8_t player, uint8_t count, rgb color);
	void Field(uint8_t pos, rgb color);

private:
	ILEDDriver *_driver;

	void DiceClear(void);
	void Dice1(rgb color);
	void Dice2(rgb color);
	void Dice3(rgb color);
	void Dice4(rgb color);
	void Dice5(rgb color);
	void Dice6(rgb color);

	void GoalPlayer1(uint8_t count, rgb color);
	void GoalPlayer2(uint8_t count, rgb color);
	void GoalPlayer3(uint8_t count, rgb color);
	void GoalPlayer4(uint8_t count, rgb color);

	void Clear(uint8_t startpixel, uint8_t lenght);
};

#endif /* DISPLAYDRIVER_H_ */
