/*
 * FullLEDDriver.h
 *
 * Basic LED Driver. Updates always all LEDs, even if there were no changes.
 * This allows the usage of only one display buffer, but updates take longer,
 * because this updates always all LEDs.
 *
 *  Created on: May 27, 2016
 *      Author: Aiko Isselhard
 */

#ifndef FULLLEDDRIVER_H_
#define FULLLEDDRIVER_H_

#include <stm32f4xx_spi.h>
#include <inttypes.h>
#include <display/ILEDDriver.h>
#include <display/rgb.h>

#define NUM_LEDS	72

class WS2801LEDDriver: public ILEDDriver {
public:
	WS2801LEDDriver(uint16_t size);
	virtual ~WS2801LEDDriver();

	virtual void SetLED(uint16_t  pos, uint8_t r, uint8_t g, uint8_t b);
	virtual void Update(void);
	virtual void Clear(void);

private:
	uint16_t _led_count;
	rgb *_buffer;

	void InitSPI(void);
	void SPISend(uint8_t data);
};

#endif /* FULLLEDDRIVER_H_ */
