/*
 * ILEDDriver.h
 *
 *	Public interface for an LED Driver.
 *
 *  Created on: May 27, 2016
 *      Author: Aiko Isselhard
 */

#ifndef ILEDDRIVER_H_
#define ILEDDRIVER_H_

#include <inttypes.h>
#include <display/rgb.h>

class ILEDDriver {
public:
	ILEDDriver() {}
	virtual ~ILEDDriver() {}

	virtual void SetLED(uint16_t  pos, uint8_t r, uint8_t g, uint8_t b) {}
	virtual void SetLED(uint16_t  pos, rgb color) { this->SetLED(pos, color.r, color.g, color.b); }
	virtual void Update(void) {}
	virtual void Clear(void) {}
};

#endif /* ILEDDRIVER_H_ */
