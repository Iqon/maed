/*
 * DisplayDriver.cpp
 *
 *  Created on: May 27, 2016
 *      Author: Aiko Isselhard
 */

#include <display/DisplayDriver.h>

DisplayDriver::DisplayDriver(ILEDDriver *driver)
{
	this->_driver = driver;
}

DisplayDriver::~DisplayDriver()
{

}

void DisplayDriver::Dice(uint8_t no, rgb color)
{
	// Dice LEDs from 26 to 32
	this->DiceClear();
	switch(no)
	{
	case 1:
		this->Dice1(color);
		break;
	case 2:
		this->Dice2(color);
		break;
	case 3:
		this->Dice3(color);
		break;
	case 4:
		this->Dice4(color);
		break;
	case 5:
		this->Dice5(color);
		break;
	case 6:
		this->Dice6(color);
		break;
	}
}

void DisplayDriver::Dice1(rgb color)
{
	this->_driver->SetLED(29, color);
}

void DisplayDriver::Dice2(rgb color)
{
	this->_driver->SetLED(24, color);
	this->_driver->SetLED(35, color);

}

void DisplayDriver::Dice3(rgb color)
{
	this->_driver->SetLED(24, color);
	this->_driver->SetLED(29, color);
	this->_driver->SetLED(35, color);
}

void DisplayDriver::Dice4(rgb color)
{
	this->_driver->SetLED(24, color);
	this->_driver->SetLED(26, color);
	this->_driver->SetLED(35, color);
	this->_driver->SetLED(33, color);

}

void DisplayDriver::Dice5(rgb color)
{
	this->_driver->SetLED(24, color);
	this->_driver->SetLED(26, color);
	this->_driver->SetLED(35, color);
	this->_driver->SetLED(33, color);
	this->_driver->SetLED(29, color);
}

void DisplayDriver::Dice6(rgb color)
{
	this->_driver->SetLED(26, color);
	this->_driver->SetLED(25, color);
	this->_driver->SetLED(24, color);
	this->_driver->SetLED(35, color);
	this->_driver->SetLED(34, color);
	this->_driver->SetLED(33, color);
}

void DisplayDriver::DiceClear(void)
{
	this->_driver->SetLED(29, 0x00, 0x00, 0x00);
	this->_driver->SetLED(26, 0x00, 0x00, 0x00);
	this->_driver->SetLED(25, 0x00, 0x00, 0x00);
	this->_driver->SetLED(24, 0x00, 0x00, 0x00);
	this->_driver->SetLED(35, 0x00, 0x00, 0x00);
	this->_driver->SetLED(34, 0x00, 0x00, 0x00);
	this->_driver->SetLED(33, 0x00, 0x00, 0x00);
}

void DisplayDriver::Multiplyer(uint8_t player, uint8_t no, rgb color)
{
	switch(player)
	{
	case 0:
		this->Clear(4, 3);
		this->Clear(13, 3);
		this->Clear(14, 3);
		switch(no)
		{
		case 3:
			this->_driver->SetLED(14, color);
		case 2:
			this->_driver->SetLED(13, color);
		case 1:
			this->_driver->SetLED(4, color);
			break;
		}
		break;
	case 1:
		this->Clear(68, 3);
		this->Clear(67, 3);
		this->Clear(59, 3);
		switch(no)
		{
		case 3:
			this->_driver->SetLED(68, color);
		case 2:
			this->_driver->SetLED(67, color);
		case 1:
			this->_driver->SetLED(59, color);
			break;
		}
		break;
	case 2:
		this->Clear(40, 3);
		this->Clear(49, 3);
		this->Clear(50, 3);
		switch(no)
		{
		case 3:
			this->_driver->SetLED(50, color);
		case 2:
			this->_driver->SetLED(49, color);
		case 1:
			this->_driver->SetLED(40, color);
			break;
		}
		break;
	case 3:
		this->Clear(23, 3);
		this->Clear(31, 3);
		this->Clear(32, 3);
		switch(no)
		{
		case 3:
			this->_driver->SetLED(32, color);
		case 2:
			this->_driver->SetLED(31, color);
		case 1:
			this->_driver->SetLED(23, color);
			break;
		}
		break;
	default:
		return;
	}
}

void DisplayDriver::Clear(uint8_t startpixel, uint8_t lenght)
{
	for(uint8_t i = 0x00 ; i < lenght ; i++)
	{
		this->_driver->SetLED(startpixel + i, 0x00, 0x00, 0x00);
	}
}

void DisplayDriver::House(uint8_t player, uint8_t no, rgb color)
{
	int start;

	switch(player)
	{
	case 0:
		start = 0;
		break;
	case 1:
		start = 55;
		break;
	case 2:
		start = 36;
		break;
	case 3:
		start = 19;
		break;
	default:
		return;
	}

	this->Clear(start, 4);

	switch(no)
	{
	case 4:
		this->_driver->SetLED(start + 3, color);
	case 3:
		this->_driver->SetLED(start + 2, color);
	case 2:
		this->_driver->SetLED(start + 1, color);
	case 1:
		this->_driver->SetLED(start + 0, color);
		break;
	}
}

void DisplayDriver::Goal(uint8_t player, uint8_t count, rgb color)
{
	switch(player)
	{
	case 0:
		this->GoalPlayer1(count, color);
		break;
	case 1:
		this->GoalPlayer2(count, color);
		break;
	case 2:
		this->GoalPlayer3(count, color);
		break;
	case 3:
		this->GoalPlayer4(count, color);
		break;
	default:
		return;
	}
}

void DisplayDriver::GoalPlayer1(uint8_t count, rgb color)
{
	this->Clear(9, 4);

	switch(count)
	{
	case 4:
		this->_driver->SetLED(9, color);
	case 3:
		this->_driver->SetLED(10, color);
	case 2:
		this->_driver->SetLED(11, color);
	case 1:
		this->_driver->SetLED(12, color);
		break;
	}
}

void DisplayDriver::GoalPlayer2(uint8_t count, rgb color)
{
	this->Clear(63, 4);

	switch(count)
	{
	case 4:
		this->_driver->SetLED(63, color);
	case 3:
		this->_driver->SetLED(64, color);
	case 2:
		this->_driver->SetLED(65, color);
	case 1:
		this->_driver->SetLED(66, color);
		break;
	}
}

void DisplayDriver::GoalPlayer3(uint8_t count, rgb color)
{
	this->Clear(45, 4);

	switch(count)
	{
	case 4:
		this->_driver->SetLED(45, color);
	case 3:
		this->_driver->SetLED(46, color);
	case 2:
		this->_driver->SetLED(47, color);
	case 1:
		this->_driver->SetLED(48, color);
		break;
	}
}

void DisplayDriver::GoalPlayer4(uint8_t count, rgb color)
{
	this->Clear(27, 4);

	switch(count)
	{
	case 4:
		this->_driver->SetLED(27, color);
	case 3:
		this->_driver->SetLED(28, color);
	case 2:
		this->_driver->SetLED(29, color);
	case 1:
		this->_driver->SetLED(30, color);
		break;
	}
}

void DisplayDriver::Field(uint8_t pos, rgb color)
{
	uint8_t mapping[ROUND_LEN] = {
			4,
			5,
			6,
			7,
			8,
			71,
			70,
			69,
			68,
			67,
			59,
			60,
			61,
			62,
			54,
			53,
			52,
			51,
			50,
			49,
			40,
			41,
			42,
			43,
			44,
			35,
			34,
			33,
			32,
			31,
			23,
			24,
			25,
			26,
			18,
			17,
			16,
			15,
			14,
			13

	};

	uint8_t led = mapping[pos % ROUND_LEN];
	this->_driver->SetLED(led, color);
}
